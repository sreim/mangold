package de.fub.agse.meq.mangold.client;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import net.edzard.kinetic.Kinetic;
import net.edzard.kinetic.Vector2d;

import com.google.gwt.json.client.JSONArray;
import com.google.gwt.json.client.JSONObject;
import com.google.gwt.json.client.JSONString;
import com.google.gwt.json.client.JSONValue;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.web.bindery.event.shared.HandlerRegistration;

import de.fub.agse.meq.mangold.client.component.ComponentFactory;
import de.fub.agse.meq.mangold.client.components.Component;
import de.fub.agse.meq.mangold.client.components.InputComponent;
import de.fub.agse.meq.mangold.client.components.OutputComponent;
import de.fub.agse.meq.mangold.client.components.Presentable;
import de.fub.agse.meq.mangold.client.components.connections.GeneralConnection;
import de.fub.agse.meq.mangold.client.exception.ComponentCreationException;
import de.fub.agse.meq.mangold.client.exception.LoadWorkspaceException;
import de.fub.agse.meq.mangold.client.exception.ParseComponentException;
import de.fub.agse.meq.mangold.client.exception.UnknownComponentException;
import de.fub.agse.meq.mangold.client.helper.JsonHelper;
import de.fub.agse.meq.mangold.client.interaction.events.ChangeEditorToolEvent;
import de.fub.agse.meq.mangold.client.interaction.events.ConfigureBtnClickedEvent;
import de.fub.agse.meq.mangold.client.interaction.events.LoadWorkspaceEvent;
import de.fub.agse.meq.mangold.client.interaction.events.LoadWorkspaceFinalEvent;
import de.fub.agse.meq.mangold.client.interaction.events.NewWorkspaceEvent;
import de.fub.agse.meq.mangold.client.interaction.events.PropertySaveEvent;
import de.fub.agse.meq.mangold.client.interaction.events.RedoStatusChangeEvent;
import de.fub.agse.meq.mangold.client.interaction.events.SaveWorkspaceDeltaEvent;
import de.fub.agse.meq.mangold.client.interaction.events.SaveWorkspaceEvent;
import de.fub.agse.meq.mangold.client.interaction.events.SaveWorkspaceFinalEvent;
import de.fub.agse.meq.mangold.client.interaction.events.UndoStatusChangeEvent;
import de.fub.agse.meq.mangold.client.interaction.events.action.ActionEvent;
import de.fub.agse.meq.mangold.client.interaction.events.action.CreateComponentEvent;
import de.fub.agse.meq.mangold.client.interaction.events.action.CreateConnectionEvent;
import de.fub.agse.meq.mangold.client.interaction.events.action.RemoveEvent;
import de.fub.agse.meq.mangold.client.model.ClientModel;
import de.fub.agse.meq.mangold.client.serialization.JsonWrapper;
import de.fub.agse.meq.mangold.client.service.WorkspaceServiceAsync;
import de.fub.agse.meq.mangold.client.ui.helper.AlertifyHelper;
import de.fub.agse.meq.mangold.client.util.LogAdapter;
import de.fub.agse.meq.mangold.server.SystemProperties;

public class ClientController {

	private static ClientController instance;

	public static ClientController getInstance() {
		if (instance == null)
			instance = new ClientController();
		return instance;
	}

	private static final LogAdapter LOG = LogAdapter
			.get(ClientController.class);

	// ----- END STATIC ----

	private HandlerRegistration undoStatusRegistration;
	private HandlerRegistration redoStatusRegistration;
	private HandlerRegistration configureBtnRegistration;
	private HandlerRegistration propertySaveRegistration;
	private HandlerRegistration changeEditorToolRegistration;
	private HandlerRegistration saveWorkspaceRegistration;
	private HandlerRegistration saveWorkspaceFinalRegistration;
	private HandlerRegistration saveWorkspaceDeltaRegistration;
	private HandlerRegistration loadWorkspaceRegistration;
	private HandlerRegistration loadWorkspaceFinalRegistration;
	private HandlerRegistration newWorkspaceRegistration;
	private HandlerRegistration addComponentRegistration;
	private HandlerRegistration removeRegistration;
	private HandlerRegistration createConnectionRegistration;

	private WorkspaceWidget workspaceWidget;
	private FlowController controller;

	private Map<String, Map<String, JSONObject>> thingMap;
	private Map<String, Configuration> connectionPointConfigurations;

	private ClientController() {
	}

	/**
	 * Needs to be called after WorkspaceWidget is being created
	 */
	public void registerEvents() {
		undoStatusRegistration = UndoStatusChangeEvent.register(
				ClientModel.EVENT_BUS, new UndoStatusChangeEvent.Handler() {
					public void onUndoStatusChange(UndoStatusChangeEvent event) {
						workspaceWidget.menuBar.setUndoEnabled(event
								.getUndoEnabled());
					}
				});
		redoStatusRegistration = RedoStatusChangeEvent.register(
				ClientModel.EVENT_BUS, new RedoStatusChangeEvent.Handler() {
					public void onRedoStatusChange(RedoStatusChangeEvent event) {
						workspaceWidget.menuBar.setRedoEnabled(event
								.getRedoEnabled());
					}
				});
		configureBtnRegistration = ConfigureBtnClickedEvent.register(
				ClientModel.EVENT_BUS, new ConfigureBtnClickedEvent.Handler() {
					public void onConfigureClicked(
							ConfigureBtnClickedEvent event) {
						workspaceWidget.showPropertyEditor();
					}
				});
		propertySaveRegistration = PropertySaveEvent.register(
				ClientModel.EVENT_BUS, new PropertySaveEvent.Handler() {
					public void onPropertySave(PropertySaveEvent event) {
						saveProperties();
					}
				});

		changeEditorToolRegistration = ChangeEditorToolEvent.register(
				ClientModel.EVENT_BUS, new ChangeEditorToolEvent.Handler() {
					public void onChangeEditorTool(ChangeEditorToolEvent event) {
						switch (event.getSelectedEditorToolType()) {
						case CONNECT:
							workspaceWidget.getEditorView().setCurrentTool(
									ToolTypes.CONNECT);
							break;
						case INSPECT:
							workspaceWidget.getEditorView().setCurrentTool(
									ToolTypes.INSPECT);
							break;
						case POINTER:
							workspaceWidget.getEditorView().setCurrentTool(
									ToolTypes.POINT);
							break;
						case REMOVE:
							workspaceWidget.getEditorView().setCurrentTool(
									ToolTypes.REMOVE);
							break;
						}
					}
				});
		saveWorkspaceRegistration = SaveWorkspaceEvent.register(
				ClientModel.EVENT_BUS, new SaveWorkspaceEvent.Handler() {
					public void onSaveWorkspace(SaveWorkspaceEvent event) {
						workspaceWidget.saveWorkspaceModal.show();
					}
				});
		saveWorkspaceFinalRegistration = SaveWorkspaceFinalEvent.register(
				ClientModel.EVENT_BUS, new SaveWorkspaceFinalEvent.Handler() {
					public void onSaveWorkspace(SaveWorkspaceFinalEvent event) {
						saveWorkspace();
					}
				});
		saveWorkspaceDeltaRegistration = SaveWorkspaceDeltaEvent.register(
				ClientModel.EVENT_BUS, new SaveWorkspaceDeltaEvent.Handler() {
					public void onDeltaSave(SaveWorkspaceDeltaEvent event) {
						saveDelta();
					}
				});
		loadWorkspaceRegistration = LoadWorkspaceEvent.register(
				ClientModel.EVENT_BUS, new LoadWorkspaceEvent.Handler() {
					public void onLoadWorkspace(LoadWorkspaceEvent event) {
						WorkspaceServiceAsync.Util.getInstance()
								.getWorkspacesToLoad(
										new AsyncCallback<List<String>>() {
											public void onFailure(
													Throwable caught) {
												// TODO
											}

											public void onSuccess(
													List<String> result) {
												workspaceWidget.loadWorkspaceModal
														.show(result);
											}
										});
					}
				});
		loadWorkspaceFinalRegistration = LoadWorkspaceFinalEvent.register(
				ClientModel.EVENT_BUS, new LoadWorkspaceFinalEvent.Handler() {
					public void onLoadWorkspace(
							final LoadWorkspaceFinalEvent event) {
						loadWorkspaceFinal(event);
					}
				});
		newWorkspaceRegistration = NewWorkspaceEvent.register(
				ClientModel.EVENT_BUS, new NewWorkspaceEvent.Handler() {
					public void onNewWorkspace(NewWorkspaceEvent event) {
						ClientModel.getInstance().getCurrentWorkspace().clear();
						workspaceWidget.editor.getEditor().setEditorOffset(
								new Vector2d(0, 0));
						workspaceWidget.editor.getEditor().setEditorZoom(1);
						workspaceWidget.editor.getEditor().clearForeground();
						workspaceWidget.editor.getEditor().getForeground()
								.draw();
						workspaceWidget.editor.getEditor().getBackground()
								.draw();
						workspaceWidget.editor.getEditor().updateEditorView();
					}
				});
		addComponentRegistration = CreateComponentEvent.register(
				ClientModel.EVENT_BUS,
				new ActionEvent.Handler<Component, Presentable>() {
					public void onAction(
							ActionEvent<Component, Presentable> event) {
						getFlowController().execute(
								(CreateComponentEvent) event);
					}
				});
		removeRegistration = RemoveEvent.register(ClientModel.EVENT_BUS,
				new ActionEvent.Handler<Presentable, Integer>() {
					public void onAction(ActionEvent<Presentable, Integer> event) {
						getFlowController().execute((RemoveEvent) event);
					}
				});
		createConnectionRegistration = CreateConnectionEvent.register(
				ClientModel.EVENT_BUS,
				new ActionEvent.Handler<InputComponent, Presentable>() {
					public void onAction(
							ActionEvent<InputComponent, Presentable> event) {
						getFlowController().execute(
								(CreateConnectionEvent) event);
					}
				});
	}

	public void unregisterEvents() {
		if (undoStatusRegistration == null) {
			// Events not yet initialized
			return;
		}
		undoStatusRegistration.removeHandler();
		redoStatusRegistration.removeHandler();
		configureBtnRegistration.removeHandler();
		propertySaveRegistration.removeHandler();
		changeEditorToolRegistration.removeHandler();
		saveWorkspaceRegistration.removeHandler();
		saveWorkspaceFinalRegistration.removeHandler();
		saveWorkspaceDeltaRegistration.removeHandler();
		loadWorkspaceRegistration.removeHandler();
		loadWorkspaceFinalRegistration.removeHandler();
		newWorkspaceRegistration.removeHandler();
		addComponentRegistration.removeHandler();
		removeRegistration.removeHandler();
		createConnectionRegistration.removeHandler();
	}

	public WorkspaceWidget createWorkspaceWidget() {
		workspaceWidget = new WorkspaceWidget();
		workspaceWidget.setWidth(Window.getClientWidth() + "px");
		workspaceWidget.setHeight(Window.getClientHeight() + "px");
		return workspaceWidget;
	}

	public void setLoggedIn() {
		ClientModel model = ClientModel.getInstance();
		this.controller = new FlowController(workspaceWidget);
		workspaceWidget.menuBar.setController(this.controller);
		workspaceWidget.menuBar.setUsername(model.getUserInfo()
				.getMailAddress());
		workspaceWidget.menuBar.setUserImageUrl(model.getGravatarURL());

		Kinetic.defaultDragability = false;

		workspaceWidget.editor.getOriginalWidget().init();

		AlertifyHelper.showSuccessLog(I18N.Util.get().loggedInMsg());
	}

	protected void loadLatest() {
		WorkspaceServiceAsync.Util.getInstance().loadLatest(
				new AsyncCallback<JsonWrapper>() {
					public void onFailure(Throwable caught) {
						AlertifyHelper.showErrorLog(I18N.Util.get()
								.errorTryingToFetchLatestWorkspace());
					}

					public void onSuccess(JsonWrapper result) {
						if (result == null) {
							AlertifyHelper.showNotificationLog(I18N.Util.get()
									.noLatestWorkspaceExists());
							workspaceWidget.editor.getEditor()
									.updateEditorView();
						} else {
							load(result);
						}
					}
				});
	}

	private void load(JsonWrapper result) {
		try {
			Workspace workspace = ClientModel.getInstance()
					.getCurrentWorkspace();
			JSONObject json = JsonHelper.convertToJson(result.getJson());
			workspace.clear();
			LOG.debug(json);
			workspace.setName(json.get("name").isString().stringValue());
			workspaceWidget.editor.getEditor()
					.setEditorOffset(
							new Vector2d(json
									.get(SystemProperties.WORKSPACE_POSITION)));
			workspaceWidget.editor.getEditor().setEditorZoom(
					json.get(SystemProperties.WORKSPACE_ZOOM).isNumber()
							.doubleValue());
			workspaceWidget.editor.getEditor().clearForeground();
			constructComponents(json);
			workspaceWidget.editor.getEditor().getForeground().draw();
			workspaceWidget.editor.getEditor().getBackground().draw();
			workspaceWidget.editor.getEditor().updateEditorView();
			AlertifyHelper.showSuccessLog(I18N.Util.get().successfullyLoaded());

		} catch (ParseComponentException e) {
			AlertifyHelper.showErrorLog(I18N.Util.get()
					.constructComponentError());
		} catch (LoadWorkspaceException e) {
			AlertifyHelper
					.showErrorLog(I18N.Util.get().errorLoadingWorkspace());
		}
	}

	protected void constructComponents(JSONObject json)
			throws LoadWorkspaceException {
		JSONValue thingsVal = json.get(SystemProperties.WORKSPACE_THINGS);
		JSONArray things = null;
		if (thingsVal != null && (things = thingsVal.isArray()) != null) {
			List<Component> list = new ArrayList<Component>();
			JSONObject thing = null;
			thingMap = new HashMap<String, Map<String, JSONObject>>();
			thingMap.put("components", new HashMap<String, JSONObject>());
			thingMap.put("connections", new HashMap<String, JSONObject>());
			thingMap.put("connectionPoints", new HashMap<String, JSONObject>());
			connectionPointConfigurations = new HashMap<String, Configuration>();

			for (int i = 0; i < things.size(); i++) {
				JSONValue thingVal = things.get(i);
				LOG.debug(thingVal.toString());
				if (thingVal != null && (thing = thingVal.isObject()) != null) {
					if (thing.containsKey(SystemProperties.COMPONENT_ID_FIELD)) {
						// is Component
						thingMap.get("components")
								.put(thing.get("id").isString().stringValue(),
										thing);
					} else if (thing.containsKey("source")
							|| thing.containsKey("target")) {
						// is Connection
						thingMap.get("connections")
								.put(thing.get("id").isString().stringValue(),
										thing);
					} else {
						// is Connection Point
						thingMap.get("connectionPoints")
								.put(thing.get("id").isString().stringValue(),
										thing);
					}
				}
			}
			for (String key : thingMap.get("components").keySet()) {
				JSONObject component = thingMap.get("components").get(key);
				try {
					Component c = ComponentFactory.createComponent(component);

					// Create node in editor
					final Configuration configuration = new Configuration()
							.mergeOverwriteWith(c.getFieldConfiguration());
					configuration
							.put(SystemProperties.COMPONENT_POSITION_FIELD,
									component
											.get(SystemProperties.COMPONENT_POSITION_FIELD));
					c.create(controller, workspaceWidget.getEditorView(),
							configuration);

					c.updateConfigurationEntry(
							SystemProperties.COMPONENT_ID_FIELD,
							component.get(SystemProperties.COMPONENT_ID_FIELD));
					c.setActive();
					ClientModel.getInstance().getCurrentWorkspace()
							.addStateless(c);
					list.add(c);
				} catch (NullPointerException e) {
					e.printStackTrace();
					throw new LoadWorkspaceException();
				} catch (ComponentCreationException e) {

				}
			}
			for (String key : thingMap.get("connectionPoints").keySet()) {
				JSONObject connectionPoint = thingMap.get("connectionPoints")
						.get(key);
				Configuration config = new Configuration();
				for (String cpKey : connectionPoint.keySet()) {
					if (Configuration.isEditableConfigurationKey(cpKey)) {
						config.put(cpKey, connectionPoint.get(cpKey));
					}
				}
				connectionPointConfigurations.put(key, config);
			}
			Configuration connectionConfiguration = null;
			for (String key : thingMap.get("connections").keySet()) {
				JSONObject conn = thingMap.get("connections").get(key);
				connectionConfiguration = new Configuration();

				final GeneralConnection connection = new GeneralConnection(
						(OutputComponent) ClientModel
								.getInstance()
								.getCurrentWorkspace()
								.get(conn.get("source").isString()
										.stringValue()),
						(InputComponent) ClientModel
								.getInstance()
								.getCurrentWorkspace()
								.get(conn.get("target").isString()
										.stringValue()));
				connection.create(controller,
						workspaceWidget.editor.getEditor(),
						connectionConfiguration);
				connection.setId(key);
				connection.setActive();
				ClientModel.getInstance().getCurrentWorkspace()
						.addStateless(connection);
				connection.getSourceConnectionPoint().setId(
						conn.get("sourceConnectionPoint").isString()
								.stringValue());
				connection.getSourceConnectionPoint().setActive();
				ClientModel.getInstance().getCurrentWorkspace()
						.addStateless(connection.getSourceConnectionPoint());
				connection.getTargetConnectionPoint().setId(
						conn.get("targetConnectionPoint").isString()
								.stringValue());
				connection.getTargetConnectionPoint().setActive();
				ClientModel.getInstance().getCurrentWorkspace()
						.addStateless(connection.getTargetConnectionPoint());

				if (connectionPointConfigurations.containsKey(conn
						.get("sourceConnectionPoint").isString().stringValue())) {
					connection
							.getSourceConnectionPoint()
							.getConfiguration()
							.mergeOverwriteWith(
									connectionPointConfigurations.get(conn
											.get("sourceConnectionPoint")
											.isString().stringValue()));
				}
				if (connectionPointConfigurations.containsKey(conn
						.get("targetConnectionPoint").isString().stringValue())) {
					connection
							.getTargetConnectionPoint()
							.getConfiguration()
							.mergeOverwriteWith(
									connectionPointConfigurations.get(conn
											.get("targetConnectionPoint")
											.isString().stringValue()));
				}
			}
		}
	}

	public Component constructComponent(Component component, Vector2d position)
			throws UnknownComponentException {

		if (component == null) {
			throw new UnknownComponentException();
		}

		// Instantiate component
		Component newComponent = null;
		try {
			newComponent = ComponentFactory
					.createNewInstanceOfComponent(component);
		} catch (ComponentCreationException e) {
			throw new UnknownComponentException(e.getMessage());
		}

		// Create node in editor
		final Configuration configuration = new Configuration()
				.mergeOverwriteWith(component.getFieldConfiguration());
		configuration.put(SystemProperties.COMPONENT_POSITION_FIELD,
				position.toJson());
		newComponent.create(controller, workspaceWidget.getEditorView(),
				configuration);

		return newComponent;
	}

	public void saveProperties() {
		Configuration config = workspaceWidget.propertyEditor
				.getConfiguration();
		controller.getCurrentSelection().update(config);
		workspaceWidget.getEditorView().getForeground().draw();
		ClientModel.getInstance().getCurrentWorkspace()
				.addAndRemark(controller.getCurrentSelection());
	}

	public FlowController getFlowController() {
		return controller;
	}

	private void saveWorkspace() {
		Workspace workspace = ClientModel.getInstance().getCurrentWorkspace();
		JSONObject workspaceObj = workspace.toJson();
		workspaceObj.put(SystemProperties.WORKSPACE_POSITION,
				workspaceWidget.editor.getEditor().getEditorOffset().toJson());
		workspaceObj.put(
				SystemProperties.WORKSPACE_ZOOM,
				new JSONString(Double.toString(workspaceWidget.editor
						.getEditor().getEditorZoom())));
		String str = null;
		if ((str = workspaceObj.toString()) != null) {
			WorkspaceServiceAsync.Util.getInstance().save(str,
					new AsyncCallback<Void>() {
						public void onFailure(Throwable caught) {
							AlertifyHelper.showErrorLog(I18N.Util.get()
									.unsuccessfullySaved());
						}

						public void onSuccess(Void result) {
							AlertifyHelper.showSuccessLog(I18N.Util.get()
									.successfullySaved());
						}
					});
		} else {
			AlertifyHelper.showErrorLog(I18N.Util.get().unsuccessfullySaved());
		}
	}

	private void saveDelta() {
		Workspace workspace = ClientModel.getInstance().getCurrentWorkspace();
		Map<String, Map<String, JsonWrapper>> delta = workspace.getDelta();
		WorkspaceServiceAsync.Util.getInstance().saveDelta(workspace.getName(),
				delta, workspace.getZoom(), workspace.getOffset().x,
				workspace.getOffset().y, new AsyncCallback<Void>() {
					public void onFailure(Throwable caught) {
						AlertifyHelper.showErrorLog(I18N.Util.get()
								.unsuccessfullySaved());
					}

					public void onSuccess(Void result) {
						AlertifyHelper.showSuccessLog(I18N.Util.get()
								.successfullySavedDelta());
					}
				});
	}

	private void loadWorkspaceFinal(final LoadWorkspaceFinalEvent event) {
		if (event.getWorkspace() != null && !event.getWorkspace().isEmpty()) {
			WorkspaceServiceAsync.Util.getInstance().load(event.getWorkspace(),
					new AsyncCallback<JsonWrapper>() {
						public void onFailure(Throwable caught) {
							AlertifyHelper.showErrorLog(I18N.Util.get()
									.errorFetchingWorkspaceData());
						}

						public void onSuccess(JsonWrapper result) {
							if (result != null) {
								load(result);
							} else {
								AlertifyHelper.showErrorLog(I18N.Util.get()
										.errorFetchingWorkspaceData());
							}
						}
					});
		}
	}
}
