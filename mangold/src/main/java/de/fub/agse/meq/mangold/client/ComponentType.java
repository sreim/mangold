package de.fub.agse.meq.mangold.client;

public enum ComponentType {
	SOURCE, TRANSFORMATION, SINK
}
