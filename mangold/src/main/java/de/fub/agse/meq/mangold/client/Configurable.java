package de.fub.agse.meq.mangold.client;

import net.edzard.kinetic.Vector2d;

import com.google.gwt.json.client.JSONValue;



// Merge configuration, description and representation somehow.
public interface Configurable extends Identifiable {
	
	public enum TypeConfiguration {
		TEXTBOX("textbox", "TextBox"),
		CHECKBOX("checkbox", "CheckBox"), 
		DROPDOWN("dropdown", "Dropdown");
		
		private TypeConfiguration(final String type, final String representable) {
			this.type = type;
			this.representable = representable;
		}
		
		private final String type;
		private final String representable;
		
		public String toString() {
			return this.type;
		}
		
		public String getRepresentable() {
			return this.representable;
		}
	}
	
	Configuration getConfiguration();
	
	void update(Configuration configuration);
	
	void updateConfigurationEntry(String key, JSONValue value);
	
	Configuration getFieldConfiguration();
	void setFieldConfiguration(Configuration newDefaultFieldConfiguration);
}