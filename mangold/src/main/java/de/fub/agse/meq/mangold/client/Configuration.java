package de.fub.agse.meq.mangold.client;

import java.io.Serializable;
import java.util.Set;

import com.google.gwt.json.client.JSONObject;
import com.google.gwt.json.client.JSONValue;

import de.fub.agse.meq.mangold.server.SystemProperties;

// TODO: type-safe get
public class Configuration implements Serializable {
	
	private static final long serialVersionUID = -5746469098422240141L;
	
	//private Map<String, Object> data;
	JSONObject data;
	
	public Configuration() {
		super();
		this.data = new JSONObject();
	}
	
	public Configuration(JSONObject data) {
		super();
		this.data = data != null ? data : new JSONObject();
	}
		
//	public Map<String, Object> getData() {
//		return Collections.unmodifiableMap(data);
//	}
	
	public Set<String> keySet() {
		return data.keySet();
	}
	
	public boolean containsKey(String key) {
		return data.containsKey(key);
	}
	
	public JSONValue get(String key) {
		return data.get(key);
	}
	
	public Configuration mergeOverwriteWith(Configuration other) {
		if (other != null) {
			for (String key: other.keySet()) {
				data.put(key, other.get(key));
			}		
		}
		return this;
	}

//	public Configuration remove(String key) {
//		data.remove(key);
//		return this;
//	}
	
	public Configuration put(String key, JSONValue value) {
		data.put(key, value);
		return this;
	}
	
	public JSONObject toJson() {
		return data;
	}
	
	public String toString() {
		return data.toString();
	}
	
	public static boolean isEditableConfigurationKey(String key) {
		if (key.equalsIgnoreCase("id")) return false;
		if (key.equalsIgnoreCase(SystemProperties.COMPONENT_TYPE_FIELD)) return false;
		if (key.equalsIgnoreCase(SystemProperties.COMPONENT_POSITION_FIELD)) return false;
		if (key.equalsIgnoreCase(SystemProperties.COMPONENT_ID_FIELD)) return false;
		if (key.equalsIgnoreCase("outputs")) return false;
		if (key.equalsIgnoreCase("inputs")) return false;
		if (key.equalsIgnoreCase("component")) return false;
		if (key.equalsIgnoreCase("connection")) return false;
		if (key.equalsIgnoreCase("source")) return false;
		if (key.equalsIgnoreCase("sourceConnectionPoint")) return false;
		if (key.equalsIgnoreCase("target")) return false;
		if (key.equalsIgnoreCase("targetConnectionPoint")) return false;
		return true;
	}
}
