package de.fub.agse.meq.mangold.client;

import java.util.ArrayList;
import java.util.List;

import com.google.gwt.json.client.JSONObject;
import com.google.gwt.json.client.JSONString;
import com.google.gwt.json.client.JSONValue;

import de.fub.agse.meq.mangold.client.admin.field.SetupComponentsElement;
import de.fub.agse.meq.mangold.client.admin.field.SetupComponentsHeadElement;

public class ConfigurationNode {
	
	private SetupComponentsElement elem;
	private String key;
	private List<ConfigurationNode> children;
	
	public ConfigurationNode() {
		children = new ArrayList<ConfigurationNode>();
	}
	
	public ConfigurationNode(SetupComponentsElement elem) {
		this();
		this.elem = elem;
		this.key = elem.getLabel();
	}
	
	public ConfigurationNode(String key) {
		this();
		this.key = key;
	}
	
	public void addNode(ConfigurationNode node) {
		children.add(node);
	}
	
	protected SetupComponentsElement getElem() {
		return this.elem;
	}
	
	protected String getKey() {
		if(this.elem instanceof SetupComponentsHeadElement)
			return this.elem.getValue();
		else 
			return this.key;
	}
	
	protected boolean hasChildren() {
		return this.children.size() > 0;
	}
	
	protected List<ConfigurationNode> getChildren() {
		return this.children;
	}
	
	public boolean remove(ConfigurationNode node) {
		boolean removed = children.remove(node);
		return removed;		
	}
	
	public JSONValue getJson() {
		if(hasChildren()) {
			JSONObject obj = new JSONObject();
			for (ConfigurationNode node : children) {
				obj.put(node.getKey(), node.getJson());
			}
			return obj;
		} else {
			return new JSONString(elem.getValue());
		}
	}
		
}
