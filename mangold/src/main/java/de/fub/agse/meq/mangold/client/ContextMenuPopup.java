package de.fub.agse.meq.mangold.client;

import net.edzard.kinetic.Box2d;
import net.edzard.kinetic.Vector2d;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.logical.shared.CloseEvent;
import com.google.gwt.event.logical.shared.CloseHandler;
import com.google.gwt.user.client.ui.DecoratedPopupPanel;
import com.google.gwt.user.client.ui.Grid;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.PopupPanel;

import de.fub.agse.meq.mangold.client.resources.TheResources;

public class ContextMenuPopup extends DecoratedPopupPanel {

		private final EditorView editor;
	
		private final static double margin = 10;
		private final double maxX, maxY;
		private Box2d size;
		
		final private Grid grid;

		private void correctPosition() {
		
			if (size.left < margin) {
				size.right += (margin - size.left);
				size.left = margin;
			}
			
			if (size.top < margin) {
				size.bottom += (margin - size.top);
				size.top = margin;
			}
			
			if (size.right > maxX) {
				size.left -= (size.right - maxX);
				size.right = maxX;
			}
			
			if (size.bottom > maxY) {
				size.top -= (size.bottom - maxY);
				size.bottom = maxY;
			}
		}
		
		public ContextMenuPopup(EditorView editorView) {
			
			super(true);

			setModal(true);
			this.editor = editorView;
			this.maxX = editor.getStage().getWidth() - margin;
			this.maxY = editor.getStage().getHeight() - margin;
			this.grid = new Grid(4, 4);
			
			// Pointer
			final Image pointerIcon = new Image(TheResources.INSTANCE.iconMenuPoint());
			pointerIcon.addClickHandler(new ClickHandler() {
				@Override
				public void onClick(ClickEvent event) {
					editor.setCurrentTool(ToolTypes.POINT);
					ContextMenuPopup.this.hide();
				}
			});
			grid.setWidget(0, 0, pointerIcon);	
			
			// Remove
			final Image removeIcon = new Image(TheResources.INSTANCE.iconMenuRemove());
			removeIcon.addClickHandler(new ClickHandler() {
				@Override
				public void onClick(ClickEvent event) {
					editor.setCurrentTool(ToolTypes.REMOVE);
					ContextMenuPopup.this.hide();
				}
			});
			grid.setWidget(0, 1, removeIcon);	
			
			// Connect
			final Image connectIcon = new Image(TheResources.INSTANCE.iconMenuConnect());
			connectIcon.addClickHandler(new ClickHandler() {
				@Override
				public void onClick(ClickEvent event) {
					editor.setCurrentTool(ToolTypes.CONNECT);
					ContextMenuPopup.this.hide();
				}
			});
			grid.setWidget(0, 2, connectIcon);	

			// Inspect
			final Image inspectIcon = new Image(TheResources.INSTANCE.iconMenuInspect());
			inspectIcon.addClickHandler(new ClickHandler() {
				@Override
				public void onClick(ClickEvent event) {
					editor.setCurrentTool(ToolTypes.INSPECT);
					ContextMenuPopup.this.hide();
				}
			});
			grid.setWidget(0, 3, inspectIcon);
			
			addCloseHandler(new CloseHandler<PopupPanel>() {
				@Override
				public void onClose(CloseEvent<PopupPanel> event) {
					editor.openCurtain();
				}
			});				
			setWidget(grid);	
			
			// Suppress right-click context menu
			this.getElement().setAttribute("oncontextmenu", "return false;");
		}
		
		public void display(final int x, final int y) {
						
			setPopupPositionAndShow(new DecoratedPopupPanel.PositionCallback() {
				public void setPosition(int offsetWidth, int offsetHeight) {
					
					// Calculate position
					final double cx = x - offsetWidth/2;
					final double cy = y - offsetHeight/2;
					ContextMenuPopup.this.size = new Box2d(cx, cy, cx + offsetWidth, cy + offsetHeight);
					correctPosition();
					final Vector2d offset = editor.getStageOffset();
					
					setPopupPosition((int)(offset.x + size.left), (int)(offset.y + size.top));
				}
			});
			editor.closeCurtain();	
		}
	}