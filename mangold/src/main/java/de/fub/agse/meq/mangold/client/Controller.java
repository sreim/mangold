package de.fub.agse.meq.mangold.client;

import de.fub.agse.meq.mangold.client.components.Presentable;

public interface Controller {
	
	void requestSelection(Presentable newSelection);
	void requestDeselection(Presentable aSelection);
	Presentable getCurrentSelection();
	
	void requestFocus(Presentable tangible);
	void requestBlur(Presentable tangible);
	Presentable getCurrentFocus();

	void requestRemoval(Presentable target);
	
}
