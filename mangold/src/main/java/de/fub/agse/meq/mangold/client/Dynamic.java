package de.fub.agse.meq.mangold.client;

import net.edzard.kinetic.Frame;

public interface Dynamic {
	
	public boolean update(Frame frame);
	
}