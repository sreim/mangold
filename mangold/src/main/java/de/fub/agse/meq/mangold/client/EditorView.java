package de.fub.agse.meq.mangold.client;

import java.util.List;

import net.edzard.kinetic.Box2d;
import net.edzard.kinetic.Colour;
import net.edzard.kinetic.CustomShape;
import net.edzard.kinetic.Kinetic;
import net.edzard.kinetic.Layer;
import net.edzard.kinetic.Line;
import net.edzard.kinetic.Line.LineCap;
import net.edzard.kinetic.MathTooling;
import net.edzard.kinetic.Path;
import net.edzard.kinetic.Rectangle;
import net.edzard.kinetic.Shape.LineJoin;
import net.edzard.kinetic.Stage;
import net.edzard.kinetic.Vector2d;

import com.google.gwt.dom.client.Style.Cursor;
import com.google.gwt.event.dom.client.DragLeaveEvent;
import com.google.gwt.event.dom.client.DragLeaveHandler;
import com.google.gwt.event.dom.client.DragOverEvent;
import com.google.gwt.event.dom.client.DragOverHandler;
import com.google.gwt.event.dom.client.DropEvent;
import com.google.gwt.event.dom.client.DropHandler;
import com.google.gwt.event.dom.client.KeyCodes;
import com.google.gwt.event.logical.shared.CloseEvent;
import com.google.gwt.event.logical.shared.CloseHandler;
import com.google.gwt.event.logical.shared.ResizeEvent;
import com.google.gwt.event.logical.shared.ResizeHandler;
import com.google.gwt.json.client.JSONValue;
import com.google.gwt.user.client.DOM;
import com.google.gwt.user.client.Event;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.DecoratedPopupPanel;
import com.google.gwt.user.client.ui.FocusPanel;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.PopupPanel;
import com.google.gwt.user.client.ui.ResizeLayoutPanel;

import de.fub.agse.meq.mangold.client.components.Component;
import de.fub.agse.meq.mangold.client.components.Connection;
import de.fub.agse.meq.mangold.client.components.InputComponent;
import de.fub.agse.meq.mangold.client.components.Inspectable;
import de.fub.agse.meq.mangold.client.components.OutputComponent;
import de.fub.agse.meq.mangold.client.components.Presentable;
import de.fub.agse.meq.mangold.client.interaction.events.MoveEditorEvent;
import de.fub.agse.meq.mangold.client.interaction.MoveDirection;
import de.fub.agse.meq.mangold.client.interaction.events.ZoomEditorEvent;
import de.fub.agse.meq.mangold.client.interaction.events.ZoomEditorEvent.Zoom;
import de.fub.agse.meq.mangold.client.interaction.events.action.CreateConnectionEvent;
import de.fub.agse.meq.mangold.client.interaction.events.action.RemoveEvent;
import de.fub.agse.meq.mangold.client.model.ClientModel;
import de.fub.agse.meq.mangold.client.ui.helper.AlertifyHelper;
import de.fub.agse.meq.mangold.client.util.LogAdapter;
import de.fub.agse.meq.mangold.server.SystemProperties;

public class EditorView extends Composite {

	private final static String stageName = "stageContainer";

	private ToolTypes currentTool;

	final private ResizeLayoutPanel stageContainer;
	final private FocusPanel stageContainerBase;

	final private Vector2d userPosition;
	
	private static final LogAdapter LOG = LogAdapter.get(EditorView.class); 

	// Kinetic
	private Stage stage;
	private Layer effects, foreground, background;
	private Rectangle curtain, clickCatcher;

	private Line dragConnectionLine;
	private Component dragConnectionSourceComponent;
	
	private boolean editorDragging = false;

	private Vector2d editorOffset;
	private Vector2d editorOffsetBeforeDragging;
	private Vector2d mousePositionBeforeDragging;
	private double zoomLevel;
	
	private final int MAX_X = 6000;
	private final int MAX_Y = 2000;
	private final int DISTANCE_X = 150;
	private final int DISTANCE_Y = 150;
	private final double ZOOM_STEP = 0.25;
	private final double MAX_ZOOM = 5.0;

	public EditorView() {
		this.userPosition = new Vector2d();
		this.editorOffset = new Vector2d(0, 0);
		this.zoomLevel = 1.0;

		this.sinkEvents(Event.ONMOUSEDOWN | Event.ONMOUSEMOVE | Event.ONMOUSEUP
				| Event.ONCLICK | Event.ONCONTEXTMENU | Event.ONKEYUP
				| Event.ONKEYPRESS);

		// Create editor stage
		stageContainer = new ResizeLayoutPanel();
		stageContainer.setSize("100%", "100%");
		stageContainerBase = new FocusPanel();
		stageContainerBase.setSize("100%", "100%");
		stageContainer.add(stageContainerBase);
		initWidget(stageContainer);
		
		MoveEditorEvent.register(ClientModel.EVENT_BUS, new MoveEditorEvent.Handler() {
			public void onMoveEditor(MoveEditorEvent event) {
				dragEditor(event.getDragDirection());
			}
		});
		
		ZoomEditorEvent.register(ClientModel.EVENT_BUS, new ZoomEditorEvent.Handler() {
			public void onZoomEditor(ZoomEditorEvent event) {
				zoomEditor(event.getZoom());
			}
		});
	}

	@Override
	public void onBrowserEvent(com.google.gwt.user.client.Event event) {

		final FlowController controller = ClientController.getInstance().getFlowController();

		// Update pointing device position
		final Vector2d pos = stage.getUserPosition();
		userPosition.set(pos.x+editorOffset.x, pos.y+editorOffset.y);

		switch (DOM.eventGetType(event)) {

		case Event.ONMOUSEDOWN:
			if ((currentTool == ToolTypes.CONNECT)
					&& (DOM.eventGetButton(event) == Event.BUTTON_LEFT)
					&& (controller.getCurrentFocus() instanceof OutputComponent)) {
				// Start connection line
				dragConnectionSourceComponent = (Component) controller
						.getCurrentFocus();
			} else if(controller.getCurrentFocus() == null){
				editorDragging = true;
				this.mousePositionBeforeDragging = new Vector2d(stage.getUserPosition());
				this.editorOffsetBeforeDragging = new Vector2d(editorOffset);
			}
			break;

		// Always update user position
		case Event.ONMOUSEMOVE:
			if (dragConnectionSourceComponent != null) {
				// Is locked on a target?
				boolean targetLock = false;
				InputComponent target = null;
				if (controller.getCurrentFocus() instanceof InputComponent) {
					targetLock = true;
					target = (InputComponent) controller.getCurrentFocus();
				}

				// Check if user pointer is outside source circle
				if (dragConnectionSourceComponent.getPosition().distanceTo(
						userPosition) > dragConnectionSourceComponent
						.getRadius()) {

					// Have an intersection. Update source position of
					// connection line
					dragConnectionLine
							.setPoint(
									0,
									MathTooling
											.intersectLineSegmentStartingFromCircleMidPointWithCircle(
													dragConnectionSourceComponent
															.getPosition(),
													targetLock ? target
															.getPosition()
															: userPosition,
													dragConnectionSourceComponent
															.getRadius()));

					if (targetLock) {
						// Destination position is at intersection with target
						// radius
						dragConnectionLine
								.setPoint(
										1,
										MathTooling
												.intersectLineSegmentStartingFromCircleMidPointWithCircle(
														target.getPosition(),
														dragConnectionSourceComponent
																.getPosition(),
														target.getRadius()));
					} else {
						// Destination position is at pointing device
						dragConnectionLine.setPoint(1, userPosition);
					}
					dragConnectionLine.show();
					effects.draw();

				} else {
					// No intersection, pointer is still within source circle
					if (dragConnectionLine.isVisible()) {
						dragConnectionLine.hide();
						effects.draw();
					}
				}
			} else if(editorDragging) {
				dragEditor();
			}
			break;

		// Mousebutton UP Handler: needed for connecting components
		case Event.ONMOUSEUP:
//			AlertifyHelper.showNotificationLog("[" + this.userPosition.x + " | " + this.userPosition.y + "]");
			if ((currentTool == ToolTypes.CONNECT)
					&& (DOM.eventGetButton(event) == Event.BUTTON_LEFT)) {
				final Presentable target = controller.getCurrentFocus();
				if ((target instanceof InputComponent)
						&& (target != dragConnectionSourceComponent)) {
					OutputComponent outputComponent = (OutputComponent) dragConnectionSourceComponent;
					InputComponent inputComponent = (InputComponent) target;
					final Configuration connectionConfiguration = new Configuration();
					JSONValue inputVal = inputComponent.getConfiguration().get(SystemProperties.INPUT_NAME);
					JSONValue outputVal = outputComponent.getConfiguration().get(SystemProperties.OUTPUT_NAME);
					
					connectionConfiguration.put(SystemProperties.INPUT_NAME, inputVal);
					connectionConfiguration.put(SystemProperties.OUTPUT_NAME, outputVal);
					ClientModel.EVENT_BUS.fireEvent(new CreateConnectionEvent(
							outputComponent, inputComponent, connectionConfiguration));
				}
				dragConnectionSourceComponent = null;
				dragConnectionLine.hide();
				effects.draw();
			}
			editorDragging = false;
			break;

		// Global left- and right click handler
		// Right opens our context menu
		// Left click depends on the selected tool
		case Event.ONCLICK:
			// Left button
			if (DOM.eventGetButton(event) == Event.BUTTON_LEFT) {
				Presentable t = controller.getCurrentFocus();
				switch (currentTool) {
				case POINT:
					if (t instanceof Presentable) {
						if (controller.getCurrentSelection() == t)
							controller.requestDeselection(t);
						else
							controller.requestSelection(t);
					} else {
						controller.clearSelection();
					}
					break;
				case REMOVE:
					if (t != null)
						ClientModel.EVENT_BUS.fireEvent(new RemoveEvent(t));
					break;
				case INSPECT:
					if (t instanceof Inspectable) {
						// TODO: find better way of retrieving the name
						final String url = ((Inspectable) t).getInspectionURL();
						final String name = t instanceof Configurable ? ((Configurable) t)
								.getConfiguration().get("name").isString()
								.stringValue()
								: t.getId();
						Window.open(url, name, "");
					}
					break;
				default:
					// DO nothing
				}
				// Right button
			} else if (DOM.eventGetButton(event) == Event.BUTTON_RIGHT) {
				// Display context menu
				//
			}
			break;
		case Event.ONCONTEXTMENU:
			// new ContextMenuPopup(EditorView.this).display((int)pos.x,
			// (int)pos.y);
			// let nothing happen
			break;
		case Event.ONKEYUP:
			handleKeyUp(event);
			break;
		case Event.ONKEYPRESS:
			handleKeyPress(event);
			break;
		default:
			LOG.warn(event);
			break;
		}
	}
	
	public void clearForeground() {
		this.foreground.removeChildren();
		this.foreground.clear();
	}

	private void zoomEditor(Zoom zoom) {
		switch (zoom) {
		case IN:
			zoomLevel -= ZOOM_STEP;
			break;
		case OUT:
			zoomLevel += ZOOM_STEP;
			break;
		}
		zoomLevel = Math.min(Math.max(0.5, zoomLevel), MAX_ZOOM);

		updateEditorView();
	}
	
	private void dragEditor(MoveDirection direction) {
		switch (direction) {
		case LEFT:
			this.editorOffset.x = Math.max(0, Math.min(this.editorOffset.x - 200, MAX_X-Window.getClientWidth()));
			break;
		case RIGHT:
			this.editorOffset.x = Math.max(0, Math.min(this.editorOffset.x + 200, MAX_X-Window.getClientWidth()));
			break;
		case UP:
			this.editorOffset.y = Math.max(0, Math.min(this.editorOffset.y - 200, MAX_Y-Window.getClientHeight()));
			break;
		case DOWN:
			this.editorOffset.y = Math.max(0, Math.min(this.editorOffset.y + 200, MAX_Y-Window.getClientHeight()));
			break;
		}
		updateEditorView();
	}
	
	private void dragEditor() {
		this.editorOffset.set(
				editorOffsetBeforeDragging.x + (mousePositionBeforeDragging.x - stage.getUserPosition().x),
				editorOffsetBeforeDragging.y + (mousePositionBeforeDragging.y - stage.getUserPosition().y));
		
		this.editorOffsetBeforeDragging.set(editorOffset);
		this.mousePositionBeforeDragging.set(stage.getUserPosition());
		
		updateEditorView();
	}

	public void updateEditorView() {
		drawGrid();
		adjustComponents();
		updateWorkspace();
		LOG.debug("new Editor position: " + this.editorOffset.toJson().toString());
	}

	private void adjustComponents() {
		adjustComponentPosition();
		adjustComponentSize();
		getForeground().draw();
	}
	
	private void adjustComponentPosition() {
		Workspace workspace = ClientModel.getInstance().getCurrentWorkspace();
		List<Configurable> configurables = workspace.getAllConfigurables();
		Vector2d newPos = null;
		Vector2d posAbsolute = null;
		for (Configurable configurable : configurables) {
			JSONValue posVal = configurable.getConfiguration().get(SystemProperties.COMPONENT_POSITION_FIELD);
			if(posVal == null)
				continue;
			//get recent absolute position without offset and zoom
			posAbsolute = new Vector2d(posVal);
			posAbsolute.set(((posAbsolute.x+workspace.getOffset().x)*workspace.getZoom()), 
					((posAbsolute.y+workspace.getOffset().y)*workspace.getZoom()));
			
			//calculate new position
			newPos = new Vector2d(((posAbsolute.x/zoomLevel)-editorOffset.x), 
					((posAbsolute.y/zoomLevel)-editorOffset.y));
			
			posAbsolute.x = posAbsolute.x+workspace.getOffset().x;
			posAbsolute.y = posAbsolute.y+workspace.getOffset().y;
			configurable.updateConfigurationEntry(SystemProperties.COMPONENT_POSITION_FIELD, newPos.toJson());
//			AlertifyHelper.showNotificationLog("Position new: " + newPos.toJson().toString());
			workspace.addToDelta(configurable);
		}
		for (Configurable configurable : configurables) {
			if(configurable instanceof Connection) {
				((Connection) configurable).update();
			}
		}
	}
	
	private void adjustComponentSize() {
		Workspace workspace = ClientModel.getInstance().getCurrentWorkspace();
		List<Configurable> configurables = workspace.getAllConfigurables();
		for (Configurable configurable : configurables) {
			if(configurable instanceof Presentable) {
				Presentable p = (Presentable) configurable;
				p.adjustByZoom(zoomLevel);
			}
		}
	}
	
	private void updateWorkspace() {
		Workspace workspace = ClientModel.getInstance().getCurrentWorkspace();
		workspace.setOffset(new Vector2d(editorOffset));
		workspace.setZoom(zoomLevel);
	}

	private void handleKeyUp(Event event) {
		switch (event.getKeyCode()) {
		case KeyCodes.KEY_LEFT:
			dragEditor(MoveDirection.LEFT);
			break;
		case KeyCodes.KEY_UP:
			dragEditor(MoveDirection.UP);
			break;
		case KeyCodes.KEY_RIGHT:
			dragEditor(MoveDirection.RIGHT);
			break;
		case KeyCodes.KEY_DOWN:
			dragEditor(MoveDirection.DOWN);
			break;
		}
	}

	private void handleKeyPress(Event event) {
		if (event.getAltKey()) {
			switch (event.getKeyCode()) {
			case 161: // ALT+1
				setCurrentTool(ToolTypes.INSPECT);
				break;
			case 8220: // ALT+2
				setCurrentTool(ToolTypes.CONNECT);
				break;
			case 182:
				setCurrentTool(ToolTypes.POINT);
				break;
			case 162:
				setCurrentTool(ToolTypes.REMOVE);
				break;
			}
		}
	}

	void init() {

		// Kinetic.stageId = "stageContainer";
		stageContainerBase.getElement().setId(stageName);
		// final Element stageElement =
		// Document.get().getElementById(stageName);
		stage = Kinetic.createStage(stageContainerBase.getElement(),
				stageContainerBase.getOffsetWidth(),
				stageContainerBase.getOffsetHeight());

		createEditorAreaBackgroundLayer();
		createEditorAreaForegroundLayer();
		createEditorAreaEffectsLayer();

		stageContainer.addResizeHandler(new ResizeHandler() {
			@Override
			public void onResize(ResizeEvent event) {
				stage.setSize(new Vector2d(event.getWidth(), event.getHeight()));
				clickCatcher.setWidth(stage.getWidth());
				clickCatcher.setHeight(stage.getHeight());
				if (curtain != null) {
					curtain.setWidth(stage.getWidth());
					curtain.setHeight(stage.getHeight());
				}
				stage.draw();
			}
		});

		// Configure the drop target
		stageContainerBase.addDragOverHandler(new DragOverHandler() {
			@Override
			public void onDragOver(DragOverEvent event) {
				// Necessary to have
			}
		});

		stageContainerBase.addDragLeaveHandler(new DragLeaveHandler() {
			@Override
			public void onDragLeave(DragLeaveEvent event) {
				// Necessary to have
			}
		});

		stageContainerBase.addDropHandler(new DropHandler() {

			@Override
			// TODO remove
			public void onDrop(DropEvent event) {

				// userPosition.set(
				// event.getNativeEvent().getClientX() -
				// stageContainerBase.getAbsoluteLeft(),
				// event.getNativeEvent().getClientY() -
				// stageContainerBase.getAbsoluteTop()
				// );
				//
				// // Un-wrap event content
				// // TODO: do better data transfer - the token thing is not
				// really nice
				// final int id = Integer.parseInt(event.getData("text"));
				// final Vector2d pos = new Vector2d(
				// event.getNativeEvent().getClientX() -
				// stageContainerBase.getAbsoluteLeft(),
				// event.getNativeEvent().getClientY() -
				// stageContainerBase.getAbsoluteTop()
				// );
				//
				// Create graphical representation
				// controller.execute(new CreateComponentCommand(id, pos));
			}
		});

		// TODO: check browser compatibility
		// Suppress the right-click context menu (we want to do right-click
		// functionality ourselves)
		stageContainerBase.getElement().setAttribute("oncontextmenu",
				"return false;");
		// Suppress default text selection behaviour
		stageContainerBase.getElement().setAttribute("onselectstart",
				"return false;");

		setCurrentTool(ToolTypes.POINT);

	}

	private void createEditorAreaEffectsLayer() {
		// Prepare effects layer
		effects = Kinetic.createLayer();
		stage.add(effects);

		// Prepare drag connection line
		dragConnectionLine = Kinetic
				.createLine(Vector2d.origin, Vector2d.xUnit); // just some
																// coordinates
		dragConnectionLine.setStrokeWidth(5);
		dragConnectionLine.setLineCap(LineCap.ROUND);
		dragConnectionLine.setStroke(Colour.yellow.alpha(128));
		dragConnectionLine.setLineStyle(Line.LineStyle.DASHED);
		effects.add(dragConnectionLine);
		dragConnectionLine.hide();
	}

	private void createEditorAreaForegroundLayer() {

		// Prepare foreground
		foreground = Kinetic.createLayer();
		stage.add(foreground);
	}

	private void createEditorAreaBackgroundLayer() {

		// Prepare background
		background = Kinetic.createLayer();
		stage.add(background);

		// Create a "click catcher" - sole purpose is to clear selection when
		// user clicks on background
		clickCatcher = Kinetic.createRectangle(new Box2d(0, 0,
				stage.getWidth(), stage.getHeight()));
		clickCatcher.setFill(Colour.white.alpha(0));
		clickCatcher.setDraggable(false);
		background.add(clickCatcher);
	}

	private void drawGrid() {
		Vector2d start = new Vector2d();
		Vector2d end = new Vector2d();
		
		background.removeChildren();
		
		start.y = -editorOffset.y;
		end.y = -editorOffset.y+MAX_Y;
		for (int x = 0; x < ((MAX_X/DISTANCE_X)*zoomLevel); ++x) {
			start.x = end.x = x * (DISTANCE_X/zoomLevel) - editorOffset.x;
			final Line line = Kinetic.createLine(start, end);
			line.setStrokeWidth(1);
			line.setStroke(Colour.gray);
			line.setDraggable(false);
			background.add(line);
		}
		start.x = -editorOffset.x;
		end.x = -editorOffset.x+MAX_X;
		for (int y = 0; y < ((MAX_Y/DISTANCE_Y)*zoomLevel); ++y) {
			start.y = end.y = y * (DISTANCE_Y/zoomLevel) - editorOffset.y;
			final Line line = Kinetic.createLine(start, end);
			line.setStrokeWidth(1);
			line.setStroke(Colour.gray);
			line.setDraggable(false);
			background.add(line);
		}
		background.draw();
	}

	void updateCursor() {
		Cursor cursor;
		switch (currentTool) {
		case REMOVE:
			cursor = Cursor.POINTER;
			break;
		case CONNECT:
			cursor = Cursor.CROSSHAIR;
			break;
		case INSPECT:
			cursor = Cursor.POINTER;
			break;
		default:
			cursor = Cursor.DEFAULT;
			break;
		}
		stageContainerBase.getElement().getStyle().setCursor(cursor);
		// Document.get().getElementById(stageName).getStyle().setCursor(cursor);
	}

	void setCurrentTool(ToolTypes tool) {
		switch (tool) {
		case REMOVE:
			currentTool = ToolTypes.REMOVE;
			break;
		case CONNECT:
			currentTool = ToolTypes.CONNECT;
			break;
		case INSPECT:
			currentTool = ToolTypes.INSPECT;
			break;
		default:
			currentTool = ToolTypes.POINT;
		}
		updateCursor();
	}

	// The offset of the stage in window coordinates
	Vector2d getStageOffset() {
		return new Vector2d(stageContainer.getAbsoluteLeft(),
				stageContainer.getAbsoluteTop());
	}

	void closeCurtain() {
		curtain = Kinetic.createRectangle(new Box2d(0, 0, stage.getWidth(),
				stage.getHeight()));
		curtain.setDraggable(false);
		curtain.setStrokeWidth(0);
		curtain.setStroke(Colour.black.alpha(0));
		curtain.setFill(Colour.black);
		curtain.setOpacity(0.5);
		effects.add(curtain);
		curtain.moveToTop();
		effects.draw();
	}

	void openCurtain() {
		if (curtain != null) {
			effects.remove(curtain);
			curtain = null;
		}
		effects.draw();
	}

	public Component getConnectionSource() {
		return dragConnectionSourceComponent;
	}

	public ToolTypes getCurrentTool() {
		return currentTool;
	}

	public void displayDialogueBubble(final Vector2d origin, final Box2d size) {

		final CustomShape shape = Kinetic.createCustomShape(Vector2d.origin);
		final Path path = new Path();

		final Vector2d topLeft = new Vector2d(size.left, size.top);
		final Vector2d topRight = new Vector2d(size.right, size.top);
		final Vector2d bottomLeft = new Vector2d(size.left, size.bottom);
		final Vector2d bottomRight = new Vector2d(size.right, size.bottom);

		if (origin.x < size.left) { // Left
			path.beginPath()
					.lineTo(topRight)
					.lineTo(bottomRight)
					.lineTo(bottomLeft)
					.lineTo(new Vector2d(size.left, (0.375 * size.top)
							+ (0.625 * size.bottom)));
			path.lineTo(origin)
					.lineTo(new Vector2d(size.left, (0.625 * size.top)
							+ (0.375 * size.bottom))).lineTo(topLeft)
					.closePath();
		} else if (origin.x > size.right) { // Right
			path.beginPath()
					.lineTo(topRight)
					.lineTo(new Vector2d(size.right, (0.625 * size.top)
							+ (0.375 * size.bottom)));
			path.lineTo(origin)
					.lineTo(new Vector2d(size.right, (0.375 * size.top)
							+ (0.625 * size.bottom))).lineTo(bottomRight)
					.lineTo(bottomLeft).lineTo(topLeft).closePath();
		} else if (origin.y < size.top) { // Top
			path.beginPath()
					.lineTo(new Vector2d((0.625 * size.left)
							+ (0.375 * size.right), size.top)).lineTo(origin);
			path.lineTo(
					new Vector2d((0.375 * size.left) + (0.625 * size.right),
							size.top)).lineTo(topRight).lineTo(bottomRight)
					.lineTo(bottomLeft).lineTo(topLeft).closePath();
		} else if (origin.y > size.bottom) { // Bottom
			path.beginPath()
					.lineTo(topRight)
					.lineTo(bottomRight)
					.lineTo(new Vector2d((0.375 * size.left)
							+ (0.625 * size.right), size.bottom))
					.lineTo(origin);
			path.lineTo(
					new Vector2d((0.625 * size.left) + (0.375 * size.right),
							size.bottom)).lineTo(bottomLeft).lineTo(topLeft)
					.closePath();
		} else { // Inside
			path.beginPath().lineTo(topRight).lineTo(bottomRight)
					.lineTo(bottomLeft).lineTo(topLeft).closePath();
		}

		shape.setPath(path);
		shape.setStroke(Colour.fubBlue);
		shape.setStrokeWidth(2);
		shape.setLineJoin(LineJoin.MITER);

		effects.add(shape);
		effects.draw();

		class ConfigurationDialogue extends DecoratedPopupPanel {
			public ConfigurationDialogue() {
				super(true);
				setModal(true);
				setWidget(new HTML("<p>Configuration Form</p>"));
			}
		}

		// Create a panel and add it to the screen
		final ConfigurationDialogue popup = new ConfigurationDialogue();
		popup.setPopupPositionAndShow(new DecoratedPopupPanel.PositionCallback() {
			public void setPosition(int offsetWidth, int offsetHeight) {
				popup.setPopupPosition(stageContainer.getAbsoluteLeft()
						+ (int) size.left + 10, stageContainer.getAbsoluteTop()
						+ (int) size.top + 10);
			}
		});

		popup.addCloseHandler(new CloseHandler<PopupPanel>() {
			@Override
			public void onClose(CloseEvent<PopupPanel> event) {
				effects.remove(shape);
				effects.draw();
			}
		});
	}

	public Vector2d getUserPosition() {
		return userPosition;
	}

	public Layer getBackground() {
		return background;
	}

	public Layer getForeground() {
		return foreground;
	}

	public Stage getStage() {
		return stage;
	}
	
	public Vector2d getEditorOffset() {
		return editorOffset;
	}
	
	public void setEditorOffset(Vector2d offset) {
		this.editorOffset = offset;
		ClientModel.getInstance().getCurrentWorkspace().setOffset(new Vector2d(editorOffset));
	}
	
	public double getEditorZoom() {
		return this.zoomLevel;
	}
	
	public void setEditorZoom(double zoomLevel) {
		this.zoomLevel = zoomLevel;
		ClientModel.getInstance().getCurrentWorkspace().setZoom(zoomLevel);
	}
}
