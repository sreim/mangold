package de.fub.agse.meq.mangold.client;

import java.util.ArrayList;
import java.util.List;

import de.fub.agse.meq.mangold.client.components.Component;
import de.fub.agse.meq.mangold.client.components.Connection;
import de.fub.agse.meq.mangold.client.interaction.events.RedoStatusChangeEvent;
import de.fub.agse.meq.mangold.client.interaction.events.UndoStatusChangeEvent;
import de.fub.agse.meq.mangold.client.interaction.events.action.ActionEvent;
import de.fub.agse.meq.mangold.client.interaction.events.action.CreateComponentEvent;
import de.fub.agse.meq.mangold.client.interaction.events.action.CreateConnectionEvent;
import de.fub.agse.meq.mangold.client.interaction.events.action.RemoveEvent;
import de.fub.agse.meq.mangold.client.model.ClientModel;
import de.fub.agse.meq.mangold.client.util.LogAdapter;

public class ExecutionHistoryController {
	
	private List<ActionEvent<?,?>> undoActivities;
	private List<ActionEvent<?,?>> redoActivities;
	
	private static final LogAdapter LOG = LogAdapter.get(ExecutionHistoryController.class);

	public ExecutionHistoryController() {
		this.undoActivities = new ArrayList<ActionEvent<?,?>>();
		this.redoActivities = new ArrayList<ActionEvent<?,?>>();
		fireStatusEvents();
	}
	
	public <T,E> void insertIntoHistory(ActionEvent<T,E> event) {
		this.undoActivities.add(event);
		redoActivities.clear();
		fireStatusEvents();
	}
	
	public void undoLatestAction() {
		if(!undoActivities.isEmpty()) {
			ActionEvent<?,?> latestEvent = undoActivities.remove(undoActivities.size()-1); 
			ActionEvent<?,?> undoEvent = invertEvent(latestEvent, true);
			if(undoEvent != null)  {
				ClientModel.EVENT_BUS.fireEvent(undoEvent);
				redoActivities.add(undoEvent);
			} 
			fireStatusEvents();
		}
	}
	
	public void redoUndoneAction() {
		if(!redoActivities.isEmpty()) {
			ActionEvent<?,?> undoEvent = redoActivities.remove(redoActivities.size()-1);
			ActionEvent<?,?> redoEvent = invertEvent(undoEvent, false);
			if(redoEvent != null) {
				ClientModel.EVENT_BUS.fireEvent(redoEvent);
			}
			fireStatusEvents();
		}
	}
	
	private ActionEvent<?,?> invertEvent(ActionEvent<?,?> inputEvent, boolean undoAction) {
		ActionEvent<?,?> outputEvent = null;
		if(inputEvent instanceof CreateComponentEvent) {
			CreateComponentEvent event = (CreateComponentEvent) inputEvent;
			outputEvent = new RemoveEvent(event.getResult(), undoAction);
		} else if(inputEvent instanceof CreateConnectionEvent) {
			CreateConnectionEvent event = (CreateConnectionEvent) inputEvent;
			outputEvent = new RemoveEvent(event.getResult(), undoAction);
		} else if(inputEvent instanceof RemoveEvent) {
			RemoveEvent event = (RemoveEvent) inputEvent;
			if(event.getTarget() instanceof Component) {
				Component target = (Component)event.getTarget();
				target.setFieldConfiguration(target.getConfiguration());
				outputEvent = new CreateComponentEvent(target, target.getPosition(), undoAction);
			} else if(event.getTarget() instanceof Connection) {
				Connection connection = (Connection) event.getTarget();
				Configuration configuration = connection.getConfiguration();
				connection.setFieldConfiguration(connection.getConfiguration());
				outputEvent = new CreateConnectionEvent(connection.getSource(), connection.getTarget(), configuration, undoAction);
			}
		}
		if(outputEvent == null)
			LOG.warn("Unidentified Presentable found - could not handle: " + inputEvent.getClass().getName());
		return outputEvent;
	}
	
	private void fireStatusEvents() {
		ClientModel.EVENT_BUS.fireEvent(new UndoStatusChangeEvent(!this.undoActivities.isEmpty()));
		ClientModel.EVENT_BUS.fireEvent(new RedoStatusChangeEvent(!this.redoActivities.isEmpty()));
	}
}
