package de.fub.agse.meq.mangold.client;

import java.util.List;

import com.google.gwt.core.shared.GWT;
import com.google.gwt.user.client.rpc.AsyncCallback;

import de.fub.agse.meq.mangold.client.components.Component;
import de.fub.agse.meq.mangold.client.components.Presentable;
import de.fub.agse.meq.mangold.client.components.connections.GeneralConnection;
import de.fub.agse.meq.mangold.client.exception.UnknownComponentException;
import de.fub.agse.meq.mangold.client.interaction.events.action.ActionEvent;
import de.fub.agse.meq.mangold.client.interaction.events.action.CreateComponentEvent;
import de.fub.agse.meq.mangold.client.interaction.events.action.CreateConnectionEvent;
import de.fub.agse.meq.mangold.client.interaction.events.action.RemoveEvent;
import de.fub.agse.meq.mangold.client.model.ClientModel;
import de.fub.agse.meq.mangold.client.service.WorkspaceService;
import de.fub.agse.meq.mangold.client.service.WorkspaceServiceAsync;
import de.fub.agse.meq.mangold.client.util.LogAdapter;

public class FlowController implements Controller {

	private WorkspaceServiceAsync workspaceService;
	private final WorkspaceWidget wsWidget;

	private Presentable currentSelection;
	private Presentable currentFocus;

	private Workspace workspace;
	
	private static final LogAdapter LOG = LogAdapter.get(FlowController.class); 

	private ExecutionHistoryController historyController;

	public FlowController(WorkspaceWidget workspaceWidget) {
		workspaceService = GWT.create(WorkspaceService.class);
		this.wsWidget = workspaceWidget;
		this.workspace = ClientModel.getInstance().getCurrentWorkspace(); 
		this.historyController = ClientModel.getInstance().getHistoryController();
	}

	@Override
	public void requestSelection(Presentable newSelection) {
		if (currentSelection == newSelection)
			return;
		if (currentSelection != null)
			currentSelection.deselect();
		currentSelection = newSelection;
		if (currentSelection != null) {
			newSelection.select();
			wsWidget.updateSelectionState();
		}
	}

	@Override
	public void requestDeselection(Presentable aSelection) {
		if (aSelection == null)
			return;
		if (aSelection == currentSelection) {
			currentSelection.deselect();
			currentSelection = null;
			wsWidget.updateSelectionState();
		}
	}

	@Override
	public void requestFocus(Presentable tangible) {
		if (tangible == null)
			return;
		if (currentFocus == tangible)
			return;
		if (currentFocus != null)
			currentFocus.blur();
		currentFocus = tangible;
		tangible.focus();
		wsWidget.getEditorView().getForeground().draw();

	}

	@Override
	public void requestBlur(Presentable tangible) {
		if (tangible == null)
			return;
		if (currentFocus != tangible)
			return;
		tangible.blur();
		currentFocus = null;
		wsWidget.getEditorView().getForeground().draw();
	}

	@Override
	public void requestRemoval(Presentable target) {
		if (workspace.contains(target.getId())) {
			workspace.remove(target);
			target.remove();
		}
	}
	
	/* EVENTS */
	
	public void execute(final CreateComponentEvent event) {
		try {
			final Component component = ClientController.getInstance().constructComponent(
					event.getTarget(), event.getPosition());
			// Call backend
			workspaceService.createUUIDs(1, new AsyncCallback<List<String>>() {
				public void onFailure(Throwable caught) {
					LOG.severe(caught.getLocalizedMessage());
					component.setFailure();
				}

				public void onSuccess(List<String> result) {
					final String id = result.get(0);
					component.setId(id);
					component.setActive();
					workspace.addAndRemark(component);
					event.setResult(component);
					if(!event.isUndoAction())
						historyController.insertIntoHistory(event);
				}
			});
		} catch (UnknownComponentException e) {
			LOG.severe(e.getLocalizedMessage());
		}
	}
	
	public void execute(final RemoveEvent event) {
		final Presentable target = event.getTarget();
		requestDeselection(target);
		requestRemoval(target);
		
		if(!event.isUndoAction())
			historyController.insertIntoHistory(event);
		wsWidget.getEditorView().getForeground().draw();
	}
	
	public void execute(final CreateConnectionEvent event) {

		final GeneralConnection connection = new GeneralConnection(
				event.getSource(), event.getTarget());
		connection.create(this, wsWidget.getEditorView(),
				event.getConfiguration());

		// Call backend
		workspaceService.createUUIDs(3, new AsyncCallback<List<String>>() {
			public void onFailure(Throwable caught) {
				LOG.severe(caught.getLocalizedMessage());
				connection.setFailure();
			}

			public void onSuccess(List<String> result) {
				connection.setId(result.get(0));
				connection.setActive();
				connection.getSourceConnectionPoint().setId(result.get(1));
				connection.getSourceConnectionPoint().setActive();
				connection.getTargetConnectionPoint().setId(result.get(2));
				connection.getTargetConnectionPoint().setActive();
				workspace.addConnectionAndRemark(connection, 
						connection.getTargetConnectionPoint(), connection.getSourceConnectionPoint());
				event.setResult(connection);
				if(!event.isUndoAction())
					historyController.insertIntoHistory(event);
			}
		});

		wsWidget.getEditorView().getForeground().draw();
	}
	
	public void execute(ActionEvent<? extends Object, ?> event) {
		// Catch-all
		LOG.severe("Command of type " + event.getClass().getName()
				+ ": flow controller has no idea how to handle it");
	}
	
	public void undoLatestExecution() {
		this.historyController.undoLatestAction();
	}
	
	public void redoLatestUndone() {
		this.historyController.redoUndoneAction();
	}

	@Override
	public Presentable getCurrentSelection() {
		return currentSelection;
	}

	@Override
	public Presentable getCurrentFocus() {
		return currentFocus;
	}

	public void clearSelection() {
		requestDeselection(currentSelection);
	}
}
