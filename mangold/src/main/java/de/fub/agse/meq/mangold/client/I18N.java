package de.fub.agse.meq.mangold.client;

import com.google.gwt.core.shared.GWT;
import com.google.gwt.i18n.client.Messages;

public interface I18N extends Messages {
	
	String sources();
	String transformations();
	String sinks();
	String add();
	String addSource();
	String addSink();
	String addTransformation();
	String newComponent();
	String newConfiguration();
	
	String welcomeScreenTitle();
	String signInTitle();
	String mailAddress();
	String password();
	String signIn();
	String welcomeScreenSubTitle();
	
	String workspace();
	String component();
	String components();
	String logout();
	String selection();
	String settings();
	String type();
	
	String about();
	String load();
	String newWorkspace();
	String loadBtn(String workspace);
	String save();
	String addComponent();
	String addFieldConfiguration();
	String remove();
	String configure();
	String undo();
	String redo();
	String successfullySaved();
	String successfullySavedDelta();
	String unsuccessfullySaved();
	String successfullyLoaded();
	String saveWorkspace();
	String selectName();
	String loadWorkspace();
	
	String tabDefaultFieldConfig();
	String tabComponentBehaviourInput();
	String tabComponentBehaviourOutput();
	String labelComponentInput();
	String labelComponentOutput();
	String tabEditPlainJSON();
	
	String invalidLogin();
	String systemError();
	String constructComponentError();
	String errorFetchingComponents();
	String noComponentsReturned();
	String noLatestWorkspaceExists();
	String loadingLatestWorkspace();
	String numberComponentsReturned(int numberComponents);
	String failedCreatingComponents();
	String errorBuildingConfiguration();
	String errorSaving();
	String errorAddingComponent();
	String errorConstructingWorkspace();
	String errorLoadingWorkspace();
	String errorFetchingWorkspaceData();
	String errorTryingToFetchLatestWorkspace();
	
	String loggedInMsg();
	String configureProperties();
	String configureComponents();
	
	
	/**
	 * I18N Instance
	 *
	 */
	public static class Util {
		private static I18N i18n = GWT.create(I18N.class);
		
		public static I18N get() {
			return i18n;
		}
	}
}
