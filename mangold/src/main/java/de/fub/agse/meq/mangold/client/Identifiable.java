package de.fub.agse.meq.mangold.client;

public interface Identifiable {

	public String getId();

	public void setId(String id);
	
}
