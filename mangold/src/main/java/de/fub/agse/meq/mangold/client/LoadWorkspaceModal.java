package de.fub.agse.meq.mangold.client;

import java.util.List;

import com.github.gwtbootstrap.client.ui.Button;
import com.github.gwtbootstrap.client.ui.Modal;
import com.github.gwtbootstrap.client.ui.NavLink;
import com.github.gwtbootstrap.client.ui.NavList;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Widget;

import de.fub.agse.meq.mangold.client.interaction.events.LoadWorkspaceFinalEvent;
import de.fub.agse.meq.mangold.client.model.ClientModel;

public class LoadWorkspaceModal extends Composite {

	private static LoadWorkspaceModalUiBinder uiBinder = GWT
			.create(LoadWorkspaceModalUiBinder.class);

	interface LoadWorkspaceModalUiBinder extends
			UiBinder<Widget, LoadWorkspaceModal> {
	}
	
	@UiField
	Modal workspaceModal;
	@UiField
	NavList workspaceList;
	@UiField
	Button loadBtn;
	
	private List<String> workspaces;
	private String workspaceToLoad;

	public LoadWorkspaceModal() {
		initWidget(uiBinder.createAndBindUi(this));
		
		loadBtn.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				if(workspaceToLoad != null) {
					ClientModel.EVENT_BUS.fireEvent(new LoadWorkspaceFinalEvent(workspaceToLoad));					
					workspaceModal.hide();
				}
			}
		});
	}
	
	public void show() {
		workspaceModal.show();
		loadBtn.setVisible(false);
		workspaceToLoad = null;
		workspaces = null;
		workspaceList.clear();
	}
	
	public void show(List<String> workspaces) {
		show();
		setWorkspacesToLoad(workspaces);
	}
	
	public void setWorkspacesToLoad(List<String> workspaces) {
		this.workspaces = workspaces;
		setupWorkspaceList();
	}

	private void setupWorkspaceList() {
		if(this.workspaces != null) {
			NavLink link = null;
			for (final String workspace : workspaces) {
				link = new NavLink(workspace);
				link.addClickHandler(new ClickHandler() {
					public void onClick(ClickEvent event) {
						workspaceToLoad = workspace;
						loadBtn.setText(I18N.Util.get().loadBtn(workspace));
						loadBtn.setVisible(true);
					}
				});
				workspaceList.add(link);
			}
		}
	}
}
