package de.fub.agse.meq.mangold.client;

import java.io.Serializable;
import java.security.MessageDigest;

public class LoginInfo implements Serializable {

	private static final long serialVersionUID = -6757570473718522411L;

	private boolean loggedIn = false;
	private String loginUrl;
	private String logoutUrl;
	private String emailAddress;
	private String nickname;

	public boolean isLoggedIn() {
		return loggedIn;
	}

	public void setLoggedIn(boolean loggedIn) {
		this.loggedIn = loggedIn;
	}

	public String getLoginUrl() {
		return loginUrl;
	}

	public void setLoginUrl(String loginUrl) {
		this.loginUrl = loginUrl;
	}

	public String getLogoutUrl() {
		return logoutUrl;
	}

	public void setLogoutUrl(String logoutUrl) {
		this.logoutUrl = logoutUrl;
	}

	public String getEmailAddress() {
		return emailAddress;
	}

	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}

	public String getNickname() {
		return nickname;
	}

	public void setNickname(String nickname) {
		this.nickname = nickname;
	}
	
	private static String toHexString(byte[] bytes) {
	    char[] hexArray = {'0','1','2','3','4','5','6','7','8','9','a','b','c','d','e','f'};
	    char[] hexChars = new char[bytes.length * 2];
	    int v;
	    for ( int j = 0; j < bytes.length; j++ ) {
	        v = bytes[j] & 0xFF;
	        hexChars[j*2] = hexArray[v/16];
	        hexChars[j*2 + 1] = hexArray[v%16];
	    }
	    return new String(hexChars);
	}
	
	public String getGravatarURL() {
		
		// TODO: default picture URL
		
		String hash; 
		try {
			MessageDigest md = MessageDigest.getInstance("MD5");
			md.reset();
			md.update(emailAddress.trim().toLowerCase().getBytes("ISO-8859-1"));
			hash = toHexString(md.digest());
	    } catch (Exception e) {
	    	hash = "00000000000000000000000000000000";
	    	// TODO: log exception
	    }
		final StringBuffer sb = new StringBuffer("http://www.gravatar.com/avatar/");
		sb.append(hash);
		sb.append("?s=28");
		//sb.append("&d=");
		//sb.append(URL.encode(defaultPictureURL));
		return sb.toString();
	}
}