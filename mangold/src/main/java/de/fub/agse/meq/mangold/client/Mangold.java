package de.fub.agse.meq.mangold.client;

import java.util.List;

import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.RootLayoutPanel;
import com.google.gwt.user.client.ui.Widget;

import de.fub.agse.meq.mangold.client.admin.AdminWorkspaceWidget;
import de.fub.agse.meq.mangold.client.components.Component;
import de.fub.agse.meq.mangold.client.exception.ParseComponentException;
import de.fub.agse.meq.mangold.client.helper.JsonHelper;
import de.fub.agse.meq.mangold.client.interaction.events.UserStatusChangeEvent;
import de.fub.agse.meq.mangold.client.model.ClientModel;
import de.fub.agse.meq.mangold.client.resources.TheResources;
import de.fub.agse.meq.mangold.client.serialization.JsonWrapper;
import de.fub.agse.meq.mangold.client.service.PersistenceServiceAsync;
import de.fub.agse.meq.mangold.client.service.WorkspaceServiceAsync;
import de.fub.agse.meq.mangold.client.ui.helper.AlertifyHelper;
import de.fub.agse.meq.mangold.client.util.LogAdapter;

public class Mangold implements EntryPoint {
	
	private static final LogAdapter LOG = LogAdapter.get(Mangold.class);

	public void onModuleLoad() {

		TheResources.INSTANCE.css().ensureInjected();

		// Make sure we use the whole client area
		Window.setMargin("0px");

		// If user is logging in or out
		UserStatusChangeEvent.register(ClientModel.EVENT_BUS,
				new UserStatusChangeEvent.Handler() {
					public void onUserStatusChange(UserStatusChangeEvent event) {
						showScreen(event.getUserInfo());
					}
				});

		// Check login status using login service.
		WorkspaceServiceAsync.Util.getInstance().relogin(
				new AsyncCallback<UserInfo>() {
					public void onFailure(Throwable caught) {
						LOG.severe("Error trying to relogin: "+caught.getMessage());
					}

					public void onSuccess(UserInfo user) {
						showScreen(user);
					}
				});
	}
	
	protected void showScreen(UserInfo user) {
		ClientModel.getInstance().setUserInfo(user);
		if(RootLayoutPanel.get().getWidgetCount() > 0) {
			Widget w = RootLayoutPanel.get().getWidget(0);
			if(w instanceof WorkspaceWidget) {
				ClientController.getInstance().unregisterEvents();
			}
		}
		RootLayoutPanel.get().clear();
		
		if(user != null && user.isLoggedIn())
			if(user.isAdmin())
				RootLayoutPanel.get().add(loadAdminWorkspace());
			else
				RootLayoutPanel.get().add(loadWorkspace());
		else
			RootLayoutPanel.get().add(loadWelcomeScreen());
	}

	protected WelcomeScreen loadWelcomeScreen() {
		WelcomeScreen welcome = new WelcomeScreen();
		welcome.setWidth(Window.getClientWidth() + "px");
		welcome.setHeight(Window.getClientHeight() + "px");

		return welcome;
	}

	protected WorkspaceWidget loadWorkspace() {
		final ClientController controller = ClientController.getInstance();
		final WorkspaceWidget workspace = controller.createWorkspaceWidget();
		controller.registerEvents();
		PersistenceServiceAsync.Util.getInstance().getComponents(new AsyncCallback<List<JsonWrapper>>() {
			public void onFailure(Throwable caught) {
				AlertifyHelper.showErrorLog(I18N.Util.get().errorFetchingComponents());
				AlertifyHelper.showErrorLog(caught.getLocalizedMessage());
			}
			public void onSuccess(List<JsonWrapper> result) {
				List<Component> components;
				try {
					if(result != null) {
						components = JsonHelper.convertToComponents(result);
						ClientModel.getInstance().setComponents(components);
						AlertifyHelper.showNotificationLog(I18N.Util.get().numberComponentsReturned(result.size()));
					} else {
						AlertifyHelper.showErrorLog(I18N.Util.get().noComponentsReturned());
					}
				} catch (ParseComponentException e) {
					AlertifyHelper.showErrorLog(I18N.Util.get().constructComponentError());
					AlertifyHelper.showErrorLog(e.getLocalizedMessage());
					e.printStackTrace();
				} finally {
					workspace.setupComponents();
				}
			}
		});

		controller.setLoggedIn();
		controller.loadLatest();

		return workspace;
	}
	
	protected AdminWorkspaceWidget loadAdminWorkspace() {
		final AdminWorkspaceWidget workspace = new AdminWorkspaceWidget();
		
		PersistenceServiceAsync.Util.getInstance().getComponents(new AsyncCallback<List<JsonWrapper>>() {
			public void onFailure(Throwable caught) {
				AlertifyHelper.showErrorLog(I18N.Util.get().errorFetchingComponents());
				AlertifyHelper.showErrorLog(caught.getLocalizedMessage());
			}
			public void onSuccess(List<JsonWrapper> result) {
				List<Component> components;
				try {
					if(result != null) {
						components = JsonHelper.convertToComponents(result);
						ClientModel.getInstance().setComponents(components);
						AlertifyHelper.showNotificationLog(I18N.Util.get().numberComponentsReturned(result.size()));
						workspace.setupComponents();
					} else {
						AlertifyHelper.showErrorLog(I18N.Util.get().noComponentsReturned());
					}
				} catch (ParseComponentException e) {
					AlertifyHelper.showErrorLog(I18N.Util.get().constructComponentError());
					AlertifyHelper.showErrorLog(e.getLocalizedMessage());
					e.printStackTrace();
				}
			}
		});
		
		workspace.setWidth(Window.getClientWidth() + "px");
		workspace.setHeight(Window.getClientHeight() + "px");
		
		return workspace;
	}
}
