package de.fub.agse.meq.mangold.client;

import com.github.gwtbootstrap.client.ui.Dropdown;
import com.github.gwtbootstrap.client.ui.Icon;
import com.github.gwtbootstrap.client.ui.Nav;
import com.github.gwtbootstrap.client.ui.NavLink;
import com.github.gwtbootstrap.client.ui.NavText;
import com.github.gwtbootstrap.client.ui.NavWidget;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HTMLPanel;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.Widget;

import de.fub.agse.meq.mangold.client.components.Component;
import de.fub.agse.meq.mangold.client.components.InputComponent;
import de.fub.agse.meq.mangold.client.components.InputOutputComponent;
import de.fub.agse.meq.mangold.client.components.OutputComponent;
import de.fub.agse.meq.mangold.client.components.Presentable;
import de.fub.agse.meq.mangold.client.interaction.events.ChangeEditorToolEvent;
import de.fub.agse.meq.mangold.client.interaction.events.ChangeEditorToolEvent.EditorToolType;
import de.fub.agse.meq.mangold.client.interaction.events.ConfigureBtnClickedEvent;
import de.fub.agse.meq.mangold.client.interaction.events.LoadWorkspaceEvent;
import de.fub.agse.meq.mangold.client.interaction.events.MoveEditorEvent;
import de.fub.agse.meq.mangold.client.interaction.MoveDirection;
import de.fub.agse.meq.mangold.client.interaction.events.NewWorkspaceEvent;
import de.fub.agse.meq.mangold.client.interaction.events.SaveWorkspaceEvent;
import de.fub.agse.meq.mangold.client.interaction.events.UserStatusChangeEvent;
import de.fub.agse.meq.mangold.client.interaction.events.ZoomEditorEvent;
import de.fub.agse.meq.mangold.client.interaction.events.ZoomEditorEvent.Zoom;
import de.fub.agse.meq.mangold.client.interaction.events.action.RemoveEvent;
import de.fub.agse.meq.mangold.client.model.ClientModel;
import de.fub.agse.meq.mangold.client.service.WorkspaceServiceAsync;
import de.fub.agse.meq.mangold.client.ui.DraggableNavLink;

public class MenuBar extends Composite {

	private static MenuBarUiBinder uiBinder = GWT.create(MenuBarUiBinder.class);

	interface MenuBarUiBinder extends UiBinder<Widget, MenuBar> {
	}

	@UiField
	NavText userName;
	@UiField
	Image userImage;
	@UiField
	NavLink logoutLink;
	@UiField
	NavLink removeLink;
	@UiField
	Dropdown selectionBox;
	@UiField
	NavLink configureLink;
	@UiField
	NavLink undoLink;
	@UiField
	NavLink redoLink;
	@UiField
	HTMLPanel sourcesPanel;
	@UiField
	HTMLPanel transformationsPanel;
	@UiField
	HTMLPanel sinksPanel;
	@UiField
	Nav workspaceNav;
	@UiField
	Nav pointerNav;
	@UiField
	NavLink saveLink;
	@UiField
	NavLink loadLink;
	@UiField
	NavLink newWorkspaceLink;
	@UiField
	NavWidget pointer;
	@UiField
	NavWidget remove;
	@UiField
	NavWidget connect;
	@UiField
	Icon connectIcon;
	@UiField
	Icon pointerIcon;
	@UiField
	Icon removeIcon;
	@UiField
	NavWidget moveLeftButton;
	@UiField
	NavWidget moveRightButton;
	@UiField
	NavWidget moveUpButton;
	@UiField
	NavWidget moveDownButton;
	@UiField
	NavWidget zoomInButton;
	@UiField
	NavWidget zoomOutButton;
	
	private FlowController controller;

	public MenuBar() {
		initWidget(uiBinder.createAndBindUi(this));
		initElements();
	}

	private void initElements() {
		workspaceNav.setVisible(!ClientModel.getInstance().getUserInfo().isAdmin());
		pointerNav.setVisible(!ClientModel.getInstance().getUserInfo().isAdmin());
		updateSelectedIconColor(EditorToolType.POINTER);
		
		this.removeLink.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				if (controller != null) {
					Presentable currentSelection = controller
							.getCurrentSelection();
					ClientModel.EVENT_BUS.fireEvent(new RemoveEvent(currentSelection));
				}
			}
		});

		this.logoutLink.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				WorkspaceServiceAsync.Util.getInstance().logout(
						new AsyncCallback<Void>() {
							public void onSuccess(Void result) {
								ClientModel.getInstance().getCurrentWorkspace().clear();
								ClientModel.EVENT_BUS
										.fireEvent(new UserStatusChangeEvent());
							}

							public void onFailure(Throwable caught) {
								// TODO print error
							}
						});
			}
		});
		this.configureLink.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				ClientModel.EVENT_BUS.fireEvent(new ConfigureBtnClickedEvent());
			}
		});
		this.undoLink.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				controller.undoLatestExecution();
			}
		});
		this.redoLink.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				controller.redoLatestUndone();
			}
		});
		this.remove.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				ClientModel.EVENT_BUS.fireEvent(new ChangeEditorToolEvent(EditorToolType.REMOVE));
				updateSelectedIconColor(EditorToolType.REMOVE);
			}
		});
		this.connect.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				ClientModel.EVENT_BUS.fireEvent(new ChangeEditorToolEvent(EditorToolType.CONNECT));
				updateSelectedIconColor(EditorToolType.CONNECT);
			}
		});
		this.pointer.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				ClientModel.EVENT_BUS.fireEvent(new ChangeEditorToolEvent(EditorToolType.POINTER));
				updateSelectedIconColor(EditorToolType.POINTER);
			}
		});
		this.saveLink.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				ClientModel.EVENT_BUS.fireEvent(new SaveWorkspaceEvent());
			}
		});
		this.loadLink.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				ClientModel.EVENT_BUS.fireEvent(new LoadWorkspaceEvent());
			}
		});
		this.newWorkspaceLink.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				ClientModel.EVENT_BUS.fireEvent(new NewWorkspaceEvent());
			}
		});
		this.moveLeftButton.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				ClientModel.EVENT_BUS.fireEvent(new MoveEditorEvent(MoveDirection.LEFT));
			}
		});
		
		this.moveUpButton.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				ClientModel.EVENT_BUS.fireEvent(new MoveEditorEvent(MoveDirection.UP));
			}
		});
		
		this.moveRightButton.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				ClientModel.EVENT_BUS.fireEvent(new MoveEditorEvent(MoveDirection.RIGHT));
			}
		});
		
		this.moveDownButton.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				ClientModel.EVENT_BUS.fireEvent(new MoveEditorEvent(MoveDirection.DOWN));
			}
		});
		
		this.zoomInButton.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				ClientModel.EVENT_BUS.fireEvent(new ZoomEditorEvent(Zoom.IN));
			}
		});

		this.zoomOutButton.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				ClientModel.EVENT_BUS.fireEvent(new ZoomEditorEvent(Zoom.OUT));
			}
		});
	}
	
	private void updateSelectedIconColor(EditorToolType iconType) {
		switch (iconType) {
		case CONNECT:
			pointerIcon.removeStyleName("icon_selected");
			connectIcon.addStyleName("icon_selected");
			removeIcon.removeStyleName("icon_selected");
			break;
		case POINTER:
			pointerIcon.addStyleName("icon_selected");
			connectIcon.removeStyleName("icon_selected");
			removeIcon.removeStyleName("icon_selected");
			break;
		case REMOVE:
			pointerIcon.removeStyleName("icon_selected");
			connectIcon.removeStyleName("icon_selected");
			removeIcon.addStyleName("icon_selected");
			break;
		case INSPECT:
			break;
		default:
			break;
		}
	}

	public void setUsername(String username) {
		this.userName.setText(username);
	}

	public void setUserImageUrl(String url) {
		this.userImage.setUrl(url);
	}

	public void setController(FlowController controller) {
		this.controller = controller;
	}

	public void setSelection(boolean enabled) {
		this.selectionBox.setVisible(enabled);
	}
	
	public void setComponents() {
		for (Component component : ClientModel.getInstance().getComponents()) {
			addComponent(component);
		}
	}

	private void addComponent(Component component) {
		DraggableNavLink componentLink = new DraggableNavLink(component);
		if (component instanceof InputOutputComponent) {
			transformationsPanel.add(componentLink);
		} else if (component instanceof InputComponent) {
			sinksPanel.add(componentLink);
		} else if (component instanceof OutputComponent) {
			sourcesPanel.add(componentLink);
		}		
	}

	public void setUndoEnabled(boolean enabled) {
		this.undoLink.setDisabled(!enabled);
	}

	public void setRedoEnabled(boolean enabled) {
		this.redoLink.setDisabled(!enabled);
	}
}
