package de.fub.agse.meq.mangold.client;

import java.util.ArrayList;
import java.util.List;

import com.github.gwtbootstrap.client.ui.Button;
import com.github.gwtbootstrap.client.ui.Modal;
import com.github.gwtbootstrap.client.ui.WellForm;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.json.client.JSONObject;
import com.google.gwt.json.client.JSONString;
import com.google.gwt.json.client.JSONValue;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Widget;

import de.fub.agse.meq.mangold.client.interaction.events.PropertySaveEvent;
import de.fub.agse.meq.mangold.client.model.ClientModel;
import de.fub.agse.meq.mangold.client.ui.helper.AlertifyHelper;

public class PropertyEditorModal extends Composite {

	private static PropertyEditorModalUiBinder uiBinder = GWT
			.create(PropertyEditorModalUiBinder.class);

	interface PropertyEditorModalUiBinder extends
			UiBinder<Widget, PropertyEditorModal> {
	}
	
	@UiField
	Modal propertyModal;
	@UiField
	WellForm propertiesPanel;
	@UiField
	Button saveBtn;
	
	private List<PropertyFormElement> formElements;

	public PropertyEditorModal() {
		this.formElements = new ArrayList<PropertyFormElement>();
		initWidget(uiBinder.createAndBindUi(this));
		saveBtn.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				ClientModel.EVENT_BUS.fireEvent(new PropertySaveEvent());
				propertyModal.hide();
			}
		});
	}
	
	public void show() {
		propertyModal.show();
	}
	
	public void setConfiguration(Configuration configuration) {
		this.propertiesPanel.clear();
		this.formElements.clear();
		for (String key : configuration.keySet()) {
			if(!Configuration.isEditableConfigurationKey(key))
				continue;
			if (key.equalsIgnoreCase("input")) continue;
			if (key.equalsIgnoreCase("output")) continue;
			JSONValue value = configuration.get(key);
			JSONString str = null;
			JSONObject obj = null;
			if((str = value.isString()) != null) {
				addFormRow(key, str.stringValue());
			} else if((obj = value.isObject()) != null) {
				addFormRow(key, obj);
			}
			
		}
	}
	
	private void addFormRow(String key, String value) {
		PropertyFormElement element = new PropertyFormElement(key, value);
		this.propertiesPanel.add(element);
		this.formElements.add(element);
	}
	
	private void addFormRow(String key, JSONObject obj) {
		PropertyFormElement element = new PropertyFormElement(key, obj);
		this.propertiesPanel.add(element);
		this.formElements.add(element);
	}
	
	public Configuration getConfiguration() {
		JSONObject obj = new JSONObject();
		for(PropertyFormElement element : this.formElements) {
			obj.put(element.getLabel(), new JSONString(element.getValue()));
		}
		return new Configuration(obj);
	}

}
