package de.fub.agse.meq.mangold.client;

import com.github.gwtbootstrap.client.ui.HelpBlock;
import com.github.gwtbootstrap.client.ui.ListBox;
import com.github.gwtbootstrap.client.ui.TextBox;
import com.github.gwtbootstrap.client.ui.base.InlineLabel;
import com.google.gwt.core.client.GWT;
import com.google.gwt.json.client.JSONArray;
import com.google.gwt.json.client.JSONObject;
import com.google.gwt.json.client.JSONString;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Widget;

import de.fub.agse.meq.mangold.client.Configurable.TypeConfiguration;
import de.fub.agse.meq.mangold.server.SystemProperties;

public class PropertyFormElement extends Composite {

	private static PropertyFormElementUiBinder uiBinder = GWT
			.create(PropertyFormElementUiBinder.class);

	interface PropertyFormElementUiBinder extends
			UiBinder<Widget, PropertyFormElement> {
	}

	@UiField
	InlineLabel label;
	@UiField
	TextBox txtBox;
	@UiField
	ListBox dropdown;
	@UiField
	HelpBlock helpBlock;

	private final String TYPE_TEXTBOX = "textbox";
	private final String TYPE_CHECKBOX = "checkbox";
	private final String TYPE_DROPDOWN = "dropdown";

	private String value;
	private JSONArray values;

	public PropertyFormElement() {
		initWidget(uiBinder.createAndBindUi(this));
	}

	public PropertyFormElement(String label, String value) {
		this();
		setFormData(label, value);
	}

	public PropertyFormElement(String label, JSONObject obj) {
		this();
		setFormData(label, obj);
	}

	/**
	 * type == textbox
	 * 
	 * @param label
	 * @param value
	 */
	public void setFormData(String label, String value) {
		this.label.setText(label);
		this.value = value;
		setupTypeElement(TypeConfiguration.TEXTBOX);
	}

	public void setFormData(String label, JSONObject obj) {
		this.label.setText(label);
		String typeStr = null;
		if (obj.containsKey(SystemProperties.COMPONENT_TYPE_FIELD)) {
			JSONString str = obj.get(SystemProperties.COMPONENT_TYPE_FIELD)
					.isString();
			if (str != null) {
				typeStr = str.stringValue();
			}
		}
		if (typeStr == null)
			typeStr = TYPE_TEXTBOX;

		TypeConfiguration type = null;
		if (TYPE_TEXTBOX.equals(typeStr)) {
			type = TypeConfiguration.TEXTBOX;
			if (obj.containsKey(SystemProperties.COMPONENT_VALUE_FIELD)) {
				JSONString valueStr = obj.get(
						SystemProperties.COMPONENT_VALUE_FIELD).isString();
				if (valueStr != null) {
					this.value = valueStr.stringValue();
				}
			}
			// } else if(TYPE_CHECKBOX.equals(typeStr)) {
			// type = TypeConfiguration.CHECKBOX;
			// JSONArray valuesArr = obj.get("values").isArray();
			// if(valuesArr != null) {
			// this.values = valuesArr;
			// }
			// } else if(TYPE_DROPDOWN.equals(typeStr)) {
			// type = TypeConfiguration.DROPDOWN;
			// JSONArray valuesArr = obj.get("values").isArray();
			// if(valuesArr != null) {
			// this.values = valuesArr;
			// }
		}

		if (type == null)
			return;

		setupTypeElement(type);
	}

	private void setupTypeElement(TypeConfiguration type) {
		if (type == null) {
			return;
		}
		switch (type) {
		case TEXTBOX:
			this.txtBox.setText(value);
			txtBox.setVisible(true);
			break;
		case CHECKBOX:
			// TODO handle Checkboxes
			break;
		case DROPDOWN:
			for (int i = 0; i < this.values.size(); i++) {
				JSONString str = null;
				if ((str = this.values.get(i).isString()) != null) {
					this.dropdown.setValue(i, str.stringValue());
				}
			}
			this.dropdown.setVisible(true);
			break;
		default:
			break;
		}
	}

	public String getLabel() {
		return this.label.getText();
	}

	public String getValue() {
		return this.txtBox.getText();
	}
}
