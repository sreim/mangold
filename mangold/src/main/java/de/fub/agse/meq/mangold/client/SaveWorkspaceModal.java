package de.fub.agse.meq.mangold.client;

import com.github.gwtbootstrap.client.ui.Button;
import com.github.gwtbootstrap.client.ui.Modal;
import com.github.gwtbootstrap.client.ui.TextBox;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.KeyCodes;
import com.google.gwt.event.dom.client.KeyPressEvent;
import com.google.gwt.event.dom.client.KeyPressHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Widget;

import de.fub.agse.meq.mangold.client.interaction.events.SaveWorkspaceFinalEvent;
import de.fub.agse.meq.mangold.client.model.ClientModel;
import de.fub.agse.meq.mangold.server.SystemProperties;

public class SaveWorkspaceModal extends Composite {

	private static SaveWorkspaceModalUiBinder uiBinder = GWT
			.create(SaveWorkspaceModalUiBinder.class);

	interface SaveWorkspaceModalUiBinder extends
			UiBinder<Widget, SaveWorkspaceModal> {
	}
	
	@UiField
	Modal workspaceModal;
	@UiField
	Button saveBtn;
	@UiField
	TextBox workspaceNameTb;

	public SaveWorkspaceModal() {
		initWidget(uiBinder.createAndBindUi(this));
		
		saveBtn.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				save();
			}
		});
		
		workspaceNameTb.addKeyPressHandler(new KeyPressHandler() {
			public void onKeyPress(KeyPressEvent event) {
				if(KeyCodes.KEY_ENTER == event.getUnicodeCharCode())
					save();
			}
		});
	}
	
	private void save() {
		if(workspaceNameTb.getText() != null && !workspaceNameTb.getText().isEmpty()) {
			ClientModel.getInstance().getCurrentWorkspace().setName(workspaceNameTb.getText());
			ClientModel.EVENT_BUS.fireEvent(new SaveWorkspaceFinalEvent());
			workspaceModal.hide();
		}
	}
	
	public void show() {
		workspaceModal.show();
		workspaceNameTb.setFocus(true);
		workspaceNameTb.setCursorPos(0);
		
		String workspaceName = ClientModel.getInstance().getCurrentWorkspace().getName();
		if(!SystemProperties.WORKSPACE_DEFAULT_NAME.equals(workspaceName))
			workspaceNameTb.setText(workspaceName);
	}
}
