package de.fub.agse.meq.mangold.client;

public enum ToolTypes {
	POINT,
	REMOVE,
	CONNECT, 
	INSPECT
}
