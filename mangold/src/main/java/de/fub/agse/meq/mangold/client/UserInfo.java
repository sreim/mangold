package de.fub.agse.meq.mangold.client;

import java.io.Serializable;

public class UserInfo implements Serializable {

	private static final long serialVersionUID = -5030855156433317777L;
	
	private String mailAddress;
	private boolean loggedIn = false;
	private boolean admin = false;
	
	public UserInfo() {
	}

	public String getMailAddress() {
		return mailAddress;
	}

	public void setMailAddress(String mailAddress) {
		this.mailAddress = mailAddress;
	}

	public boolean isLoggedIn() {
		return loggedIn;
	}

	public void setLoggedIn(boolean loggedIn) {
		this.loggedIn = loggedIn;
	}

	public boolean isAdmin() {
		return admin;
	}

	public void setAdmin(boolean admin) {
		this.admin = admin;
	}

}
