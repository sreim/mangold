package de.fub.agse.meq.mangold.client;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.KeyCodes;
import com.google.gwt.event.dom.client.KeyUpEvent;
import com.google.gwt.event.dom.client.KeyUpHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.PasswordTextBox;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.Widget;

import de.fub.agse.meq.mangold.client.interaction.events.UserStatusChangeEvent;
import de.fub.agse.meq.mangold.client.model.ClientModel;
import de.fub.agse.meq.mangold.client.service.WorkspaceServiceAsync;

public class WelcomeScreen extends Composite implements KeyUpHandler {

	private static WelcomeMessageUiBinder uiBinder = GWT
			.create(WelcomeMessageUiBinder.class);

	interface WelcomeMessageUiBinder extends UiBinder<Widget, WelcomeScreen> {
	}
	
	@UiField TextBox loginMail;
	@UiField PasswordTextBox loginPassword;
	@UiField Button submitBtn;
	@UiField Label errorLabel;

	public WelcomeScreen() {
		initWidget(uiBinder.createAndBindUi(this));
		
		loginMail.getElement().setAttribute("placeholder", I18N.Util.get().mailAddress());
		loginPassword.getElement().setAttribute("placeholder", I18N.Util.get().password());
		submitBtn.removeStyleName("gwt-Button");
		
		submitBtn.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				login();
			}
		});
		
		loginMail.addKeyUpHandler(this);
		loginPassword.addKeyUpHandler(this);
	}
	
	@Override
	public void onKeyUp(KeyUpEvent event) {
		if(event.getNativeKeyCode() == KeyCodes.KEY_ENTER) {
			login();
		}
	}

	protected void login() {
		if(!loginMail.getText().isEmpty() && !loginPassword.getText().isEmpty()) {
			WorkspaceServiceAsync.Util.getInstance().login(loginMail.getText(), loginPassword.getText(), true, new AsyncCallback<UserInfo>() {
				public void onFailure(Throwable caught) {
					showSystemWarning(I18N.Util.get().systemError());
				}
				public void onSuccess(UserInfo result) {
					if(result.isLoggedIn()) {
						showSystemWarning(false);
						ClientModel.EVENT_BUS.fireEvent(new UserStatusChangeEvent(result));
						
					} else {
						showSystemWarning(I18N.Util.get().invalidLogin());
					}
				}
			});
		} else {
			showSystemWarning(I18N.Util.get().invalidLogin());
		}
	}

	protected void showSystemWarning(boolean show) {
		this.errorLabel.setVisible(show);
	}
	
	protected void showSystemWarning(String warning) {
		if(!warning.isEmpty()) {
			this.errorLabel.setText(warning);
			this.showSystemWarning(true);
		} else
			this.showSystemWarning(false);
	}

}
