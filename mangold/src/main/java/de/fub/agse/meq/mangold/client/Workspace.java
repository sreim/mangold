package de.fub.agse.meq.mangold.client;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import net.edzard.kinetic.Vector2d;

import com.google.gwt.json.client.JSONArray;
import com.google.gwt.json.client.JSONObject;
import com.google.gwt.json.client.JSONString;
import com.google.gwt.user.client.Timer;

import de.fub.agse.meq.mangold.client.interaction.events.ComponentPositionChangedEvent;
import de.fub.agse.meq.mangold.client.interaction.events.SaveWorkspaceDeltaEvent;
import de.fub.agse.meq.mangold.client.model.ClientModel;
import de.fub.agse.meq.mangold.client.serialization.JsonElement;
import de.fub.agse.meq.mangold.client.serialization.JsonWrapper;
import de.fub.agse.meq.mangold.server.SystemProperties;

public class Workspace implements Serializable {

	private static final long serialVersionUID = 1737949106727766626L;

	private String name;
	private Vector2d offset;
	private double zoom;
	private int deltaCalls;

	private static final int DELTAS_UNTIL_SAVE = 1;
	private static final int DELTA_CHECK_TIME = 3000;

	private Map<String, Configurable> configurables;
	private Map<String, Configurable> configurablesAdded;
	private Map<String, Configurable> configurablesRemoved;
	private Map<String, Configurable> configurablesChanged;

	public Workspace() {
		this(SystemProperties.WORKSPACE_DEFAULT_NAME);
		Timer t = new Timer() {
			public void run() {
				if (deltaCalls >= DELTAS_UNTIL_SAVE) {
					ClientModel.EVENT_BUS.fireEvent(new SaveWorkspaceDeltaEvent());
				}
			}
		};
		t.scheduleRepeating(DELTA_CHECK_TIME);
	}

	public Workspace(String name) {
		this.name = name;
		this.deltaCalls = 0;
		this.configurables = new HashMap<String, Configurable>();
		this.configurablesAdded = new HashMap<String, Configurable>();
		this.configurablesRemoved = new HashMap<String, Configurable>();
		this.configurablesChanged = new HashMap<String, Configurable>();
		this.offset = new Vector2d(0.0, 0.0);
		this.zoom = 1.0;

		ComponentPositionChangedEvent.register(ClientModel.EVENT_BUS,
				new ComponentPositionChangedEvent.Handler() {
					public void onPositionChanged(
							ComponentPositionChangedEvent event) {
						addAndRemark(event.getComponent());
					}
				});
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void addAndRemark(Configurable configurable) {
		if (!SystemProperties.WORKSPACE_DEFAULT_NAME.equals(this.name)) {
			addToDelta(configurable);
			incrementDeltaCalls();
		}
		add(configurable);

	}

	public void addConnectionAndRemark(Configurable connection, Configurable scp,
			Configurable tcp) {
		if (!SystemProperties.WORKSPACE_DEFAULT_NAME.equals(this.name)) {
			this.addToDelta(connection);
			this.addToDelta(scp);
			this.addToDelta(tcp);
			incrementDeltaCalls();
		}
		this.add(connection);
		this.add(scp);
		this.add(tcp);
	}

	protected void addToDelta(Configurable configurable) {
		if (configurables.containsKey(configurable.getId()))
			configurablesChanged.put(configurable.getId(), configurable);
		else
			configurablesAdded.put(configurable.getId(), configurable);
	}

	private void add(Configurable configurable) {
		configurables.put(configurable.getId(), configurable);
	}

	public void addStateless(Configurable configurable) {
		configurables.put(configurable.getId(), configurable);
	}

	private void incrementDeltaCalls() {
		// count delta only if there is a workspace set already
		if (!SystemProperties.WORKSPACE_DEFAULT_NAME.equals(this.name)) {
			this.deltaCalls++;
		}
	}

	public boolean remove(Configurable configurable) {
		if (configurables.containsKey(configurable.getId()))
			configurablesRemoved.put(configurable.getId(), configurable);
		incrementDeltaCalls();
		return configurables.remove(configurable.getId()) != null;
	}

	public Configurable get(String id) {
		return configurables.get(id);
	}

	public List<Configurable> getAllConfigurables() {
		return new ArrayList<Configurable>(configurables.values());
	}

	public boolean contains(String id) {
		return configurables.containsKey(id);
	}

	public JSONObject toJson() {
		JSONObject workspace = new JSONObject();
		workspace.put("name", new JSONString(name));
		JSONArray configurablesArray = new JSONArray();
		int i = 0;
		for (Configurable thing : configurables.values()) {
			configurablesArray.set(i++, thing.getConfiguration().toJson());
		}
		workspace.put(SystemProperties.WORKSPACE_THINGS, configurablesArray);
		return workspace;
	}

	public Map<String, Map<String, JsonWrapper>> getDelta() {
		Map<String, Map<String, JsonWrapper>> delta = new HashMap<String, Map<String, JsonWrapper>>();
		delta.put(SystemProperties.WORKSPACE_DELTA_REMOVED,
				new HashMap<String, JsonWrapper>());
		delta.put(SystemProperties.WORKSPACE_DELTA_ADDED,
				new HashMap<String, JsonWrapper>());
		delta.put(SystemProperties.WORKSPACE_DELTA_CHANGED,
				new HashMap<String, JsonWrapper>());
		for (String key : configurablesAdded.keySet()) {
			delta.get(SystemProperties.WORKSPACE_DELTA_ADDED).put(
					key,
					new JsonElement(configurablesAdded.get(key)
							.getConfiguration().toJson().toString()));
		}
		for (String key : configurablesRemoved.keySet()) {
			delta.get(SystemProperties.WORKSPACE_DELTA_REMOVED).put(
					key,
					new JsonElement(configurablesRemoved.get(key)
							.getConfiguration().toJson().toString()));
		}
		for (String key : configurablesChanged.keySet()) {
			delta.get(SystemProperties.WORKSPACE_DELTA_CHANGED).put(
					key,
					new JsonElement(configurablesChanged.get(key)
							.getConfiguration().toJson().toString()));
		}
		configurablesAdded.clear();
		configurablesRemoved.clear();
		configurablesChanged.clear();
		deltaCalls = 0;
		
		return delta;
	}

	public void clear() {
		this.configurables.clear();
		this.configurablesAdded.clear();
		this.configurablesChanged.clear();
		this.configurablesRemoved.clear();
		deltaCalls = 0;
		setName(SystemProperties.WORKSPACE_DEFAULT_NAME);
	}

	public Vector2d getOffset() {
		return offset;
	}

	public void setOffset(Vector2d offset) {
		if (this.offset.x != offset.x || this.offset.y != offset.y) {
			this.offset = offset;
			incrementDeltaCalls();
		}
	}

	public double getZoom() {
		return zoom;
	}

	public void setZoom(double zoom) {
		if (this.zoom != zoom) {
			this.zoom = zoom;
			incrementDeltaCalls();
		}
	}
}
