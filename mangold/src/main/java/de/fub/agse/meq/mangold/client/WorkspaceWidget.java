package de.fub.agse.meq.mangold.client;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.logical.shared.ResizeEvent;
import com.google.gwt.event.logical.shared.ResizeHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.LayoutPanel;
import com.google.gwt.user.client.ui.ResizeComposite;
import com.google.gwt.user.client.ui.SplitLayoutPanel;

import de.fub.agse.meq.mangold.client.components.Presentable;
import de.fub.agse.meq.mangold.client.ui.DroppableEditorView;
import de.fub.agse.meq.mangold.client.util.LogAdapter;

public class WorkspaceWidget extends ResizeComposite {

	interface WorkspaceWidgetUiBinder extends
			UiBinder<LayoutPanel, WorkspaceWidget> {
	}

	private static WorkspaceWidgetUiBinder uiBinder = GWT
			.create(WorkspaceWidgetUiBinder.class);

	MenuBar workspaceMenu;
	@UiField
	MenuBar menuBar;

	@UiField
	LayoutPanel layoutPanel;
	private final LayoutPanel applicationArea;
	@UiField
	SplitLayoutPanel horizontalSplitPanel;
	@UiField
	DroppableEditorView editor;
	@UiField
	PropertyEditorModal propertyEditor;
	@UiField
	SaveWorkspaceModal saveWorkspaceModal;
	@UiField
	LoadWorkspaceModal loadWorkspaceModal;
	
	SplitLayoutPanel verticalSplitPanel;
	
	private static final LogAdapter LOG = LogAdapter.get(WorkspaceWidget.class);

	public WorkspaceWidget() {
		this.applicationArea = uiBinder.createAndBindUi(this);
		initWidget(this.applicationArea);

		Window.addResizeHandler(new ResizeHandler() {
			public void onResize(ResizeEvent event) {
				applicationArea.setWidth(Window.getClientWidth() + "px");
				applicationArea.setHeight(Window.getClientHeight() + "px");
			}
		});
	}

	public EditorView getEditorView() {
		return editor.getOriginalWidget();
	}

	void updateSelectionState() {

		Presentable selection = ClientController.getInstance().getFlowController().getCurrentSelection();
		if (selection != null)
			menuBar.setSelection(true);
		else
			menuBar.setSelection(false);
	}

	public void setupComponents() {
		this.menuBar.setComponents();
	}
	
	public void showPropertyEditor() {
		this.propertyEditor.setConfiguration(ClientController.getInstance().getFlowController().getCurrentSelection()
				.getConfiguration());
		this.propertyEditor.show();
	}
}
