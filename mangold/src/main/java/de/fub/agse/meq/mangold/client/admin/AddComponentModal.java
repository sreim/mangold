package de.fub.agse.meq.mangold.client.admin;

import com.github.gwtbootstrap.client.ui.Accordion;
import com.github.gwtbootstrap.client.ui.AccordionGroup;
import com.github.gwtbootstrap.client.ui.Button;
import com.github.gwtbootstrap.client.ui.Modal;
import com.github.gwtbootstrap.client.ui.WellForm;
import com.github.gwtbootstrap.client.ui.constants.ButtonType;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.json.client.JSONObject;
import com.google.gwt.json.client.JSONString;
import com.google.gwt.json.client.JSONValue;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Widget;

import de.fub.agse.meq.mangold.client.ComponentType;
import de.fub.agse.meq.mangold.client.Configuration;
import de.fub.agse.meq.mangold.client.ConfigurationNode;
import de.fub.agse.meq.mangold.client.I18N;
import de.fub.agse.meq.mangold.client.admin.field.SetupComponentsNameElement;
import de.fub.agse.meq.mangold.client.admin.field.SetupComponentsTypeElement;
import de.fub.agse.meq.mangold.client.component.ComponentFactory;
import de.fub.agse.meq.mangold.client.components.Component;
import de.fub.agse.meq.mangold.client.exception.ComponentCreationException;
import de.fub.agse.meq.mangold.client.exception.ConfigurationCreationException;
import de.fub.agse.meq.mangold.client.interaction.events.AddNewComponentEvent;
import de.fub.agse.meq.mangold.client.model.ClientModel;
import de.fub.agse.meq.mangold.client.ui.helper.AlertifyHelper;
import de.fub.agse.meq.mangold.server.SystemProperties;

public class AddComponentModal extends Composite {

	private static AddComponentModalUiBinder uiBinder = GWT
			.create(AddComponentModalUiBinder.class);

	interface AddComponentModalUiBinder extends
			UiBinder<Widget, AddComponentModal> {
	}
	
	@UiField
	Modal addComponentModal;
	@UiField
	WellForm componentPanel;
	@UiField
	Button addBtn;
	
	private ComponentType componentType;
	private ConfigurationNode configTree;
	
	private Component component;

	public AddComponentModal() {
		initWidget(uiBinder.createAndBindUi(this));
		
		this.addBtn.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				add();
			}
		});
	}
	
	private void add() {
		try {
			component.setFieldConfiguration(getConfigFromTree());
			ClientModel.EVENT_BUS.fireEvent(new AddNewComponentEvent(component));
			hide();
		} catch (ConfigurationCreationException e) {
			AlertifyHelper.showErrorLog(I18N.Util.get().errorAddingComponent());
		}
	}
	
	private void hide() {
		componentPanel.clear();
		addComponentModal.hide();
	}
	
	private void setUpPanel() throws ComponentCreationException {
		componentPanel.clear();
		if(componentType == null) {
			AlertifyHelper.showErrorLog(I18N.Util.get().systemError());
			return;
		}
		
		switch (componentType) {
		case SOURCE:
			this.addComponentModal.setTitle(I18N.Util.get().addSource());
			break;
		case TRANSFORMATION:
			this.addComponentModal.setTitle(I18N.Util.get().addTransformation());
			break;
		case SINK:
			this.addComponentModal.setTitle(I18N.Util.get().addSink());
			break;
		}
		
		component = ComponentFactory.createNewInstanceOfComponent(componentType);
		setupFieldConfigurationTab(component);
	}

	public void show(ComponentType type) {
		this.componentType = type;
		try {
			setUpPanel();
			addComponentModal.show();
		} catch (ComponentCreationException e) {
			AlertifyHelper.showErrorLog(I18N.Util.get().constructComponentError());
		}
	}
	
	public void setupFieldConfigurationTab(Component component) {
		Accordion subAccordion = new Accordion();
		configTree = new ConfigurationNode();
		boolean subAccordionUsed = false;
		
		for (String key : component.getFieldConfiguration().keySet()) {
			if(Configuration.isEditableConfigurationKey(key)) {
				JSONValue value = component.getFieldConfiguration().get(key);
				JSONString str = null;
				JSONObject obj = null;
				if((str = value.isString()) != null) {					
					SetupComponentsNameElement elem = setupElement(key, str.stringValue());
					if(!SystemProperties.INPUT_NAME.equals(key)
							&& !SystemProperties.OUTPUT_NAME.equals(key))
						componentPanel.add(elem);
				} else if((obj = value.isObject()) != null) {
					AccordionGroup grp = setupElement(key, obj);
					if(!SystemProperties.INPUT_NAME.equals(key)
							&& !SystemProperties.OUTPUT_NAME.equals(key)) {
						subAccordion.add(grp);
						subAccordionUsed = true;
					}
				}
			}
		}
		if(subAccordionUsed)
			componentPanel.add(subAccordion);
		
	}
	
	private SetupComponentsNameElement setupElement(String key, String value) {
		SetupComponentsNameElement element = new SetupComponentsNameElement(key, value);
		configTree.addNode(new ConfigurationNode(element));
		return element;
	}
	
	private AccordionGroup setupElement(String key, JSONObject value) {
		AccordionGroup subGroup = new AccordionGroup();
		subGroup.setHeading(key);
		
		ConfigurationNode configNode = new ConfigurationNode(key);
		configTree.addNode(configNode);
		
		String typeStr = null;
		JSONValue jsonTypeVal = value.get("type");
		if(jsonTypeVal != null) {
			JSONString jsonType = jsonTypeVal.isString();
			if(jsonType != null) {
				typeStr = jsonType.stringValue();
			}
		}
		SetupComponentsTypeElement typeLine = new SetupComponentsTypeElement(typeStr);
		configNode.addNode(new ConfigurationNode(typeLine));
		subGroup.add(typeLine);
		
		for (String valKey : value.keySet()) {
			if(!valKey.equals("type")) {
				SetupComponentsNameElement elem = new SetupComponentsNameElement(valKey, value.get(valKey).isString().stringValue());
				configNode.addNode(new ConfigurationNode(elem));
				subGroup.add(elem);
				
			}
		}
		Button removeBtn = new Button();
		removeBtn.setType(ButtonType.DANGER);
		removeBtn.setText(I18N.Util.get().remove());
		subGroup.add(removeBtn);
		return subGroup;
	}
	
	private Configuration getConfigFromTree()
			throws ConfigurationCreationException {
		JSONValue val = configTree.getJson();
		if (val == null)
			throw new ConfigurationCreationException(
					"getJson returned invalid object");
		JSONObject obj = val.isObject();
		if (obj != null) {
			return new Configuration(obj);
		} else {
			throw new ConfigurationCreationException(
					"getJson returned invalid object");
		}
	}

}
