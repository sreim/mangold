package de.fub.agse.meq.mangold.client.admin;

import com.google.gwt.core.client.GWT;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Widget;

import de.fub.agse.meq.mangold.client.MenuBar;
import de.fub.agse.meq.mangold.client.model.ClientModel;

public class AdminWorkspaceWidget extends Composite {

	private static AdminWorkspaceWidgetUiBinder uiBinder = GWT
			.create(AdminWorkspaceWidgetUiBinder.class);

	interface AdminWorkspaceWidgetUiBinder extends
			UiBinder<Widget, AdminWorkspaceWidget> {
	}
	
	@UiField
	SetupComponentsWizard componentsWizard;
	@UiField
	MenuBar menuBar;

	public AdminWorkspaceWidget() {
		initWidget(uiBinder.createAndBindUi(this));
		ClientModel model = ClientModel.getInstance();
		menuBar.setUsername(model.getUserInfo().getMailAddress());
	}
	
	public void setupComponents() {
		componentsWizard.setupComponents();
	}

}
