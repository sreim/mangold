package de.fub.agse.meq.mangold.client.admin;

import com.google.gwt.user.client.ui.Composite;

import de.fub.agse.meq.mangold.client.Configuration;
import de.fub.agse.meq.mangold.client.components.Component;

public abstract class ConfigPanel extends Composite {
	
	protected Component component;
	protected int panelHash;

	public ConfigPanel() {
		
	}
	
	public ConfigPanel(int panelHash, Component component) {
		this();
		this.component = component;
		this.panelHash = panelHash;
	}
	
	public abstract void onShow();
	
	public void updateComponent() {
	}
	
	public abstract Configuration getConfiguration();
}
