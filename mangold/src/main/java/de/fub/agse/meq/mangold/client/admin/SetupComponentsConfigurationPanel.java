package de.fub.agse.meq.mangold.client.admin;

import java.util.HashMap;
import java.util.Map;

import com.github.gwtbootstrap.client.ui.Tab;
import com.github.gwtbootstrap.client.ui.TabPanel;
import com.github.gwtbootstrap.client.ui.TabPanel.ShowEvent;
import com.github.gwtbootstrap.client.ui.TabPanel.ShownEvent;
import com.google.gwt.core.client.GWT;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Widget;

import de.fub.agse.meq.mangold.client.Configuration;
import de.fub.agse.meq.mangold.client.admin.field.FieldConfigPanel;
import de.fub.agse.meq.mangold.client.admin.json.JsonConfigPanel;
import de.fub.agse.meq.mangold.client.admin.process.ProcessConfigPanel;
import de.fub.agse.meq.mangold.client.components.Component;
import de.fub.agse.meq.mangold.client.interaction.events.ComponentsConfigurationProcessedEvent;
import de.fub.agse.meq.mangold.client.interaction.events.FieldConfigurationChangedEvent;
import de.fub.agse.meq.mangold.client.interaction.events.SaveComponentsEvent;
import de.fub.agse.meq.mangold.client.model.ClientModel;
import de.fub.agse.meq.mangold.server.SystemProperties;

public class SetupComponentsConfigurationPanel extends Composite {

	private static SetupComponentsConfigurationPanelUiBinder uiBinder = GWT
			.create(SetupComponentsConfigurationPanelUiBinder.class);

	interface SetupComponentsConfigurationPanelUiBinder extends
			UiBinder<Widget, SetupComponentsConfigurationPanel> {
	}

	@UiField
	Tab fieldConfigTab;
	@UiField
	Tab componentBehaviourTabInput;
	@UiField
	Tab componentBehaviourTabOutput;
	@UiField
	Tab editJsonTab;
	@UiField
	TabPanel tabPanel;


	private static enum Panel {
		FIELD, PROCESS_INPUT, PROCESS_OUTPUT, JSON;
	}
	
	private final static int FIELD_PANEL_INDEX = 0;
	private final static int PROCESS_PANEL_INPUT_INDEX = 1;
	private final static int PROCESS_PANEL_OUTPUT_INDEX = 2;
	private final static int JSON_PANEL_INDEX = 3;

	private Map<Panel, ConfigPanel> panels;
	private int panelHash;

	private Component component;

	public SetupComponentsConfigurationPanel(final int panelHash) {
		this.panelHash = panelHash;
		panels = new HashMap<Panel, ConfigPanel>();

		initWidget(uiBinder.createAndBindUi(this));

		tabPanel.addShownHandler(new TabPanel.ShownEvent.Handler() {
			public void onShow(ShownEvent shownEvent) {
				String targetText = shownEvent.getTarget().getText();
				if (targetText != null) {
					if (targetText.equals(fieldConfigTab.getHeading())) {
						panels.get(Panel.FIELD).onShow();
					} else if (targetText.equals(componentBehaviourTabInput
							.getHeading())) {
						panels.get(Panel.PROCESS_INPUT).onShow();
					} else if (targetText.equals(componentBehaviourTabOutput
							.getHeading())) {
						panels.get(Panel.PROCESS_OUTPUT).onShow();
					} else if (targetText.equals(editJsonTab.getHeading())) {
						panels.get(Panel.JSON).onShow();
					}
				}
			}
		});

		tabPanel.addShowHandler(new TabPanel.ShowEvent.Handler() {
			public void onShow(ShowEvent showEvent) {
				String relatedTargetText = showEvent.getRelatedTarget()
						.getText();
				if (relatedTargetText != null) {
					if (relatedTargetText.equals(fieldConfigTab.getHeading())) {
						Configuration config = panels.get(Panel.FIELD)
								.getConfiguration();
						component.setFieldConfiguration(config);
					} else if (relatedTargetText.equals(editJsonTab
							.getHeading())) {
						Configuration config = panels.get(Panel.JSON)
								.getConfiguration();
						component.setFieldConfiguration(config);
					} else if (relatedTargetText.equals(componentBehaviourTabInput
							.getHeading())) {
						Configuration config = panels.get(Panel.PROCESS_INPUT)
								.getConfiguration();
						component.setFieldConfiguration(config);
					} else if (relatedTargetText.equals(componentBehaviourTabOutput
							.getHeading())) {
						Configuration config = panels.get(Panel.PROCESS_OUTPUT)
								.getConfiguration();
						component.setFieldConfiguration(config);
					}
					updateConfig();
				}
			}
		});

		FieldConfigurationChangedEvent.register(ClientModel.EVENT_BUS,
				new FieldConfigurationChangedEvent.Handler() {
					public void onFieldConfigChanged(
							FieldConfigurationChangedEvent event) {
						if (panelHash != event.getPanelHash())
							return;
						// TODO validate json
						Configuration config = panels.get(Panel.FIELD)
								.getConfiguration();
						component.setFieldConfiguration(config);

						updateConfig();
					}
				});
		
		SaveComponentsEvent.register(ClientModel.EVENT_BUS, new SaveComponentsEvent.Handler() {
			public void onSaveComponent(SaveComponentsEvent event) {
				//TODO Index anders, wenn ein Panel fehlt?
				int selectedTabIndex = tabPanel.getSelectedTab();
				Configuration config = null;
				switch (selectedTabIndex) {
				case FIELD_PANEL_INDEX:
					config = panels.get(Panel.FIELD).getConfiguration();
					component.setFieldConfiguration(config);
					break;
				case PROCESS_PANEL_INPUT_INDEX:
					config = panels.get(Panel.PROCESS_INPUT).getConfiguration();
					component.setFieldConfiguration(config);
					break;
				case PROCESS_PANEL_OUTPUT_INDEX:
					config = panels.get(Panel.PROCESS_OUTPUT).getConfiguration();
					component.setFieldConfiguration(config);
					break;
				case JSON_PANEL_INDEX:
					config = panels.get(Panel.JSON).getConfiguration();
					component.setFieldConfiguration(config);
					break;
				default:
					break;
				}
				
				ClientModel.EVENT_BUS
				.fireEvent(new ComponentsConfigurationProcessedEvent());
			}
		});

	}

	protected void updateConfig() {
		ConfigPanel panel = null;
		for (Panel ePanel : Panel.values()) {
			panel = panels.get(ePanel);
			if(panel != null)
				panel.updateComponent();
		}
	}

	private void setFieldConfigTab(ConfigPanel w) {
		fieldConfigTab.clear();
		fieldConfigTab.add(w);
		this.panels.put(Panel.FIELD, w);
	}

	private void setComponentBehaviourInputTab(ConfigPanel w) {
		componentBehaviourTabInput.clear();
		componentBehaviourTabInput.add(w);
		this.panels.put(Panel.PROCESS_INPUT, w);
	}
	
	private void setComponentBehaviourOutputTab(ConfigPanel w) {
		componentBehaviourTabOutput.clear();
		componentBehaviourTabOutput.add(w);
		this.panels.put(Panel.PROCESS_OUTPUT, w);
	}

	private void setPlainJsonConfigTab(ConfigPanel w) {
		editJsonTab.clear();
		editJsonTab.add(w);
		this.panels.put(Panel.JSON, w);
	}

	public void setTabs(Component component) {
		this.component = component;
		Configuration config = component.getFieldConfiguration();
		
		setFieldConfigTab(new FieldConfigPanel(panelHash, component));
		
		if(config != null && config.get(SystemProperties.OUTPUT_NAME) != null) {
			setComponentBehaviourOutputTab(new ProcessConfigPanel(panelHash, component, false));
		} else {
			componentBehaviourTabOutput.clear();
			componentBehaviourTabOutput.setEnabled(false);
		}
		
		if(config != null && config.get(SystemProperties.INPUT_NAME) != null) {
			setComponentBehaviourInputTab(new ProcessConfigPanel(panelHash, component, true));
		} else {
			componentBehaviourTabInput.clear();
			componentBehaviourTabInput.setEnabled(false);
		}
		
		setPlainJsonConfigTab(new JsonConfigPanel(panelHash, component));
	}

}
