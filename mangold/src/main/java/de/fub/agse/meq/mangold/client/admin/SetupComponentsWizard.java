package de.fub.agse.meq.mangold.client.admin;

import java.util.List;

import com.github.gwtbootstrap.client.ui.Button;
import com.github.gwtbootstrap.client.ui.DropdownTab;
import com.github.gwtbootstrap.client.ui.NavLink;
import com.github.gwtbootstrap.client.ui.Tab;
import com.github.gwtbootstrap.client.ui.TabLink;
import com.github.gwtbootstrap.client.ui.TabPanel;
import com.github.gwtbootstrap.client.ui.constants.ButtonType;
import com.github.gwtbootstrap.client.ui.constants.IconType;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.Random;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Widget;

import de.fub.agse.meq.mangold.client.ComponentType;
import de.fub.agse.meq.mangold.client.I18N;
import de.fub.agse.meq.mangold.client.components.Component;
import de.fub.agse.meq.mangold.client.helper.JsonHelper;
import de.fub.agse.meq.mangold.client.interaction.events.AddNewComponentEvent;
import de.fub.agse.meq.mangold.client.interaction.events.ComponentsConfigurationProcessedEvent;
import de.fub.agse.meq.mangold.client.interaction.events.SaveComponentsEvent;
import de.fub.agse.meq.mangold.client.model.ClientModel;
import de.fub.agse.meq.mangold.client.serialization.JsonWrapper;
import de.fub.agse.meq.mangold.client.service.PersistenceServiceAsync;
import de.fub.agse.meq.mangold.client.ui.helper.AlertifyHelper;

public class SetupComponentsWizard extends Composite {

	private static SetupComponentsWizardUiBinder uiBinder = GWT
			.create(SetupComponentsWizardUiBinder.class);

	interface SetupComponentsWizardUiBinder extends
			UiBinder<Widget, SetupComponentsWizard> {
	}
	
	@UiField
	TabPanel tabPanel;
	@UiField
	AddComponentModal addComponentModal;
	
	private static final int UPPER_BOUND = 1231512312;
	private int numberComponentsProcessed = 0;
	
	private Button saveBtn;
	private NavLink addSourceTab;
	private NavLink addTransformationTab;
	private NavLink addSinkTab;
	private DropdownTab addComponentsDropdown;

	public SetupComponentsWizard() {
		initWidget(uiBinder.createAndBindUi(this));
		
		ComponentsConfigurationProcessedEvent.register(ClientModel.EVENT_BUS, new ComponentsConfigurationProcessedEvent.Handler() {
			public void onConfigurationProcessed(ComponentsConfigurationProcessedEvent event) {
				numberComponentsProcessed++;
				if(numberComponentsProcessed == ClientModel.getInstance().getComponents().size()) {
					List<JsonWrapper> list = JsonHelper.convertToPersistable(ClientModel.getInstance().getComponents());
					PersistenceServiceAsync.Util.getInstance().saveComponents(list, new AsyncCallback<Void>() {
						public void onSuccess(Void result) {
							AlertifyHelper.showSuccessLog(I18N.Util.get().successfullySaved());
							numberComponentsProcessed = 0;
						}
						public void onFailure(Throwable caught) {
							AlertifyHelper.showErrorLog(I18N.Util.get().errorSaving());
							numberComponentsProcessed = 0;
						}
					});
				}
			}
		});
		
		AddNewComponentEvent.register(ClientModel.EVENT_BUS, new AddNewComponentEvent.Handler() {
			public void onAddComponent(AddNewComponentEvent event) {
				if(event.getComponent() == null)
					return;
				ClientModel.getInstance().getComponents().add(event.getComponent());
				setupComponents();
			}
		});
		
		saveBtn = new Button(I18N.Util.get().save(), IconType.FILE);
		saveBtn.setType(ButtonType.PRIMARY);
		
		saveBtn.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				ClientModel.EVENT_BUS
						.fireEvent(new SaveComponentsEvent());
				//TODO
			}
		});
		
		addComponentsDropdown = new DropdownTab(I18N.Util.get().addComponent());
		
		addSourceTab = new NavLink(I18N.Util.get().addSource());
		addTransformationTab = new NavLink(I18N.Util.get().addTransformation());
		addSinkTab = new NavLink(I18N.Util.get().addSink());
		
		this.addSourceTab.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				showAddComponentPanel(ComponentType.SOURCE);
			}
		});
		this.addTransformationTab.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				showAddComponentPanel(ComponentType.TRANSFORMATION);
			}
		});
		this.addSinkTab.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				showAddComponentPanel(ComponentType.SINK);
			}
		});
		
		addComponentsDropdown.add(addSourceTab);
		addComponentsDropdown.add(addTransformationTab);
		addComponentsDropdown.add(addSinkTab);
	}

	public void setupComponents() {
		tabPanel.clear();
		tabPanel.add(addComponentsDropdown);
		
		for (Component component : ClientModel.getInstance().getComponents()) {
			Tab tab = generateComponentTab(component);
			tabPanel.add(tab);
		}
		
		if(tabPanel.getWidgetCount() > 0) {
			tabPanel.selectTab(0);
		}
		TabLink saveTab = new TabLink();
		saveTab.setCreateTabPane(false);
		
		saveTab.add(saveBtn);
		tabPanel.add(saveTab);
	}
	
	private Tab generateComponentTab(Component component) {
		Tab tab = new Tab();
		tab.setHeading(component.getName());
		
		SetupComponentsConfigurationPanel configPanel = new SetupComponentsConfigurationPanel(generatePanelHash());
		configPanel.setTabs(component);
		
		tab.add(configPanel);
		return tab;
	}
	
	private void showAddComponentPanel(ComponentType type) {
		addComponentModal.show(type);
	}
	
	private int generatePanelHash() {
		return Random.nextInt(UPPER_BOUND);
	}
}
