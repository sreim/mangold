package de.fub.agse.meq.mangold.client.admin.field;

import com.github.gwtbootstrap.client.ui.Accordion;
import com.github.gwtbootstrap.client.ui.AccordionGroup;
import com.github.gwtbootstrap.client.ui.Button;
import com.github.gwtbootstrap.client.ui.Paragraph;
import com.github.gwtbootstrap.client.ui.constants.ButtonType;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.json.client.JSONObject;
import com.google.gwt.json.client.JSONString;
import com.google.gwt.json.client.JSONValue;

import de.fub.agse.meq.mangold.client.Configuration;
import de.fub.agse.meq.mangold.client.ConfigurationNode;
import de.fub.agse.meq.mangold.client.I18N;
import de.fub.agse.meq.mangold.client.admin.ConfigPanel;
import de.fub.agse.meq.mangold.client.components.Component;
import de.fub.agse.meq.mangold.client.exception.ConfigurationCreationException;
import de.fub.agse.meq.mangold.client.interaction.events.FieldConfigurationChangedEvent;
import de.fub.agse.meq.mangold.client.model.ClientModel;
import de.fub.agse.meq.mangold.client.ui.helper.AlertifyHelper;
import de.fub.agse.meq.mangold.server.SystemProperties;

public class FieldConfigPanel extends ConfigPanel {

	private Paragraph paragraph;
	private ConfigurationNode configTree;

	public FieldConfigPanel(int panelHash, Component component) {
		super(panelHash, component);
		this.paragraph = new Paragraph();
		initWidget(paragraph);
		setupFieldConfigurationTab();
	}

	public void setupFieldConfigurationTab() {
		paragraph.clear();
		configTree = new ConfigurationNode();
		Accordion subAccordion = new Accordion();
		boolean subAccordionUsed = false;

		for (String key : component.getDefaultFieldConfiguration().keySet()) {
			if (Configuration.isEditableConfigurationKey(key) 
					&& !SystemProperties.COMPONENT_PROCESS_FIELD.equals(key)) {
				JSONValue value = component.getDefaultFieldConfiguration().get(key);
				JSONString str = null;
				JSONObject obj = null;
				if ((str = value.isString()) != null) {
					paragraph.add(setupElement(key, str.stringValue()));
				} else if ((obj = value.isObject()) != null) {
					subAccordion.add(setupElement(key, obj));
					subAccordionUsed = true;
				}
			}
		}
		if (subAccordionUsed)
			paragraph.add(subAccordion);

		Button addBtn = new Button(I18N.Util.get().addFieldConfiguration());
		addBtn.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				SetupComponentsHeadElement head = new SetupComponentsHeadElement(
						I18N.Util.get().newConfiguration());
				SetupComponentsNameElement elem = new SetupComponentsNameElement(
						"default", "");

				ConfigurationNode node = new ConfigurationNode(head);
				node.addNode(new ConfigurationNode(elem));

				configTree.addNode(node);
				ClientModel.EVENT_BUS
						.fireEvent(new FieldConfigurationChangedEvent(panelHash));
			}
		});
		paragraph.add(addBtn);
	}

	private SetupComponentsNameElement setupElement(String key, String value) {
		SetupComponentsNameElement element = new SetupComponentsNameElement(
				key, value);
		configTree.addNode(new ConfigurationNode(element));
		return element;
	}

	private AccordionGroup setupElement(String key, JSONObject value) {
		AccordionGroup subGroup = new AccordionGroup();
		subGroup.setHeading(key);

		SetupComponentsHeadElement nameElem = new SetupComponentsHeadElement(
				key);
		if (!SystemProperties.INPUT_NAME.equals(key)
				&& !SystemProperties.OUTPUT_NAME.equals(key)
				&& !SystemProperties.COMPONENT_PROCESS_FIELD.equals(key)) {
			subGroup.add(nameElem);
		}

		// dirty: adding widget without rendering it. Instead only adding it for
		// code stability
		final ConfigurationNode configNode = new ConfigurationNode(nameElem);
		configTree.addNode(configNode);

		for (String valKey : value.keySet()) {
			if (!valKey.toLowerCase().equals(
					SystemProperties.COMPONENT_TYPE_FIELD)) {
				SetupComponentsNameElement elem = new SetupComponentsNameElement(
						valKey, value.get(valKey).isString().stringValue());
				subGroup.add(elem);
				configNode.addNode(new ConfigurationNode(elem));
			}
		}
		if (!SystemProperties.INPUT_NAME.equals(key)
				&& !SystemProperties.OUTPUT_NAME.equals(key)
				&& !SystemProperties.COMPONENT_PROCESS_FIELD.equals(key)) {
			Button removeBtn = new Button();
			removeBtn.setType(ButtonType.DANGER);
			removeBtn.setText(I18N.Util.get().remove());

			removeBtn.addClickHandler(new ClickHandler() {
				public void onClick(ClickEvent event) {
					configTree.remove(configNode);
					ClientModel.EVENT_BUS
							.fireEvent(new FieldConfigurationChangedEvent(
									panelHash));
				}
			});
			subGroup.add(removeBtn);
		}
		return subGroup;
	}

	public Configuration getConfiguration() {
		try {
			return getConfigFromTree();
		} catch (ConfigurationCreationException e) {
			AlertifyHelper.showErrorLog(I18N.Util.get()
					.errorBuildingConfiguration());
			return new Configuration(component.getDefaultFieldConfiguration());
		}
	}

	private Configuration getConfigFromTree()
			throws ConfigurationCreationException {
		JSONValue val = configTree.getJson();
		if (val == null)
			throw new ConfigurationCreationException(
					"getJson returned invalid object");
		JSONObject obj = val.isObject();
		if (obj != null) {
			return new Configuration(obj);
		} else {
			throw new ConfigurationCreationException(
					"getJson returned invalid object");
		}
	}

	public void onShow() {}

	public void updateComponent() {
		super.updateComponent();
		setupFieldConfigurationTab();
	}
}
