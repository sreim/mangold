package de.fub.agse.meq.mangold.client.admin.field;

public interface SetupComponentsElement {
	
	String getLabel();
	String getValue();

}
