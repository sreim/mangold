package de.fub.agse.meq.mangold.client.admin.field;

import com.github.gwtbootstrap.client.ui.TextBox;
import com.github.gwtbootstrap.client.ui.base.InlineLabel;
import com.google.gwt.core.client.GWT;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Widget;

import de.fub.agse.meq.mangold.server.SystemProperties;

public class SetupComponentsHeadElement extends Composite implements SetupComponentsElement {

	private static SetupComponentsHeadElementUiBinder uiBinder = GWT
			.create(SetupComponentsHeadElementUiBinder.class);

	interface SetupComponentsHeadElementUiBinder extends
			UiBinder<Widget, SetupComponentsHeadElement> {
	}
	
	@UiField
	InlineLabel label;
	@UiField
	TextBox value;

	public SetupComponentsHeadElement() {
		initWidget(uiBinder.createAndBindUi(this));
	}
	
	public SetupComponentsHeadElement(String value) {
		this();
		setLabel(SystemProperties.COMPONENT_NAME_FIELD);
		setValue(value);
	}

	public String getLabel() {
		return label.getText();
	}

	public void setLabel(String label) {
		this.label.setText(label);
	}

	public String getValue() {
		return value.getText();
	}

	public void setValue(String value) {
		this.value.setText(value);
	}

}
