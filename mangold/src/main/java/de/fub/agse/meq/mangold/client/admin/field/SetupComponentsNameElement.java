package de.fub.agse.meq.mangold.client.admin.field;

import com.github.gwtbootstrap.client.ui.TextBox;
import com.github.gwtbootstrap.client.ui.base.InlineLabel;
import com.google.gwt.core.client.GWT;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Widget;

public class SetupComponentsNameElement extends Composite implements SetupComponentsElement {

	protected static SetupComponentsNameElementUiBinder uiBinder = GWT
			.create(SetupComponentsNameElementUiBinder.class);

	interface SetupComponentsNameElementUiBinder extends
			UiBinder<Widget, SetupComponentsNameElement> {
	}
	
	@UiField
	InlineLabel label;
	@UiField
	TextBox value;

	public SetupComponentsNameElement() {
		initWidget(uiBinder.createAndBindUi(this));
	}
	
	public SetupComponentsNameElement(String label, String value) {
		this();
		setLabel(label);
		setValue(value);
	}

	public String getLabel() {
		return label.getText();
	}

	public void setLabel(String label) {
		this.label.setText(label);
	}

	public String getValue() {
		return value.getText();
	}

	public void setValue(String value) {
		this.value.setText(value);
	}

}
