package de.fub.agse.meq.mangold.client.admin.field;

import com.github.gwtbootstrap.client.ui.ListBox;
import com.github.gwtbootstrap.client.ui.base.InlineLabel;
import com.google.gwt.core.client.GWT;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Widget;

import de.fub.agse.meq.mangold.client.Configurable.TypeConfiguration;

public class SetupComponentsTypeElement extends Composite implements SetupComponentsElement {

	private static SetupComponentsTypeElementUiBinder uiBinder = GWT
			.create(SetupComponentsTypeElementUiBinder.class);

	interface SetupComponentsTypeElementUiBinder extends
			UiBinder<Widget, SetupComponentsTypeElement> {
	}
	
	@UiField
	InlineLabel typeLabel;
	@UiField
	ListBox typeDd;

	public SetupComponentsTypeElement() {
		initWidget(uiBinder.createAndBindUi(this));
	}
	
	public SetupComponentsTypeElement(String typeStr) {
		this();
		for (int i = 0; i < TypeConfiguration.values().length; i++) {
			TypeConfiguration type = TypeConfiguration.values()[i];
			typeDd.addItem(type.getRepresentable(), type.toString());
			typeDd.setItemSelected(i, type.toString().equals(typeStr));
		}
	}

	public String getLabel() {
		return typeLabel.getText();
	}

	public String getValue() {
		return typeDd.getItemText(typeDd.getSelectedIndex());
	}
}
