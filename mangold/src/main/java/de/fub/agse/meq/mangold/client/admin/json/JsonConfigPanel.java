package de.fub.agse.meq.mangold.client.admin.json;

import com.github.gwtbootstrap.client.ui.TextArea;
import com.google.codemirror2_gwt.client.CodeMirrorConfig;
import com.google.codemirror2_gwt.client.CodeMirrorWrapper;
import com.google.gwt.core.client.GWT;
import com.google.gwt.json.client.JSONObject;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Widget;

import de.fub.agse.meq.mangold.client.Configuration;
import de.fub.agse.meq.mangold.client.I18N;
import de.fub.agse.meq.mangold.client.admin.ConfigPanel;
import de.fub.agse.meq.mangold.client.components.Component;
import de.fub.agse.meq.mangold.client.exception.ParseComponentException;
import de.fub.agse.meq.mangold.client.helper.JsonHelper;
import de.fub.agse.meq.mangold.client.ui.helper.AlertifyHelper;

public class JsonConfigPanel extends ConfigPanel {

	private static JsonConfigPanelUiBinder uiBinder = GWT
			.create(JsonConfigPanelUiBinder.class);

	interface JsonConfigPanelUiBinder extends UiBinder<Widget, JsonConfigPanel> {
	}
	
	@UiField
	TextArea editor;
	
	private CodeMirrorWrapper editorWrapper;

	public JsonConfigPanel(int panelHash, Component component) {
		super(panelHash, component);
		initWidget(uiBinder.createAndBindUi(this));
		
		setUpCodeEditor();
	}

	private void setUpCodeEditor() {
		CodeMirrorConfig config = CodeMirrorConfig.makeBuilder();
		config = config.setMode("text/javascript").setShowLineNumbers(true).setMatchBrackets(true);
		editorWrapper = CodeMirrorWrapper.createEditor(editor.getElement(), config);
		updateEditorContent();
	}
	
	private void updateEditorContent() {
		if(component.getDefaultFieldConfiguration() != null)
			editorWrapper.setValue(component.getDefaultFieldConfiguration().toString());
	}
	
	public void updateComponent() {
		super.updateComponent();
		updateEditorContent();
	}

	public void onShow() {
		this.editorWrapper.refresh();
	}

	@Override
	public Configuration getConfiguration() {
		try {
			String configStr = editorWrapper.getValue();
			JSONObject configObj = JsonHelper.convertToJson(configStr);
			return new Configuration(configObj);
		} catch (ParseComponentException e) {
			AlertifyHelper.showErrorLog(I18N.Util.get()
					.errorBuildingConfiguration());
			return new Configuration(component.getDefaultFieldConfiguration());
		}
	}

}
