package de.fub.agse.meq.mangold.client.admin.process;

import com.github.gwtbootstrap.client.ui.TextArea;
import com.google.codemirror2_gwt.client.CodeMirrorConfig;
import com.google.codemirror2_gwt.client.CodeMirrorWrapper;
import com.google.gwt.core.client.GWT;
import com.google.gwt.json.client.JSONObject;
import com.google.gwt.json.client.JSONString;
import com.google.gwt.json.client.JSONValue;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.Widget;

import de.fub.agse.meq.mangold.client.Configuration;
import de.fub.agse.meq.mangold.client.I18N;
import de.fub.agse.meq.mangold.client.admin.ConfigPanel;
import de.fub.agse.meq.mangold.client.components.Component;
import de.fub.agse.meq.mangold.server.SystemProperties;

public class ProcessConfigPanel extends ConfigPanel {

	private static ProcessConfigPanelUiBinder uiBinder = GWT
			.create(ProcessConfigPanelUiBinder.class);

	interface ProcessConfigPanelUiBinder extends
			UiBinder<Widget, ProcessConfigPanel> {
	}

	@UiField
	TextArea editor;
	@UiField
	Label directionLabel;

	private CodeMirrorWrapper editorWrapper;
	private boolean input;

	public ProcessConfigPanel(int panelHash, Component component) {
		super(panelHash, component);
		initWidget(uiBinder.createAndBindUi(this));
		this.input = true;

		setUpCodeEditor();
	}
	
	public ProcessConfigPanel(int panelHash, Component component, boolean input) {
		super(panelHash, component);
		initWidget(uiBinder.createAndBindUi(this));
		this.input = input;

		setUpCodeEditor();
	}

	private void setUpCodeEditor() {
		CodeMirrorConfig config = CodeMirrorConfig.makeBuilder();
		config = config.setMode("text/x-groovy").setShowLineNumbers(true)
				.setMatchBrackets(true);
		editorWrapper = CodeMirrorWrapper.createEditor(editor.getElement(),
				config);

		updateEditorContent();
	}

	private void updateEditorContent() {
		JSONObject fieldConfiguration = component.getDefaultFieldConfiguration();
		JSONValue val = null;
		JSONObject obj = null;
		JSONString process;

		if (fieldConfiguration != null) {
			if(input) {
				val = fieldConfiguration.get(SystemProperties.INPUT_NAME);
				directionLabel.setText(I18N.Util.get().labelComponentInput());
			} else {
				val = fieldConfiguration.get(SystemProperties.OUTPUT_NAME);	
				directionLabel.setText(I18N.Util.get().labelComponentOutput());
			}
			if(val != null) {
				obj = val.isObject();
			}
			editorWrapper.setValue("");
			if (obj != null && obj.get(SystemProperties.COMPONENT_PROCESS_FIELD) != null
					&& (process = obj.get(
							SystemProperties.COMPONENT_PROCESS_FIELD)
							.isString()) != null) {
				editorWrapper.setValue(process.stringValue());
			}
		}
	}

	public void updateComponent() {
		super.updateComponent();
		updateEditorContent();
	}

	public void onShow() {
		editorWrapper.refresh();
	}

	@Override
	public Configuration getConfiguration() {
		JSONObject config = component.getDefaultFieldConfiguration();
		JSONValue val = null;
		JSONObject obj = null;
		if(input) {
			val = config.get(SystemProperties.INPUT_NAME);
		} else {
			val = config.get(SystemProperties.OUTPUT_NAME);
		}
		if(val != null) {
			obj = val.isObject();
		}
		obj.put(SystemProperties.COMPONENT_PROCESS_FIELD, new JSONString(
				editorWrapper.getValue()));
		return new Configuration(config);
	}

}
