package de.fub.agse.meq.mangold.client.component;

import com.google.gwt.dom.client.Document;
import com.google.gwt.json.client.JSONObject;
import com.google.gwt.json.client.JSONString;
import com.google.gwt.json.client.JSONValue;

import de.fub.agse.meq.mangold.client.ComponentType;
import de.fub.agse.meq.mangold.client.Configuration;
import de.fub.agse.meq.mangold.client.I18N;
import de.fub.agse.meq.mangold.client.components.Component;
import de.fub.agse.meq.mangold.client.components.ConnectionPoint;
import de.fub.agse.meq.mangold.client.components.general.InputComponentTemplate;
import de.fub.agse.meq.mangold.client.components.general.InputOutputComponentTemplate;
import de.fub.agse.meq.mangold.client.components.general.OutputComponentTemplate;
import de.fub.agse.meq.mangold.client.exception.ComponentCreationException;
import de.fub.agse.meq.mangold.server.SystemProperties;

public class ComponentFactory {
	
	private static ComponentFactory instance;
	
	private ComponentFactory() {
	}
	
	private static ComponentFactory getInstance() {
		if(instance == null)
			instance = new ComponentFactory();
		return instance;
	}
	
	public static Component createComponent(JSONObject json) throws ComponentCreationException {
		return getInstance().createComponentInternal(json);
	}
	
	private Component createComponentInternal(JSONObject json) throws ComponentCreationException {
		
		Component component = null;
		
		//check which kind of component to set up
		JSONValue inputVal = json.get("input");
		JSONValue outputVal = json.get("output");
		
		if(outputVal != null && inputVal != null) {
			component = new InputOutputComponentTemplate(json);
		} else if(outputVal != null) {
			component = new OutputComponentTemplate(json);
		} else if(inputVal != null) {
			component = new InputComponentTemplate(json);
		}
		
		if(component == null) {
			throw new ComponentCreationException();
		}

		return component;
	}
	
	public static Component createNewInstanceOfComponent(Component component) throws ComponentCreationException {
		if(component instanceof InputComponentTemplate) {
			return createNewInstanceOfComponent(ComponentType.SINK);
		} else if(component instanceof InputOutputComponentTemplate) {
			return createNewInstanceOfComponent(ComponentType.TRANSFORMATION);
		} else if(component instanceof OutputComponentTemplate) {
			return createNewInstanceOfComponent(ComponentType.SOURCE);
		}
		throw new ComponentCreationException("Given Component is not valid");
	}
	
	public static Component createNewInstanceOfComponent(ComponentType type) throws ComponentCreationException {
		Component template = null;
		switch (type) {
		case SOURCE:
			template = new OutputComponentTemplate();
			break;
		case TRANSFORMATION:
			template = new InputOutputComponentTemplate();
			break;
		case SINK:
			template = new InputComponentTemplate();
			break;
		}
		if(template == null)
			throw new ComponentCreationException("Given Component is not valid");
		
		JSONObject json = template.getDefaultFieldConfiguration();
		json.put("name", new JSONString(I18N.Util.get().newComponent()));
		//TODO macht das hier Sinn??
		json.put(SystemProperties.COMPONENT_ID_FIELD, new JSONString(Document.get().createUniqueId()));
		return template;
		
	}
}
