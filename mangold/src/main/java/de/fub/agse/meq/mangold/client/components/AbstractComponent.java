package de.fub.agse.meq.mangold.client.components;

import java.util.Arrays;

import net.edzard.kinetic.Circle;
import net.edzard.kinetic.Colour;
import net.edzard.kinetic.EasingFunction;
import net.edzard.kinetic.EventType;
import net.edzard.kinetic.Group;
import net.edzard.kinetic.Image;
import net.edzard.kinetic.Kinetic;
import net.edzard.kinetic.MathTooling;
import net.edzard.kinetic.Node;
import net.edzard.kinetic.Node.EventListener;
import net.edzard.kinetic.TextPath;
import net.edzard.kinetic.Vector2d;

import com.google.gwt.json.client.JSONValue;

import de.fub.agse.meq.mangold.client.Configuration;
import de.fub.agse.meq.mangold.client.Controller;
import de.fub.agse.meq.mangold.client.EditorView;
import de.fub.agse.meq.mangold.client.interaction.events.ComponentPositionChangedEvent;
import de.fub.agse.meq.mangold.client.model.ClientModel;
import de.fub.agse.meq.mangold.server.SystemProperties;

/*
 * State:
 * (S id) 
 * V position
 * S name
 */
public abstract class AbstractComponent extends AbstractPresentable implements Component, Draggable {

	protected Group icon;
	protected Circle badge;
	protected Node symbol;
	protected TextPath banner;
	
	private static final double RADIUS_DEFAULT = 30;
	
	public AbstractComponent() {
		super();
	}
	
	protected String getComponentHash() {
		return this.getFieldConfiguration().get(SystemProperties.COMPONENT_ID_FIELD).isString().stringValue();
	}
	
	protected void updateShapeIdentification(String globalID) {
		// Update shape ID's
		icon.setID(globalID);
		badge.setID(globalID + "_" + "badge");
		symbol.setID(globalID + "_" + "symbol");
		banner.setID(globalID + "_" + "banner");
	}
	
	protected void dragMove() {
		// is empty
	};
	
	protected void dragEnd() {
		ClientModel.EVENT_BUS.fireEvent(new ComponentPositionChangedEvent(this));
	}
	
	protected Node createSymbol() {
		Group symbol = Kinetic.createGroup(Vector2d.origin, 0);
		Image img = Kinetic.createImage(new Vector2d(-32, -32), getSymbolImage());
		symbol.add(img);
		return symbol;
	}
	
	private void updateBannerText(String text) {
		banner.setText((String)text);
		double width = banner.getTextWidth();
		double circumference = Math.PI * 100;
		double offset = (width / 2) / circumference;
		double offsetRot = offset * 2 * Math.PI;
		banner.setRotation(-offsetRot);	
	}
	
	protected abstract void setupConnectionPointConfig();
	
	@Override
	public void create(Controller controller, EditorView view, Configuration configuration) {
		
		super.create(controller, view, configuration);
		this.setupConnectionPointConfig();
		
		// Create icon badge
		icon = Kinetic.createGroup(new Vector2d(configuration.get(SystemProperties.COMPONENT_POSITION_FIELD)), 0);
		badge = Kinetic.createCircle(Vector2d.origin, RADIUS_DEFAULT );	// Radius is 30
		icon.add(badge);
		
		// Create symbol
		symbol = createSymbol();
		if (symbol != null) {
			symbol.setListening(false);
			icon.add(symbol);
		}
		
		// Create name banner
		banner = Kinetic.createTextPath(Vector2d.origin, "unknown", 
				"M0-50c27.614,0,50,22.386,50,50S27.614,50,0,50S-50,27.614-50,0S-27.614-50,0-50z");		
		banner.setListening(false);
		banner.setTextStroke(Colour.darkslategray);
		banner.setFontFamily("courier new");
		banner.setFontSize(14);
		updateBannerText(configuration.get("name").isString().stringValue());
		icon.add(banner);
		
		view.getForeground().add(icon);
		
		icon.addEventListener(EventType.DRAGMOVE, new EventListener() {
			@Override
			public boolean handle() {
				dragMove();
				return true;
			}
		});
		
		icon.addEventListener(EventType.DRAGEND, new EventListener() {
			
			@Override
			public boolean handle() {
				dragEnd();
				return false;
			}
		});
		
		badge.addEventListener(EventType.MOUSEOVER, new EventListener() {
			@Override
			public boolean handle() {
				AbstractComponent.this.controller.requestFocus(AbstractComponent.this);
				return true;
			}
		});
		
		badge.addEventListener(EventType.MOUSEOUT, new EventListener() {
			@Override
			public boolean handle() {
				AbstractComponent.this.controller.requestBlur(AbstractComponent.this);
				return true;
			}
		});
		
		if (MathTooling.isPointInCircle(view.getUserPosition(), getPosition(), getRadius())) {
			controller.requestFocus(this); // Initial focus
		} else {
			controller.requestBlur(this);
		}
		update(configuration);
		setPassive();
		
		// Animate entry
		final Group animGoal = Kinetic.createGroup(icon.getPosition(), 0);
		animGoal.setScale(new Vector2d(2, 2));
		icon.transitionTo(animGoal, 0.2, EasingFunction.EASE_OUT, new Runnable() {
			@Override
			public void run() {
				animGoal.setScale(new Vector2d(1, 1));
				icon.transitionTo(animGoal, 0.4, EasingFunction.BOUNCE_EASE_OUT, new Runnable() {
					@Override
					public void run() {}
				});
			}
		});
		adjustByZoom(ClientModel.getInstance().getCurrentWorkspace().getZoom());
	}
	
	@Override
	public void remove() {
		if (icon == null) return;
		view.getForeground().remove(icon);
		icon.removeEventListener(EventType.DRAGMOVE);
		badge.removeEventListener(Arrays.asList(EventType.MOUSEOVER, EventType.MOUSEOUT));
	}
	
	@Override
	public Configuration getConfiguration() {
		configuration.put(SystemProperties.COMPONENT_POSITION_FIELD, getPosition().toJson());
		return super.getConfiguration();
	}

	@Override
	public void updateConfigurationEntry(String key, JSONValue value) {
		super.updateConfigurationEntry(key, value);
		if (key.equalsIgnoreCase(SystemProperties.COMPONENT_POSITION_FIELD)) icon.setPosition(new Vector2d(value));
		else if (key.equalsIgnoreCase("name")) updateBannerText(value.isString().stringValue());
	}
	
//	protected abstract void setSymbolActive(); 
	
	@Override
	public void setActive() {
		badge.setFill(Colour.fubGreen);
		badge.setStrokeWidth(3);
		badge.setStroke(selected?Colour.gold:Colour.black);
//		setSymbolActive();
		super.setActive();
	}
	
//	protected abstract void setSymbolPassive(); 
	
	@Override
	public void setPassive() {
		badge.setFill(Colour.silver);
		badge.setStrokeWidth(3);
		badge.setStroke(selected?Colour.lightgrey:Colour.gray);
//		setSymbolPassive();
		super.setPassive();
	}
	
	protected void setSymbolFailure() {
		// Defaults to passive symbol
		//TODO ?
//		setSymbolPassive();
	}

	@Override
	public void setFailure() {
		badge.setFill(Colour.lightpink);
		badge.setStrokeWidth(3);
		badge.setStroke(selected?Colour.pink:Colour.red);
		setSymbolFailure();
		super.setFailure();
	}

	@Override
	public void show() {
		this.icon.show();
	}

	@Override
	public void hide() {
		this.icon.hide();	
	}

	@Override
	public void select() {
		badge.setStroke(isActive?Colour.gold:hasFailure?Colour.pink:Colour.lightgrey);
		super.select();
	}
	
	@Override
	public void deselect() {
		badge.setStroke(isActive?Colour.black:hasFailure?Colour.red:Colour.gray);
		super.deselect();
	}
	
	@Override
	public void focus() {
		switch (view.getCurrentTool()) {
		case REMOVE:
			badge.setShadow(greenGlow);
			icon.setDraggable(false);
			break;
		case POINT:
			badge.setShadow(greenGlow);
			if (this instanceof Draggable) icon.setDraggable(true);
			break;
		case CONNECT:
			badge.setShadow(redGlow);
			icon.setDraggable(false);
			break;
		case INSPECT:
			badge.setShadow(this instanceof Inspectable? greenGlow : redGlow);
			icon.setDraggable(false);
			break;			
		default:
			badge.setShadow(greenGlow);
			icon.setDraggable(false);
		}	
	}
	
	@Override
	public void blur() {
		if (icon == null) return;
		badge.clearShadow();
		icon.setDraggable(false);
	}
	
	@Override
	public Vector2d getPosition() {
		return icon.getPosition();
//		return new Vector2d(getConfiguration().get(SystemProperties.COMPONENT_POSITION_FIELD));
	}
	
	@Override
	public String getName() {
		return this.getFieldConfiguration().get("name").isString().stringValue();
	}

	@Override
	public double getRadius() {
		return badge.getRadius();
	}
	
	@Override
	public void adjustByZoom(double zoomLevel) {
		this.badge.setRadius(RADIUS_DEFAULT / zoomLevel);
		this.symbol.setScale(new Vector2d(1/zoomLevel, 1/zoomLevel));
		this.icon.setScale(new Vector2d(1/zoomLevel, 1/zoomLevel));
	}

	@Override
	public String toString() {
		return "Component " + getId();
	}
}
