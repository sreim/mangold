package de.fub.agse.meq.mangold.client.components;

import java.util.Arrays;

import net.edzard.kinetic.Colour;
import net.edzard.kinetic.EventType;
import net.edzard.kinetic.Kinetic;
import net.edzard.kinetic.Line;
import net.edzard.kinetic.MathTooling;
import net.edzard.kinetic.Node.EventListener;
import net.edzard.kinetic.Vector2d;

import com.google.gwt.json.client.JSONObject;
import com.google.gwt.json.client.JSONString;
import com.google.gwt.json.client.JSONValue;

import de.fub.agse.meq.mangold.client.Configuration;
import de.fub.agse.meq.mangold.client.Controller;
import de.fub.agse.meq.mangold.client.EditorView;
import de.fub.agse.meq.mangold.client.model.ClientModel;

/*
 * State:
 * (S id)
 * S source
 * S sourceConnectionPoint
 * S target
 * S targetConnectionPoint
 */
public abstract class AbstractConnection extends AbstractPresentable implements Connection {
	
	private final OutputComponent source;
	private final OutputConnectionPoint sourceConnectionPoint;
	
	private final InputComponent target;
	private final InputConnectionPoint targetConnectionPoint;
	
	private Line line;
	private static final double DEFAULT_STROKE_WIDTH = 8;
	
	public AbstractConnection(OutputComponent source, InputComponent target) {
		super();
		this.source = source;
		this.sourceConnectionPoint = source.createOutput();
		this.sourceConnectionPoint.addConnection(this);
		this.target = target;
		this.targetConnectionPoint = target.createInput();
		this.targetConnectionPoint.addConnection(this);
	}
	
	@Override
	protected void updateShapeIdentification(String globalID) {
		// Update shape ID's
		line.setID(globalID);
	}
	
	@Override
	public OutputComponent getSource() {
		return source;
	}

	@Override
	public InputComponent getTarget() {
		return target;
	}

	@Override
	public OutputConnectionPoint getSourceConnectionPoint() {
		return sourceConnectionPoint;
	}

	@Override
	public InputConnectionPoint getTargetConnectionPoint() {
		return targetConnectionPoint;
	}

	@Override
	public void update() {
		
		// Init positions and radii
		final Vector2d srcPos = source.getPosition();
		final Vector2d tgtPos = target.getPosition();
		final double srcRadius = source.getRadius();
		final double tgtRadius = target.getRadius();
		final Vector2d srcIntersection = MathTooling.intersectLineSegmentStartingFromCircleMidPointWithCircle(srcPos, tgtPos, srcRadius);
		final Vector2d targetIntersection = MathTooling.intersectLineSegmentStartingFromCircleMidPointWithCircle(tgtPos, srcPos, tgtRadius);
		
		// Is the connection visible?
		if ((srcIntersection != null) && (targetIntersection != null) && (srcPos.distanceTo(tgtPos) > srcRadius+tgtRadius)) {
			
			line.setPoint(0, srcIntersection);
			line.setPoint(1, targetIntersection);
			line.show();
			
			Vector2d v = new Vector2d(tgtPos);
			v.sub(srcPos);
			final double rotation = v.angle(Vector2d.xUnit);
			final boolean inUpperHalf = v.y < 0;
			
			sourceConnectionPoint.update(srcIntersection, (inUpperHalf? Math.PI - rotation : Math.PI + rotation));
			sourceConnectionPoint.show();
			
			targetConnectionPoint.update(targetIntersection, (inUpperHalf? Math.PI - rotation : Math.PI + rotation));
			targetConnectionPoint.show();
			
		} else {
			
			// Nope - hide it
			hide();
			sourceConnectionPoint.hide();
			targetConnectionPoint.hide();
		}
	}

	@Override
	public void create(Controller controller, EditorView view, Configuration configuration) {
		
		super.create(controller, view, configuration);
		
		// Create line
		line = Kinetic.createLine(source.getPosition(), target.getPosition());
		line.setStrokeWidth(2);
		line.setDraggable(false);
		view.getForeground().add(line);
		line.moveToBottom();
		
		line.addEventListener(EventType.MOUSEOVER, new EventListener() {
			@Override
			public boolean handle() {
				AbstractConnection.this.controller.requestFocus(AbstractConnection.this);
				return true;
			}
		});
		
		line.addEventListener(EventType.MOUSEOUT, new EventListener() {
			@Override
			public boolean handle() {
				AbstractConnection.this.controller.requestBlur(AbstractConnection.this);
				return true;
			}
		});
		
		JSONValue inputVal = configuration.get("input");
		JSONValue outputVal = configuration.get("output");
		JSONObject inputObj = null;
		JSONObject outputObj = null;
		
		if(inputVal != null) {
			inputObj = inputVal.isObject();
		}
		if(outputVal != null) {
			outputObj = outputVal.isObject();
		}
		
		// Create source connection point
		final Configuration scpConfiguration = new Configuration();
		scpConfiguration.mergeOverwriteWith(new Configuration(outputObj));
//		scpConfiguration.put("position", source.getPosition());
//		scpConfiguration.put("rotation", 0.0);
		sourceConnectionPoint.create(controller, view, scpConfiguration);
		
		// Create target connection point
		final Configuration tgtConfiguration = new Configuration();
		tgtConfiguration.mergeOverwriteWith(new Configuration(inputObj));
//		tgtConfiguration.put("position", target.getPosition());
//		tgtConfiguration.put("rotation", 0.0);
		targetConnectionPoint.create(controller, view, tgtConfiguration);
		
		update();
		setPassive();
		
		// Animate entry
		line.setOpacity(0.0);
		final Line animGoal = Kinetic.createLine(source.getPosition(), target.getPosition());
		animGoal.setStrokeWidth(DEFAULT_STROKE_WIDTH );
		animGoal.setOpacity(1);
		line.transitionTo(animGoal, 1);
		
		adjustByZoom(ClientModel.getInstance().getCurrentWorkspace().getZoom());
	}
	
	@Override
	public void remove() {
		if (line == null) return;
		view.getForeground().remove(line);
		line.removeEventListener(Arrays.asList(EventType.MOUSEOVER, EventType.MOUSEOUT));
		line = null;
		controller.requestRemoval(sourceConnectionPoint);
		controller.requestRemoval(targetConnectionPoint);
	}
	
	@Override
	public Configuration getConfiguration() {
		configuration.put("source", new JSONString(source.getId()));
		configuration.put("sourceConnectionPoint", new JSONString(sourceConnectionPoint.getId()));
		configuration.put("target", new JSONString(target.getId()));
		configuration.put("targetConnectionPoint", new JSONString(targetConnectionPoint.getId()));
		return super.getConfiguration();
	}
	
	
	@Override
	public void updateConfigurationEntry(String key, JSONValue value) {
		super.updateConfigurationEntry(key, value);
		// TODO: change values from configuration here
	}
	
	@Override
	public void setActive() {
		line.setStroke(selected?Colour.gold:Colour.black);
		super.setActive();
	}
	
	@Override
	public void setPassive() {
		line.setStroke(selected?Colour.lightgrey:Colour.gray);
		super.setPassive();
	}
	
	@Override
	public void setFailure() {
		line.setStroke(selected?Colour.pink:Colour.red);
		super.setFailure();
	}	
	
	@Override
	public void show() {
		this.line.show();
	}

	@Override
	public void hide() {
		this.line.hide();	
	}
	
	@Override
	public void select() {
		line.setStroke(isActive?Colour.gold:hasFailure?Colour.pink:Colour.lightgrey);
		super.select();
	}
	
	@Override
	public void deselect() {
		line.setStroke(isActive?Colour.black:hasFailure?Colour.red:Colour.gray);
		super.deselect();
	}

	@Override
	public void focus() {
		switch (view.getCurrentTool()) {
		case REMOVE:
			line.setShadow(greenGlow);
			break;
		case POINT:
			line.setShadow(greenGlow);
			break;
		default:
			line.setShadow(greenGlow);
		}	
	}
	
	@Override
	public void blur() {
		if (line == null) return;
		line.clearShadow();
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Connection " + getId();
	}
	
	@Override
	public void adjustByZoom(double zoomLevel) {
		line.setStrokeWidth(DEFAULT_STROKE_WIDTH / zoomLevel);
	}
}
