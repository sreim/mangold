package de.fub.agse.meq.mangold.client.components;

import java.util.Arrays;

import net.edzard.kinetic.Colour;
import net.edzard.kinetic.EventType;
import net.edzard.kinetic.Kinetic;
import net.edzard.kinetic.Node.EventListener;
import net.edzard.kinetic.PathSVG;
import net.edzard.kinetic.Vector2d;

import com.google.gwt.json.client.JSONString;
import com.google.gwt.json.client.JSONValue;

import de.fub.agse.meq.mangold.client.Configurable;
import de.fub.agse.meq.mangold.client.Configuration;
import de.fub.agse.meq.mangold.client.Controller;
import de.fub.agse.meq.mangold.client.EditorView;
import de.fub.agse.meq.mangold.client.model.ClientModel;

/*
 * State:
 * (s id)
 * S component
 * S connection
 */
public abstract class AbstractConnectionPoint<C extends Component> extends AbstractPresentable implements ConnectionPoint, Configurable {
	
	private static final double DEFAULT_SYMBOL_SCALE = 1;
	
	final protected C component;
	protected Connection connection;
	
	private PathSVG symbol;
	
	public AbstractConnectionPoint(C component) {
		super();
		this.component = component;
	}

	@Override
	public void create(Controller controller, EditorView view, Configuration configuration) {
		
		super.create(controller, view, configuration);
		final String arrow = "M-15,0 L0,-10 L0,-5 L10,-5 L10,5 L0,5 L0,10 Z";
		symbol = Kinetic.createPathSVG(component.getPosition(), arrow);
		//symbol = Kinetic.createRegularPolygon(node.getPosition(), 10, 3);
		symbol.setFill(Colour.white);
		symbol.setStrokeWidth(2);
		symbol.setDraggable(false);
		view.getForeground().add(symbol);

		symbol.addEventListener(EventType.MOUSEOVER, new EventListener() {
			@Override
			public boolean handle() {
				AbstractConnectionPoint.this.controller.requestFocus(AbstractConnectionPoint.this);
				return true;
			}
		});
		
		symbol.addEventListener(EventType.MOUSEOUT, new EventListener() {
			@Override
			public boolean handle() {
				AbstractConnectionPoint.this.controller.requestBlur(AbstractConnectionPoint.this);
				return true;
			}
		});
		
		update(configuration);
		setPassive();
		
		adjustByZoom(ClientModel.getInstance().getCurrentWorkspace().getZoom());
	}
	
	/* (non-Javadoc)
	 * @see de.fub.agse.meq.mangold.client.components.ConnectionPoint#addConnection(de.fub.agse.meq.mangold.client.components.Edge)
	 */
	@Override
	public void addConnection(Connection connection) {
		this.connection = connection;
	}

	/* (non-Javadoc)
	 * @see de.fub.agse.meq.mangold.client.components.ConnectionPoint#update(net.edzard.kinetic.Vector2d, double)
	 */
	@Override
	public void update(Vector2d position, double rotation) {
		symbol.setPosition(position);
		//configuration.put("position", position);
		symbol.setRotation(rotation);
		//configuration.put("rotation", rotation);
	}
	
	@Override
	public void setActive() {
		symbol.setStroke(selected?Colour.gold:Colour.black);
		super.setActive();
	}
	
	@Override
	public void setPassive() {
		symbol.setStroke(selected?Colour.lightgrey:Colour.gray);
		super.setPassive();
	}
	
	@Override
	public void setFailure() {
		symbol.setStroke(selected?Colour.pink:Colour.red);
		super.setFailure();
	}	

	@Override
	public void remove() {
		if (symbol == null) return;
		view.getForeground().remove(symbol);
		symbol.removeEventListener(Arrays.asList(EventType.MOUSEOVER, EventType.MOUSEOUT));
		symbol = null;
		controller.requestRemoval(connection);
	}

	@Override
	public void show() {
		this.symbol.show();
	}

	@Override
	public void hide() {
		this.symbol.hide();	
	}
	
	@Override
	public void focus() {
		switch (view.getCurrentTool()) {
		case POINT:
		case INSPECT:
		case REMOVE:
			symbol.setShadow(greenGlow);
			break;
		default:
			symbol.setShadow(redGlow);
		}	
	}
	
	@Override
	public void blur() {
		if (symbol == null) return;
		symbol.clearShadow();
	}

	@Override
	public void select() {
		symbol.setStroke(isActive?Colour.gold:hasFailure?Colour.pink:Colour.lightgrey);
		super.select();
	}
	
	@Override
	public void deselect() {
		symbol.setStroke(isActive?Colour.black:hasFailure?Colour.red:Colour.gray);
		super.deselect();
	}
	
	@Override
	protected void updateShapeIdentification(String globalID) {
		// TODO Auto-generated method stub
		
	}
	
	@Override
	public Configuration getConfiguration() {
		configuration.put("component", new JSONString(component.getId()));
		configuration.put("connection", new JSONString(connection.getId()));
		return super.getConfiguration();
	}
	
	@Override
	public void updateConfigurationEntry(String key, JSONValue value) {
		super.updateConfigurationEntry(key, value);
	}
	
	@Override
	public C getComponent() {
		return component;
	}

	@Override
	public Connection getConnection() {
		return connection;
	}
	
	@Override
	public void adjustByZoom(double zoomLevel) {
		symbol.setScale(new Vector2d(DEFAULT_SYMBOL_SCALE / zoomLevel, DEFAULT_SYMBOL_SCALE / zoomLevel));
	}
}
