package de.fub.agse.meq.mangold.client.components;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import com.google.gwt.json.client.JSONArray;
import com.google.gwt.json.client.JSONObject;
import com.google.gwt.json.client.JSONString;

import de.fub.agse.meq.mangold.client.Configuration;

/*
 * State:
 * (S id) 
 * (V position)
 * (S name)
 * A inputs
 */
public abstract class AbstractInputComponent extends AbstractComponent
		implements InputComponent {

	private final Set<InputConnectionPoint> inputs;

	public AbstractInputComponent() {
		super();
		this.inputs = new HashSet<InputConnectionPoint>();
	}

	protected abstract JSONObject getInputConnectionPointConfig();

	@Override
	public InputConnectionPoint createInput() {
		final InputConnectionPoint cp = new InputConnectionPointImpl(this,
				getInputConnectionPointConfig());
		inputs.add(cp);
		return cp;
	}

	@Override
	public void removeInput(InputConnectionPoint connectionPoint) {
		inputs.remove(connectionPoint);
	}

	@Override
	public Collection<InputConnectionPoint> getInputConnectionPoints() {
		return inputs;
	}

	@Override
	public void remove() {
		for (ConnectionPoint connection : inputs) {
			controller.requestRemoval(connection);
		}
		super.remove();
	}

	@Override
	protected void dragMove() {
		// Update inputs
		for (ConnectionPoint input : inputs) {
			input.getConnection().update();
		}
		if (!inputs.isEmpty())
			view.getForeground().draw();
	}

	@Override
	public void focus() {
		super.focus();
		final Component connectionSource = view.getConnectionSource();
		if (connectionSource != null) {
			// Is connecting
			if (connectionSource == this) {
				badge.setShadow(redGlow); // No connection with myself
			} else {
				badge.setShadow(greenGlow);
			}
			icon.setDraggable(false);
		}
	}

	@Override
	public Configuration getConfiguration() {
		JSONArray inputArray = new JSONArray();
		int i = 0;
		for (InputConnectionPoint icp : inputs) {
			inputArray.set(i++, new JSONString(icp.getId()));
		}
		configuration.put("inputs", inputArray);
		return super.getConfiguration();
	}
}
