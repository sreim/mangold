package de.fub.agse.meq.mangold.client.components;

/*
 * State:
 * (s id)
 * (S component)
 * (S connection)
 */
public abstract class AbstractInputConnectionPoint extends AbstractConnectionPoint<InputComponent> implements InputConnectionPoint {

	public AbstractInputConnectionPoint(InputComponent node) {
		super(node);
	}

	@Override
	public void remove() {
		super.remove();
		component.removeInput(this);
	}
}
