package de.fub.agse.meq.mangold.client.components;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import com.google.gwt.json.client.JSONArray;
import com.google.gwt.json.client.JSONObject;
import com.google.gwt.json.client.JSONString;

import de.fub.agse.meq.mangold.client.Configuration;
import de.fub.agse.meq.mangold.client.ToolTypes;

/*
 * State:
 * (S id) 
 * (V position)
 * (S name)
 * A inputs
 */
public abstract class AbstractInputOutputComponent extends AbstractComponent implements InputOutputComponent {
	
	private final Set<InputConnectionPoint> inputs;
	private final Set<OutputConnectionPoint> outputs;
	
	public AbstractInputOutputComponent() {
		super();
		this.inputs = new HashSet<InputConnectionPoint>();
		this.outputs = new HashSet<OutputConnectionPoint>();
	}

//	protected abstract InputConnectionPoint createInputConnectionPoint();
	protected abstract JSONObject getInputConnectionPointConfig();
	
	@Override
	public InputConnectionPoint createInput() {
		final InputConnectionPoint cp = new InputConnectionPointImpl(this, getInputConnectionPointConfig());
		inputs.add(cp);
		return cp;
	}
	
	@Override
	public void removeInput(InputConnectionPoint connectionPoint) {
		inputs.remove(connectionPoint);
	}
	
	@Override
	public Collection<InputConnectionPoint> getInputConnectionPoints() {
		return inputs;
	}

	@Override
	public void remove() {
		for (ConnectionPoint connection: inputs) {
			controller.requestRemoval(connection);
		}
		for (ConnectionPoint connection: outputs) {
			controller.requestRemoval(connection);
		}
		super.remove();
	}
	
//	protected abstract OutputConnectionPoint createOutputConnectionPoint();
	protected abstract JSONObject getOutputConnectionPointConfig();
	
	@Override
	public OutputConnectionPoint createOutput() {
		final OutputConnectionPoint ocp = new OutputConnectionPointImpl(this, getOutputConnectionPointConfig());
//		final OutputConnectionPoint ocp = createOutputConnectionPoint();
		outputs.add(ocp);
		return ocp;
	}

	@Override
	public void removeOutput(OutputConnectionPoint connectionPoint) {;
		outputs.remove(connectionPoint);
	}	
	
	@Override
	public Collection<OutputConnectionPoint> getOutputConnectionPoints() {
		return outputs;
	}
	
	@Override
	protected void dragMove() {
		// Update inputs
		for (ConnectionPoint input: inputs) {
			input.getConnection().update();
		}
		if (!inputs.isEmpty() && !outputs.isEmpty()) view.getForeground().draw();
		for (ConnectionPoint output: outputs) {
			output.getConnection().update();
		}
		if (!outputs.isEmpty()) view.getForeground().draw();
	}

	@Override
	public void focus() {
		super.focus();
		final Component connectionSource = view.getConnectionSource();
		if (connectionSource != null) {
			// Is connecting
			if (connectionSource == this) {
				badge.setShadow(redGlow); // No connection with myself
			} else {
				badge.setShadow(greenGlow);
			}
			icon.setDraggable(false);
		}
		if ((view.getCurrentTool() == ToolTypes.CONNECT) && (connectionSource == null)) {
			// Can start connecting
			badge.setShadow(greenGlow);
			icon.setDraggable(false);
		} 
	}

	@Override
	public Configuration getConfiguration() {
		JSONArray inputArray = new JSONArray();
		int i=0;
		for (InputConnectionPoint icp: inputs) {
			inputArray.set(i++, new JSONString(icp.getId()));
		}
		configuration.put("inputs", inputArray);
		JSONArray outputArray = new JSONArray();
		i=0;
		for (OutputConnectionPoint ocp: outputs) {
			outputArray.set(i++, new JSONString(ocp.getId()));
		}
		configuration.put("outputs", outputArray);
		return super.getConfiguration();
	}
}
