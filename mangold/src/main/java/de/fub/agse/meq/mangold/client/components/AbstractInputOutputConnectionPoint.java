package de.fub.agse.meq.mangold.client.components;

/*
 * State:
 * (s id)
 * (S component)
 * (S connection)
 */
public abstract class AbstractInputOutputConnectionPoint extends AbstractConnectionPoint<InputOutputComponent> implements InputConnectionPoint, OutputConnectionPoint {

	public AbstractInputOutputConnectionPoint(InputOutputComponent node) {
		super(node);
	}

	@Override
	public void remove() {
		super.remove();
		component.removeInput(this);
		component.removeOutput(this);
	}
}
