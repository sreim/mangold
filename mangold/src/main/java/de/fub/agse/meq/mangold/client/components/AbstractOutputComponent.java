package de.fub.agse.meq.mangold.client.components;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import com.google.gwt.json.client.JSONArray;
import com.google.gwt.json.client.JSONObject;
import com.google.gwt.json.client.JSONString;

import de.fub.agse.meq.mangold.client.Configuration;
import de.fub.agse.meq.mangold.client.ToolTypes;

/*
 * State:
 * (S id) 
 * (V position)
 * (S name)
 * A outputs
 */
public abstract class AbstractOutputComponent extends AbstractComponent implements OutputComponent {
		
	private final Set<OutputConnectionPoint> outputs;
	
	public AbstractOutputComponent() {
		super();
		outputs = new HashSet<OutputConnectionPoint>();
	}

	@Override
	public void remove() {
		for (ConnectionPoint connection: outputs) {
			controller.requestRemoval(connection);
		}
		super.remove();
	}
	
//	protected abstract OutputConnectionPoint createOutputConnectionPoint();
	protected abstract JSONObject getOutputConnectionPointConfig();
	
	@Override
	public OutputConnectionPoint createOutput() {
		final OutputConnectionPoint ocp = new OutputConnectionPointImpl(this, getOutputConnectionPointConfig());
//		final OutputConnectionPoint ocp = createOutputConnectionPoint();
		outputs.add(ocp);
		return ocp;
	}

	@Override
	public void removeOutput(OutputConnectionPoint connectionPoint) {
		outputs.remove(connectionPoint);
	}	
	
	@Override
	public Collection<OutputConnectionPoint> getOutputConnectionPoints() {
		return outputs;
	}

	@Override
	protected void dragMove() {
		// Update inputs
		for (ConnectionPoint output: outputs) {
			output.getConnection().update();
		}
		if (!outputs.isEmpty()) view.getForeground().draw();
	}
	
	@Override
	public void focus() {
		super.focus();
		final Component connectionSource = view.getConnectionSource();
		if ((view.getCurrentTool() == ToolTypes.CONNECT) && (connectionSource == null)) {
			// Can start connecting
			badge.setShadow(greenGlow);
			icon.setDraggable(false);
		} 
	}

	@Override
	public Configuration getConfiguration() {
		JSONArray outputArray = new JSONArray();
		int i=0;
		for (OutputConnectionPoint ocp: outputs) {
			outputArray.set(i++, new JSONString(ocp.getId()));
		}
		configuration.put("outputs", outputArray);
		return super.getConfiguration();
	}
}

