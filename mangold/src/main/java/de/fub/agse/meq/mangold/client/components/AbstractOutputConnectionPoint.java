package de.fub.agse.meq.mangold.client.components;

/*
 * State:
 * (s id)
 * (S component)
 * (S connection)
 */
public abstract class AbstractOutputConnectionPoint extends AbstractConnectionPoint<OutputComponent> implements OutputConnectionPoint {

	public AbstractOutputConnectionPoint(OutputComponent node) {
		super(node);
	}
 
	@Override
	public void remove() {
		super.remove();
		component.removeOutput(this);
	}
}
