package de.fub.agse.meq.mangold.client.components;

import net.edzard.kinetic.Colour;
import net.edzard.kinetic.Shadow;
import net.edzard.kinetic.Vector2d;

import com.google.gwt.json.client.JSONObject;
import com.google.gwt.json.client.JSONString;
import com.google.gwt.json.client.JSONValue;

import de.fub.agse.meq.mangold.client.Configuration;
import de.fub.agse.meq.mangold.client.Controller;
import de.fub.agse.meq.mangold.client.EditorView;
import de.fub.agse.meq.mangold.client.I18N;
import de.fub.agse.meq.mangold.client.exception.ParseComponentException;
import de.fub.agse.meq.mangold.client.helper.JsonHelper;
import de.fub.agse.meq.mangold.client.ui.helper.AlertifyHelper;
import de.fub.agse.meq.mangold.server.SystemProperties;

/*
 * State:
 * S id
 */
public abstract class AbstractPresentable implements Presentable {

	final static protected Shadow greenGlow, redGlow;

	static {
		greenGlow = new Shadow(Colour.green, 16, new Vector2d(2, 1), 8);
		redGlow = new Shadow(Colour.red, 16, new Vector2d(2, 1), 8);
	}

	protected Configuration configuration;

	protected boolean selected, isActive, hasFailure;

	protected Controller controller;
	protected EditorView view;

	public AbstractPresentable() {
	}

	@Override
	public Configuration getConfiguration() {
		return configuration;
	}

	@Override
	public void updateConfigurationEntry(String key, JSONValue value) {
		configuration.put(key, value);
	}

	@Override
	public void update(Configuration configuration) {
		for (String key : configuration.keySet()) {
			updateConfigurationEntry(key, configuration.get(key));
		}
	}

	Configuration fieldConfig = null;
	
	public abstract JSONObject getDefaultFieldConfiguration();

	public Configuration getFieldConfiguration() {
		if(fieldConfig == null)
			setupFieldConfiguration(); 
		return fieldConfig;
	}
	
	private void setupFieldConfiguration() {
		JSONObject defaultConfig = getDefaultFieldConfiguration();
		String configString = defaultConfig.toString();
		configString = configString.replaceAll(SystemProperties.COMPONENT_DEFAULT_FIELD, SystemProperties.COMPONENT_VALUE_FIELD);
		try {
			JSONObject config = JsonHelper.convertToJson(configString);
			this.fieldConfig = new Configuration(config);
		} catch (ParseComponentException e) {
			AlertifyHelper.showErrorLog(I18N.Util.get().errorBuildingConfiguration());
			this.fieldConfig = new Configuration(defaultConfig);
		}
	}
	
	public void setFieldConfiguration(Configuration newDefaultConfiguration) {
		this.fieldConfig = newDefaultConfiguration;
	}

	protected abstract void updateShapeIdentification(final String globalID);

	@Override
	public void create(Controller controller, EditorView view,
			Configuration configuration) {
		this.controller = controller;
		this.view = view;
		this.configuration = configuration;
	}

	@Override
	public String getId() {
		return configuration.get("id").isString().stringValue();
	}

	@Override
	public void setId(String uuid) {
		configuration.put("id", new JSONString(uuid));
		updateShapeIdentification(uuid);
	}

	@Override
	public void setActive() {
		isActive = true;
		hasFailure = false;
		view.getForeground().draw();
	}

	@Override
	public void setPassive() {
		isActive = false;
		hasFailure = false;
		view.getForeground().draw();
	}

	@Override
	public void setFailure() {
		isActive = false;
		hasFailure = true;
		view.getForeground().draw();
	}

	@Override
	public void select() {
		selected = true;
		view.getForeground().draw();
	}

	@Override
	public void deselect() {
		selected = false;
		view.getForeground().draw();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return getId();
	}
}
