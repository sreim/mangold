package de.fub.agse.meq.mangold.client.components;

import net.edzard.kinetic.Vector2d;

import com.google.gwt.user.client.ui.Image;

import de.fub.agse.meq.mangold.client.Configurable;

public interface Component extends Presentable, Configurable {

	Vector2d getPosition();
	
	String getName();
	
	double getRadius();
	
	Image getSymbolImage();	
}
