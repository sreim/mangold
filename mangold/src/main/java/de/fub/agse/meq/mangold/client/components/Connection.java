package de.fub.agse.meq.mangold.client.components;

public interface Connection extends Presentable {

	OutputComponent getSource();
	InputComponent getTarget();
	
	OutputConnectionPoint getSourceConnectionPoint();
	InputConnectionPoint getTargetConnectionPoint();
	
	void update();
}
