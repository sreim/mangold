package de.fub.agse.meq.mangold.client.components;

import de.fub.agse.meq.mangold.client.Configurable;
import net.edzard.kinetic.Vector2d;

public interface ConnectionPoint extends Configurable, Presentable {

	void addConnection(Connection connection);
	
	Component getComponent();
	Connection getConnection();
	
	void update(Vector2d position, double rotation);
	
}
