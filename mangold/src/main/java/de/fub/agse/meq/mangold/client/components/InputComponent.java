package de.fub.agse.meq.mangold.client.components;

import java.util.Collection;

public interface InputComponent extends Component {

	InputConnectionPoint createInput();
	
	void removeInput(InputConnectionPoint connectionPoint);
	
	Collection<InputConnectionPoint> getInputConnectionPoints();

}
