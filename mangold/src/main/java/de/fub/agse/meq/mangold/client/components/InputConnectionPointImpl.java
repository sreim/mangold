package de.fub.agse.meq.mangold.client.components;

import com.google.gwt.json.client.JSONObject;

public class InputConnectionPointImpl extends AbstractInputConnectionPoint {

	public InputConnectionPointImpl(InputComponent node, JSONObject configuration) {
		super(node);
		this.configuration = configuration;
	}
	
	private final JSONObject configuration;

	@Override
	public JSONObject getDefaultFieldConfiguration() {
		return this.configuration;
	}
		
}