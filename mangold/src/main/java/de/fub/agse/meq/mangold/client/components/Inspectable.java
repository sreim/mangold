package de.fub.agse.meq.mangold.client.components;

public interface Inspectable {

	String getInspectionURL();
	
}
