package de.fub.agse.meq.mangold.client.components;

import java.util.Collection;

public interface OutputComponent extends Component {

	OutputConnectionPoint createOutput(); 
	
	void removeOutput(OutputConnectionPoint connectionPoint);

	Collection<OutputConnectionPoint> getOutputConnectionPoints();
}
