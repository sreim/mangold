package de.fub.agse.meq.mangold.client.components;

import com.google.gwt.json.client.JSONObject;

import de.fub.agse.meq.mangold.client.Configurable;
import de.fub.agse.meq.mangold.client.Configuration;
import de.fub.agse.meq.mangold.client.Controller;
import de.fub.agse.meq.mangold.client.EditorView;

public interface Presentable extends Configurable {

	void create(Controller controller, EditorView view, Configuration configuration);
	void remove();
	
	void show();
	void hide();
	
	void focus();
	void blur();
	
	void select();
	void deselect();
	
	void setActive();
	void setPassive();
	void setFailure();
	
	JSONObject getDefaultFieldConfiguration();
	
	void adjustByZoom(double zoomLevel);

}