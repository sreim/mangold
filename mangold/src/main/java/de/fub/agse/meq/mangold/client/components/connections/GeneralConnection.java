package de.fub.agse.meq.mangold.client.components.connections;

import com.google.gwt.json.client.JSONObject;
import com.google.gwt.json.client.JSONString;

import de.fub.agse.meq.mangold.client.components.AbstractConnection;
import de.fub.agse.meq.mangold.client.components.InputComponent;
import de.fub.agse.meq.mangold.client.components.OutputComponent;

/*
 * State:
 * (S id)
 * (S source)
 * (S target)
 * S type
 */
public class GeneralConnection extends AbstractConnection {
	
	public GeneralConnection(OutputComponent source, InputComponent target) {
		super(source, target);
	}

	@Override
	public JSONObject getDefaultFieldConfiguration() {
		JSONObject obj = new JSONObject();
		obj.put("name", new JSONString("General Connection"));
		return obj;
	}

//	@Override
//	protected ThingType getType() {
//		return ThingType.GENERAL_CONNECTION;
//	}
}
