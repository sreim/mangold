package de.fub.agse.meq.mangold.client.components.general;

import com.github.gwtbootstrap.client.ui.Image;
import com.google.gwt.json.client.JSONObject;
import com.google.gwt.json.client.JSONString;
import com.google.gwt.json.client.JSONValue;

import de.fub.agse.meq.mangold.client.components.AbstractInputComponent;
import de.fub.agse.meq.mangold.client.resources.TheResources;

public class InputComponentTemplate extends AbstractInputComponent {
	private JSONObject json;
	private JSONObject inputJson;

	public InputComponentTemplate() {
		super();
		this.json = new JSONObject();
		this.json.put("input", new JSONObject());
	}

	public InputComponentTemplate(JSONObject json) {
		this();
		this.json = json;
	}

	@Override
	public Image getSymbolImage() {
		// try to fetch image from json
		JSONString strImage = null;
		Image img = null;

		if (json.get("symbolImage") != null
				&& (strImage = json.get("symbolImage").isString()) != null) {
			img = new Image(strImage.stringValue());
		}

		if (img == null)
			img = new Image(TheResources.INSTANCE.iconInfoSink().getSafeUri());

		return img;
	}

	@Override
	public JSONObject getDefaultFieldConfiguration() {
		return json;
	}

	@Override
	protected JSONObject getInputConnectionPointConfig() {
		return inputJson;
	}

	@Override
	protected void setupConnectionPointConfig() {
		JSONValue inputVal = this.configuration.get("input");
		if(inputVal != null) {
			this.inputJson = inputVal.isObject();
		}
	}
}
