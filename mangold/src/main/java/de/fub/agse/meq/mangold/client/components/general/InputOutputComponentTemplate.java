package de.fub.agse.meq.mangold.client.components.general;

import com.github.gwtbootstrap.client.ui.Image;
import com.google.gwt.json.client.JSONObject;
import com.google.gwt.json.client.JSONString;
import com.google.gwt.json.client.JSONValue;

import de.fub.agse.meq.mangold.client.components.AbstractInputOutputComponent;
import de.fub.agse.meq.mangold.client.resources.TheResources;

public class InputOutputComponentTemplate extends AbstractInputOutputComponent {
	private JSONObject json;
	private JSONObject outputJson;
	private JSONObject inputJson;
	
	public InputOutputComponentTemplate() {
		super();
		this.json = new JSONObject();
		this.json.put("input", new JSONObject());
		this.json.put("output", new JSONObject());
	}
	
	public InputOutputComponentTemplate(JSONObject json) {
		this();
		this.json = json;
	}
	
	@Override
	public Image getSymbolImage() {
		//try to fetch image from json
		JSONString strImage = null;
		Image img = null;
		
		if(json.get("symbolImage") != null && (strImage = json.get("symbolImage").isString()) != null)
			img = new Image(strImage.stringValue());
		
		if(img == null)
			img = new Image(TheResources.INSTANCE.iconDataSource().getSafeUri());
	
		return img;
	}

	@Override
	public JSONObject getDefaultFieldConfiguration() {
		return json;
	}
	
	
	@Override
	protected JSONObject getOutputConnectionPointConfig() {
		return outputJson;
	}
	
	@Override
	protected JSONObject getInputConnectionPointConfig() {
		return inputJson;
	}

	@Override
	protected void setupConnectionPointConfig() {
		JSONValue inputVal = this.configuration.get("input");
		JSONValue outputVal = this.configuration.get("output");
		if(inputVal != null) {
			inputJson = inputVal.isObject();
		}
		if(outputVal != null) {
			outputJson = outputVal.isObject();
		}
	}
}
