package de.fub.agse.meq.mangold.client.components.general;

import com.github.gwtbootstrap.client.ui.Image;
import com.google.gwt.json.client.JSONObject;
import com.google.gwt.json.client.JSONString;
import com.google.gwt.json.client.JSONValue;

import de.fub.agse.meq.mangold.client.components.AbstractOutputComponent;
import de.fub.agse.meq.mangold.client.resources.TheResources;

public class OutputComponentTemplate extends AbstractOutputComponent {
	private JSONObject json;
	private JSONObject outputJson;

	public OutputComponentTemplate() {
		super();
		this.json = new JSONObject();
		this.outputJson = new JSONObject();
		this.json.put("output", new JSONObject());
	}

	public OutputComponentTemplate(JSONObject json) {
		this();
		this.json = json;
	}

	@Override
	public Image getSymbolImage() {
		// try to fetch image from json
		JSONString strImage = null;
		Image img = null;

		if (json.get("symbolImage") != null
				&& (strImage = json.get("symbolImage").isString()) != null) {
			img = new Image(strImage.stringValue());
		}

		if (img == null)
			img = new Image(TheResources.INSTANCE.iconDataSource().getSafeUri());

		return img;
	}

	@Override
	protected void setupConnectionPointConfig() {
		JSONValue outputVal = this.configuration.get("output");
		if(outputVal != null) {
			outputJson = outputVal.isObject();
		}
	}

	@Override
	public JSONObject getDefaultFieldConfiguration() {
		return json;
	}

	@Override
	protected JSONObject getOutputConnectionPointConfig() {
		return outputJson;
	}

}
