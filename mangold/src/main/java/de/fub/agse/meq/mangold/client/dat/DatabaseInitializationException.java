package de.fub.agse.meq.mangold.client.dat;

public class DatabaseInitializationException extends Exception {

	private static final long serialVersionUID = 2075601417546084712L;
	
	public DatabaseInitializationException() {
		super();
	}
	
	public DatabaseInitializationException(String msg) {
		super(msg);
	}

}
