package de.fub.agse.meq.mangold.client.dat;

public class DatabaseNotInitializedException extends Exception {

	private static final long serialVersionUID = 2075601417546084712L;
	
	public DatabaseNotInitializedException() {
		super();
	}
	
	public DatabaseNotInitializedException(String msg) {
		super(msg);
	}

}
