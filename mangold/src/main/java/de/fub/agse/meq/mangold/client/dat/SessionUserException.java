package de.fub.agse.meq.mangold.client.dat;

public class SessionUserException extends Exception {

	private static final long serialVersionUID = 2075601417546084712L;
	
	public SessionUserException() {
		super();
	}
	
	public SessionUserException(String msg) {
		super(msg);
	}

}
