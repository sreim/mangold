package de.fub.agse.meq.mangold.client.exception;

public class ComponentCreationException extends Exception {
	
	private static final long serialVersionUID = -1061416309463455788L;

	public ComponentCreationException() {
		super("Component could not be properly created!");
	}
	
	public ComponentCreationException(String message) {
		super(message);
	}
}
