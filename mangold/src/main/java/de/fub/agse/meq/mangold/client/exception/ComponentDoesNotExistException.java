package de.fub.agse.meq.mangold.client.exception;

public class ComponentDoesNotExistException extends Exception {

	private static final long serialVersionUID = -4378362330462759064L;
	
	
	public ComponentDoesNotExistException() {
		super("Component does not exist in storage");
	}
	
	public ComponentDoesNotExistException(String m) {
		super(m);
	}
}
