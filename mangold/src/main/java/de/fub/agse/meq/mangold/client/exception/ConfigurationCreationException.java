package de.fub.agse.meq.mangold.client.exception;

public class ConfigurationCreationException extends Exception {
	
	private static final long serialVersionUID = -1061416309463455788L;

	public ConfigurationCreationException() {
		super("Configuration could not be properly created!");
	}
	
	public ConfigurationCreationException(String message) {
		super(message);
	}
}
