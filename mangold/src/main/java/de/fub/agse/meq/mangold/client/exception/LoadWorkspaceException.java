package de.fub.agse.meq.mangold.client.exception;

public class LoadWorkspaceException extends Exception {
	
	private static final long serialVersionUID = -1061416309463455788L;

	public LoadWorkspaceException() {
		super("Could not successfully load Workpsace!");
	}
	
	public LoadWorkspaceException(String message) {
		super(message);
	}
}
