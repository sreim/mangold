package de.fub.agse.meq.mangold.client.exception;

public class ParseComponentException extends Exception {
	
	private static final long serialVersionUID = -1061416309463455788L;

	public ParseComponentException() {
		super("Component could not correctly be parsed!");
	}
	
	public ParseComponentException(String message) {
		super(message);
	}
}
