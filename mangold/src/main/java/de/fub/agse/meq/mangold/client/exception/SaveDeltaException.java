package de.fub.agse.meq.mangold.client.exception;

public class SaveDeltaException extends Exception {

	private static final long serialVersionUID = -620901729499147051L;

	public SaveDeltaException() {
		super("Delta could not successfully be persisted");
	}
	
	public SaveDeltaException(String message) {
		super(message);
	}
}
