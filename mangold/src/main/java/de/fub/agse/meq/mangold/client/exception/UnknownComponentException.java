package de.fub.agse.meq.mangold.client.exception;

public class UnknownComponentException extends Exception {

	private static final long serialVersionUID = -4378362330462759064L;
	
	
	public UnknownComponentException() {
		super("Unknown component");
	}
	
	public UnknownComponentException(String m) {
		super(m);
	}
}
