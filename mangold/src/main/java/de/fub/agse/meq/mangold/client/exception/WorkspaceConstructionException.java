package de.fub.agse.meq.mangold.client.exception;

public class WorkspaceConstructionException extends Exception {
	
	private static final long serialVersionUID = -1061416309463455788L;

	public WorkspaceConstructionException() {
		super("Workspace could not be properly created!");
	}
	
	public WorkspaceConstructionException(String message) {
		super(message);
	}
}
