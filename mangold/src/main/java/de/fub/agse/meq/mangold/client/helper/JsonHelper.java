package de.fub.agse.meq.mangold.client.helper;

import java.util.ArrayList;
import java.util.List;

import com.google.gwt.json.client.JSONObject;
import com.google.gwt.json.client.JSONParser;
import com.google.gwt.json.client.JSONValue;

import de.fub.agse.meq.mangold.client.Configuration;
import de.fub.agse.meq.mangold.client.I18N;
import de.fub.agse.meq.mangold.client.component.ComponentFactory;
import de.fub.agse.meq.mangold.client.components.Component;
import de.fub.agse.meq.mangold.client.exception.ComponentCreationException;
import de.fub.agse.meq.mangold.client.exception.ParseComponentException;
import de.fub.agse.meq.mangold.client.serialization.JsonElement;
import de.fub.agse.meq.mangold.client.serialization.JsonWrapper;
import de.fub.agse.meq.mangold.client.ui.helper.AlertifyHelper;

public class JsonHelper {

	public static List<Component> convertToComponents(
			List<JsonWrapper> components) throws ParseComponentException {
		List<Component> results = new ArrayList<Component>();
		boolean receivedCreationError = false;
		for (JsonWrapper wrappedComponent : components) {
			try {
				results.add(convertToComponent(wrappedComponent));
			} catch (ComponentCreationException e) {
				receivedCreationError = true;
			}
		}
		if (receivedCreationError) {
			AlertifyHelper.showErrorLog(I18N.Util.get()
					.failedCreatingComponents());
		}
		return results;
	}
	
	public static List<Component> convertToComponents(JSONValue json) throws ComponentCreationException {
		JSONObject obj = null;
		List<Component> results = new ArrayList<Component>();
		if((obj = json.isObject()) != null) {
			JSONValue val = null;
			JSONObject elem = null;
			for (String key : obj.keySet()) {
				val = obj.get(key);
				if((elem = val.isObject()) != null) {
					Component c = ComponentFactory.createComponent(elem);
					if(c != null)
						results.add(c);
				}
			}
			return results;
		}
		return null;
	}

	public static Component convertToComponent(JsonWrapper jsonWrapper)
			throws ParseComponentException, ComponentCreationException {
		Component component = null;

		JSONObject obj = convertToJson(jsonWrapper.getJson());
		
		//TODO JSON validation:
		//name, input/output exists?

		component = ComponentFactory.createComponent(obj);
		return component;
	}
	
	public static List<JsonWrapper> convertToPersistable(List<Component> components) {
		List<JsonWrapper> list = new ArrayList<JsonWrapper>();
		for (Component component : components) {
			Configuration config = component.getFieldConfiguration();
			if(config != null) {
				JsonWrapper json = new JsonElement(config.toJson().toString());
				list.add(json);
			}
		}
		return list;
	}

	public static JSONObject convertToJson(String jsonStr)
			throws ParseComponentException {
		JSONValue val = null;
		try {
			val = JSONParser.parseStrict(jsonStr);
		} catch (Exception e) {
			
		}
		JSONObject componentObj = null;

		if (val != null && (componentObj = val.isObject()) != null)
			return componentObj;
		else
			throw new ParseComponentException(
					"Passed json is not a valid JSON Object");
	}

}
