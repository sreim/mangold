package de.fub.agse.meq.mangold.client.interaction;

public enum MoveDirection {
	LEFT, RIGHT, UP, DOWN;
}
