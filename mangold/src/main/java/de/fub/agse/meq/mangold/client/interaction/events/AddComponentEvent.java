package de.fub.agse.meq.mangold.client.interaction.events;

import com.google.web.bindery.event.shared.Event;
import com.google.web.bindery.event.shared.EventBus;
import com.google.web.bindery.event.shared.HandlerRegistration;

import de.fub.agse.meq.mangold.client.components.Component;

public class AddComponentEvent extends Event<AddComponentEvent.Handler> {

	public interface Handler {
		void onAddComponent(AddComponentEvent event);
	}

	private final Component component;
	
	public AddComponentEvent() {
		super();
		component = null;
	}

	public AddComponentEvent(Component component) {
		super();
		this.component = component;
	}

	public static HandlerRegistration register(EventBus eventBus,
			AddComponentEvent.Handler handler) {
		return eventBus.addHandler(TYPE, handler);
	}

	private static final Type<AddComponentEvent.Handler> TYPE = new Type<AddComponentEvent.Handler>();

	@Override
	public Type<AddComponentEvent.Handler> getAssociatedType() {
		return TYPE;
	}

	@Override
	protected void dispatch(AddComponentEvent.Handler handler) {
		handler.onAddComponent(this);
	}

	public Component getComponent() {
		return this.component;
	}
}
