package de.fub.agse.meq.mangold.client.interaction.events;

import com.google.web.bindery.event.shared.Event;
import com.google.web.bindery.event.shared.EventBus;
import com.google.web.bindery.event.shared.HandlerRegistration;

import de.fub.agse.meq.mangold.client.components.Component;

public class AddNewComponentEvent extends Event<AddNewComponentEvent.Handler> {

	public interface Handler {
		void onAddComponent(AddNewComponentEvent event);
	}

	private final Component component;
	
	public AddNewComponentEvent() {
		super();
		component = null;
	}

	public AddNewComponentEvent(Component component) {
		super();
		this.component = component;
	}

	public static HandlerRegistration register(EventBus eventBus,
			AddNewComponentEvent.Handler handler) {
		return eventBus.addHandler(TYPE, handler);
	}

	private static final Type<AddNewComponentEvent.Handler> TYPE = new Type<AddNewComponentEvent.Handler>();

	@Override
	public Type<AddNewComponentEvent.Handler> getAssociatedType() {
		return TYPE;
	}

	@Override
	protected void dispatch(AddNewComponentEvent.Handler handler) {
		handler.onAddComponent(this);
	}

	public Component getComponent() {
		return this.component;
	}
}
