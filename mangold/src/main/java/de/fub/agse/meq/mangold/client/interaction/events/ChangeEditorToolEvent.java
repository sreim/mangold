package de.fub.agse.meq.mangold.client.interaction.events;

import com.google.web.bindery.event.shared.Event;
import com.google.web.bindery.event.shared.EventBus;
import com.google.web.bindery.event.shared.HandlerRegistration;

public class ChangeEditorToolEvent extends Event<ChangeEditorToolEvent.Handler> {

	public interface Handler {
		void onChangeEditorTool(ChangeEditorToolEvent event);
	}
	
	private EditorToolType toolType;
	
	public enum EditorToolType {
		POINTER, REMOVE, CONNECT, INSPECT;
	}
	
	public ChangeEditorToolEvent(EditorToolType type) {
		super();
		this.toolType = type;
	}

	public static HandlerRegistration register(EventBus eventBus,
			ChangeEditorToolEvent.Handler handler) {
		return eventBus.addHandler(TYPE, handler);
	}

	private static final Type<ChangeEditorToolEvent.Handler> TYPE = new Type<ChangeEditorToolEvent.Handler>();

	@Override
	public Type<ChangeEditorToolEvent.Handler> getAssociatedType() {
		return TYPE;
	}

	@Override
	protected void dispatch(ChangeEditorToolEvent.Handler handler) {
		handler.onChangeEditorTool(this);
	}
	
	public EditorToolType getSelectedEditorToolType() {
		return this.toolType;
	}
}
