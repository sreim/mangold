package de.fub.agse.meq.mangold.client.interaction.events;

import com.google.web.bindery.event.shared.Event;
import com.google.web.bindery.event.shared.EventBus;
import com.google.web.bindery.event.shared.HandlerRegistration;

import de.fub.agse.meq.mangold.client.components.Component;

public class ComponentPositionChangedEvent extends Event<ComponentPositionChangedEvent.Handler> {

	public interface Handler {
		void onPositionChanged(ComponentPositionChangedEvent event);
	}

	private final Component component;
	
	public ComponentPositionChangedEvent() {
		super();
		component = null;
	}

	public ComponentPositionChangedEvent(Component component) {
		super();
		this.component = component;
	}

	public static HandlerRegistration register(EventBus eventBus,
			ComponentPositionChangedEvent.Handler handler) {
		return eventBus.addHandler(TYPE, handler);
	}

	private static final Type<ComponentPositionChangedEvent.Handler> TYPE = new Type<ComponentPositionChangedEvent.Handler>();

	@Override
	public Type<ComponentPositionChangedEvent.Handler> getAssociatedType() {
		return TYPE;
	}

	@Override
	protected void dispatch(ComponentPositionChangedEvent.Handler handler) {
		handler.onPositionChanged(this);
	}

	public Component getComponent() {
		return this.component;
	}
}
