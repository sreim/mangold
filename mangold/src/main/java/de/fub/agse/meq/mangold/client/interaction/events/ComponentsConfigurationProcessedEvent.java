package de.fub.agse.meq.mangold.client.interaction.events;

import com.google.web.bindery.event.shared.Event;
import com.google.web.bindery.event.shared.EventBus;
import com.google.web.bindery.event.shared.HandlerRegistration;

public class ComponentsConfigurationProcessedEvent extends Event<ComponentsConfigurationProcessedEvent.Handler> {

	public interface Handler {
		void onConfigurationProcessed(ComponentsConfigurationProcessedEvent event);
	}
	
	public ComponentsConfigurationProcessedEvent() {
		super();
	}

	public static HandlerRegistration register(EventBus eventBus,
			ComponentsConfigurationProcessedEvent.Handler handler) {
		return eventBus.addHandler(TYPE, handler);
	}
	
	private static final Type<ComponentsConfigurationProcessedEvent.Handler> TYPE = new Type<ComponentsConfigurationProcessedEvent.Handler>();

	@Override
	public Type<ComponentsConfigurationProcessedEvent.Handler> getAssociatedType() {
		return TYPE;
	}

	@Override
	protected void dispatch(ComponentsConfigurationProcessedEvent.Handler handler) {
		handler.onConfigurationProcessed(this);
	}
}
