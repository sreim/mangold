package de.fub.agse.meq.mangold.client.interaction.events;

import com.google.web.bindery.event.shared.Event;
import com.google.web.bindery.event.shared.EventBus;
import com.google.web.bindery.event.shared.HandlerRegistration;

public class ConfigureBtnClickedEvent extends Event<ConfigureBtnClickedEvent.Handler> {

	public interface Handler {
		void onConfigureClicked(ConfigureBtnClickedEvent event);
	}
	
	public ConfigureBtnClickedEvent() {
		super();
	}

	public static HandlerRegistration register(EventBus eventBus,
			ConfigureBtnClickedEvent.Handler handler) {
		return eventBus.addHandler(TYPE, handler);
	}

	private static final Type<ConfigureBtnClickedEvent.Handler> TYPE = new Type<ConfigureBtnClickedEvent.Handler>();

	@Override
	public Type<ConfigureBtnClickedEvent.Handler> getAssociatedType() {
		return TYPE;
	}

	@Override
	protected void dispatch(ConfigureBtnClickedEvent.Handler handler) {
		handler.onConfigureClicked(this);
	}
}
