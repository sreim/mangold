package de.fub.agse.meq.mangold.client.interaction.events;

import com.google.web.bindery.event.shared.Event;
import com.google.web.bindery.event.shared.EventBus;
import com.google.web.bindery.event.shared.HandlerRegistration;

public class FieldConfigurationChangedEvent extends Event<FieldConfigurationChangedEvent.Handler> {

	private int panelHash;
	
	public interface Handler {
		void onFieldConfigChanged(FieldConfigurationChangedEvent event);
	}

	public FieldConfigurationChangedEvent(int panelHash) {
		this.panelHash = panelHash;
	}

	public static HandlerRegistration register(EventBus eventBus,
			FieldConfigurationChangedEvent.Handler handler) {
		return eventBus.addHandler(TYPE, handler);
	}

	private static final Type<FieldConfigurationChangedEvent.Handler> TYPE = new Type<FieldConfigurationChangedEvent.Handler>();

	@Override
	public Type<FieldConfigurationChangedEvent.Handler> getAssociatedType() {
		return TYPE;
	}

	@Override
	protected void dispatch(FieldConfigurationChangedEvent.Handler handler) {
		handler.onFieldConfigChanged(this);
	}
	
	public int getPanelHash() {
		return this.panelHash;
	}

}
