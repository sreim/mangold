package de.fub.agse.meq.mangold.client.interaction.events;

import com.google.web.bindery.event.shared.Event;
import com.google.web.bindery.event.shared.EventBus;
import com.google.web.bindery.event.shared.HandlerRegistration;

public class LoadWorkspaceEvent extends Event<LoadWorkspaceEvent.Handler> {

	public interface Handler {
		void onLoadWorkspace(LoadWorkspaceEvent event);
	}
	
	public LoadWorkspaceEvent() {
		super();
	}

	public static HandlerRegistration register(EventBus eventBus,
			LoadWorkspaceEvent.Handler handler) {
		return eventBus.addHandler(TYPE, handler);
	}
	
	private static final Type<LoadWorkspaceEvent.Handler> TYPE = new Type<LoadWorkspaceEvent.Handler>();

	@Override
	public Type<LoadWorkspaceEvent.Handler> getAssociatedType() {
		return TYPE;
	}

	@Override
	protected void dispatch(LoadWorkspaceEvent.Handler handler) {
		handler.onLoadWorkspace(this);
	}
}
