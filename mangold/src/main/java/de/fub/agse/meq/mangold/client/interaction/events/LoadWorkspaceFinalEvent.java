package de.fub.agse.meq.mangold.client.interaction.events;

import com.google.web.bindery.event.shared.Event;
import com.google.web.bindery.event.shared.EventBus;
import com.google.web.bindery.event.shared.HandlerRegistration;

public class LoadWorkspaceFinalEvent extends Event<LoadWorkspaceFinalEvent.Handler> {

	public interface Handler {
		void onLoadWorkspace(LoadWorkspaceFinalEvent event);
	}
	
	private String workspace;
	
	public LoadWorkspaceFinalEvent() {
		super();
	}
	
	public LoadWorkspaceFinalEvent(String workspace) {
		this.workspace = workspace;
	}

	public static HandlerRegistration register(EventBus eventBus,
			LoadWorkspaceFinalEvent.Handler handler) {
		return eventBus.addHandler(TYPE, handler);
	}
	
	private static final Type<LoadWorkspaceFinalEvent.Handler> TYPE = new Type<LoadWorkspaceFinalEvent.Handler>();

	@Override
	public Type<LoadWorkspaceFinalEvent.Handler> getAssociatedType() {
		return TYPE;
	}

	@Override
	protected void dispatch(LoadWorkspaceFinalEvent.Handler handler) {
		handler.onLoadWorkspace(this);
	}
	
	public String getWorkspace() {
		return this.workspace;
	}
}
