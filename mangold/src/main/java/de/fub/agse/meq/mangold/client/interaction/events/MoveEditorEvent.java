package de.fub.agse.meq.mangold.client.interaction.events;

import com.google.web.bindery.event.shared.Event;
import com.google.web.bindery.event.shared.EventBus;
import com.google.web.bindery.event.shared.HandlerRegistration;

import de.fub.agse.meq.mangold.client.interaction.MoveDirection;

public class MoveEditorEvent extends Event<MoveEditorEvent.Handler> {
	
	public interface Handler {
		void onMoveEditor(MoveEditorEvent event);
	}

	private final MoveDirection direction;
	
	public MoveEditorEvent() {
		super();
		direction = MoveDirection.LEFT;
	}

	public MoveEditorEvent(MoveDirection direction) {
		super();
		this.direction = direction;
	}

	public static HandlerRegistration register(EventBus eventBus,
			MoveEditorEvent.Handler handler) {
		return eventBus.addHandler(TYPE, handler);
	}

	private static final Type<MoveEditorEvent.Handler> TYPE = new Type<MoveEditorEvent.Handler>();

	@Override
	public Type<MoveEditorEvent.Handler> getAssociatedType() {
		return TYPE;
	}

	@Override
	protected void dispatch(MoveEditorEvent.Handler handler) {
		handler.onMoveEditor(this);
	}

	public MoveDirection getDragDirection() {
		return this.direction;
	}
}
