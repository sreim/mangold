package de.fub.agse.meq.mangold.client.interaction.events;

import com.google.web.bindery.event.shared.Event;
import com.google.web.bindery.event.shared.EventBus;
import com.google.web.bindery.event.shared.HandlerRegistration;

public class NewWorkspaceEvent extends Event<NewWorkspaceEvent.Handler> {

	public interface Handler {
		void onNewWorkspace(NewWorkspaceEvent event);
	}
	
	public NewWorkspaceEvent() {
		super();
	}

	public static HandlerRegistration register(EventBus eventBus,
			NewWorkspaceEvent.Handler handler) {
		return eventBus.addHandler(TYPE, handler);
	}
	
	private static final Type<NewWorkspaceEvent.Handler> TYPE = new Type<NewWorkspaceEvent.Handler>();

	@Override
	public Type<NewWorkspaceEvent.Handler> getAssociatedType() {
		return TYPE;
	}

	@Override
	protected void dispatch(NewWorkspaceEvent.Handler handler) {
		handler.onNewWorkspace(this);
	}
}
