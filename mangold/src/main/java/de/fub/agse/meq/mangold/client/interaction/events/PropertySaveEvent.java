package de.fub.agse.meq.mangold.client.interaction.events;

import com.google.web.bindery.event.shared.Event;
import com.google.web.bindery.event.shared.EventBus;
import com.google.web.bindery.event.shared.HandlerRegistration;

public class PropertySaveEvent extends Event<PropertySaveEvent.Handler> {

	public interface Handler {
		void onPropertySave(PropertySaveEvent event);
	}
	
	public PropertySaveEvent() {
		super();
	}

	public static HandlerRegistration register(EventBus eventBus,
			PropertySaveEvent.Handler handler) {
		return eventBus.addHandler(TYPE, handler);
	}

	private static final Type<PropertySaveEvent.Handler> TYPE = new Type<PropertySaveEvent.Handler>();

	@Override
	public Type<PropertySaveEvent.Handler> getAssociatedType() {
		return TYPE;
	}

	@Override
	protected void dispatch(PropertySaveEvent.Handler handler) {
		handler.onPropertySave(this);
	}
}
