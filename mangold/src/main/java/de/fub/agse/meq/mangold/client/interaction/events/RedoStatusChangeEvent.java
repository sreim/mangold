package de.fub.agse.meq.mangold.client.interaction.events;

import com.google.web.bindery.event.shared.Event;
import com.google.web.bindery.event.shared.EventBus;
import com.google.web.bindery.event.shared.HandlerRegistration;

public class RedoStatusChangeEvent extends Event<RedoStatusChangeEvent.Handler> {

	public interface Handler {
		void onRedoStatusChange(RedoStatusChangeEvent event);
	}

	private final boolean redoEnabled;
	
	public RedoStatusChangeEvent() {
		super();
		redoEnabled = false;
	}

	public RedoStatusChangeEvent(boolean enabled) {
		super();
		this.redoEnabled = enabled;
	}

	public static HandlerRegistration register(EventBus eventBus,
			RedoStatusChangeEvent.Handler handler) {
		return eventBus.addHandler(TYPE, handler);
	}

	private static final Type<RedoStatusChangeEvent.Handler> TYPE = new Type<RedoStatusChangeEvent.Handler>();

	@Override
	public Type<RedoStatusChangeEvent.Handler> getAssociatedType() {
		return TYPE;
	}

	@Override
	protected void dispatch(RedoStatusChangeEvent.Handler handler) {
		handler.onRedoStatusChange(this);
	}

	public boolean getRedoEnabled() {
		return this.redoEnabled;
	}
}
