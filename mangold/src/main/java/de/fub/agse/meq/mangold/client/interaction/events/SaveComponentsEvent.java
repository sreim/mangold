package de.fub.agse.meq.mangold.client.interaction.events;

import com.google.web.bindery.event.shared.Event;
import com.google.web.bindery.event.shared.EventBus;
import com.google.web.bindery.event.shared.HandlerRegistration;

public class SaveComponentsEvent extends Event<SaveComponentsEvent.Handler> {

	public interface Handler {
		void onSaveComponent(SaveComponentsEvent event);
	}
	
	public SaveComponentsEvent() {
		super();
	}

	public static HandlerRegistration register(EventBus eventBus,
			SaveComponentsEvent.Handler handler) {
		return eventBus.addHandler(TYPE, handler);
	}
	
	private static final Type<SaveComponentsEvent.Handler> TYPE = new Type<SaveComponentsEvent.Handler>();

	@Override
	public Type<SaveComponentsEvent.Handler> getAssociatedType() {
		return TYPE;
	}

	@Override
	protected void dispatch(SaveComponentsEvent.Handler handler) {
		handler.onSaveComponent(this);
	}
}
