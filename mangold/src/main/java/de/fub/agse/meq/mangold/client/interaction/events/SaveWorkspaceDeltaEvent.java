package de.fub.agse.meq.mangold.client.interaction.events;

import com.google.web.bindery.event.shared.Event;
import com.google.web.bindery.event.shared.EventBus;
import com.google.web.bindery.event.shared.HandlerRegistration;

public class SaveWorkspaceDeltaEvent extends Event<SaveWorkspaceDeltaEvent.Handler> {

	public interface Handler {
		void onDeltaSave(SaveWorkspaceDeltaEvent event);
	}
	
	public SaveWorkspaceDeltaEvent() {
		super();
	}

	public static HandlerRegistration register(EventBus eventBus,
			SaveWorkspaceDeltaEvent.Handler handler) {
		return eventBus.addHandler(TYPE, handler);
	}
	
	private static final Type<SaveWorkspaceDeltaEvent.Handler> TYPE = new Type<SaveWorkspaceDeltaEvent.Handler>();

	@Override
	public Type<SaveWorkspaceDeltaEvent.Handler> getAssociatedType() {
		return TYPE;
	}

	@Override
	protected void dispatch(SaveWorkspaceDeltaEvent.Handler handler) {
		handler.onDeltaSave(this);
	}
}
