package de.fub.agse.meq.mangold.client.interaction.events;

import com.google.web.bindery.event.shared.Event;
import com.google.web.bindery.event.shared.EventBus;
import com.google.web.bindery.event.shared.HandlerRegistration;

public class SaveWorkspaceEvent extends Event<SaveWorkspaceEvent.Handler> {

	public interface Handler {
		void onSaveWorkspace(SaveWorkspaceEvent event);
	}
	
	public SaveWorkspaceEvent() {
		super();
	}

	public static HandlerRegistration register(EventBus eventBus,
			SaveWorkspaceEvent.Handler handler) {
		return eventBus.addHandler(TYPE, handler);
	}
	
	private static final Type<SaveWorkspaceEvent.Handler> TYPE = new Type<SaveWorkspaceEvent.Handler>();

	@Override
	public Type<SaveWorkspaceEvent.Handler> getAssociatedType() {
		return TYPE;
	}

	@Override
	protected void dispatch(SaveWorkspaceEvent.Handler handler) {
		handler.onSaveWorkspace(this);
	}
}
