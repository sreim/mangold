package de.fub.agse.meq.mangold.client.interaction.events;

import com.google.web.bindery.event.shared.Event;
import com.google.web.bindery.event.shared.EventBus;
import com.google.web.bindery.event.shared.HandlerRegistration;

public class SaveWorkspaceFinalEvent extends Event<SaveWorkspaceFinalEvent.Handler> {

	public interface Handler {
		void onSaveWorkspace(SaveWorkspaceFinalEvent event);
	}
	
	public SaveWorkspaceFinalEvent() {
		super();
	}

	public static HandlerRegistration register(EventBus eventBus,
			SaveWorkspaceFinalEvent.Handler handler) {
		return eventBus.addHandler(TYPE, handler);
	}
	
	private static final Type<SaveWorkspaceFinalEvent.Handler> TYPE = new Type<SaveWorkspaceFinalEvent.Handler>();

	@Override
	public Type<SaveWorkspaceFinalEvent.Handler> getAssociatedType() {
		return TYPE;
	}

	@Override
	protected void dispatch(SaveWorkspaceFinalEvent.Handler handler) {
		handler.onSaveWorkspace(this);
	}
}
