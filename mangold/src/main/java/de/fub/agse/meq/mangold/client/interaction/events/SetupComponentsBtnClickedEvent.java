package de.fub.agse.meq.mangold.client.interaction.events;

import com.google.web.bindery.event.shared.Event;
import com.google.web.bindery.event.shared.EventBus;
import com.google.web.bindery.event.shared.HandlerRegistration;

public class SetupComponentsBtnClickedEvent extends Event<SetupComponentsBtnClickedEvent.Handler> {

	public interface Handler {
		void onSetupClicked(SetupComponentsBtnClickedEvent event);
	}
	
	public SetupComponentsBtnClickedEvent() {
		super();
	}

	public static HandlerRegistration register(EventBus eventBus,
			SetupComponentsBtnClickedEvent.Handler handler) {
		return eventBus.addHandler(TYPE, handler);
	}

	private static final Type<SetupComponentsBtnClickedEvent.Handler> TYPE = new Type<SetupComponentsBtnClickedEvent.Handler>();

	@Override
	public Type<SetupComponentsBtnClickedEvent.Handler> getAssociatedType() {
		return TYPE;
	}

	@Override
	protected void dispatch(SetupComponentsBtnClickedEvent.Handler handler) {
		handler.onSetupClicked(this);
	}
}
