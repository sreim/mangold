package de.fub.agse.meq.mangold.client.interaction.events;

import com.google.web.bindery.event.shared.Event;
import com.google.web.bindery.event.shared.EventBus;
import com.google.web.bindery.event.shared.HandlerRegistration;

public class UndoStatusChangeEvent extends Event<UndoStatusChangeEvent.Handler> {

	public interface Handler {
		void onUndoStatusChange(UndoStatusChangeEvent event);
	}

	private final boolean undoEnabled;
	
	public UndoStatusChangeEvent() {
		super();
		undoEnabled = false;
	}

	public UndoStatusChangeEvent(boolean enabled) {
		super();
		this.undoEnabled = enabled;
	}

	public static HandlerRegistration register(EventBus eventBus,
			UndoStatusChangeEvent.Handler handler) {
		return eventBus.addHandler(TYPE, handler);
	}

	private static final Type<UndoStatusChangeEvent.Handler> TYPE = new Type<UndoStatusChangeEvent.Handler>();

	@Override
	public Type<UndoStatusChangeEvent.Handler> getAssociatedType() {
		return TYPE;
	}

	@Override
	protected void dispatch(UndoStatusChangeEvent.Handler handler) {
		handler.onUndoStatusChange(this);
	}

	public boolean getUndoEnabled() {
		return this.undoEnabled;
	}
}
