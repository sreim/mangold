package de.fub.agse.meq.mangold.client.interaction.events;

import com.google.web.bindery.event.shared.Event;
import com.google.web.bindery.event.shared.EventBus;
import com.google.web.bindery.event.shared.HandlerRegistration;

import de.fub.agse.meq.mangold.client.UserInfo;

public class UserStatusChangeEvent extends Event<UserStatusChangeEvent.Handler> {

	public interface Handler {
		void onUserStatusChange(UserStatusChangeEvent event);
	}

	private final UserInfo user;
	
	public UserStatusChangeEvent() {
		super();
		user = null;
	}

	public UserStatusChangeEvent(UserInfo user) {
		super();
		this.user = user;
	}

	public static HandlerRegistration register(EventBus eventBus,
			UserStatusChangeEvent.Handler handler) {
		return eventBus.addHandler(TYPE, handler);
	}

	private static final Type<UserStatusChangeEvent.Handler> TYPE = new Type<UserStatusChangeEvent.Handler>();

	@Override
	public Type<UserStatusChangeEvent.Handler> getAssociatedType() {
		return TYPE;
	}

	@Override
	protected void dispatch(UserStatusChangeEvent.Handler handler) {
		handler.onUserStatusChange(this);
	}

	public UserInfo getUserInfo() {
		return this.user;
	}
}
