package de.fub.agse.meq.mangold.client.interaction.events;

import com.google.web.bindery.event.shared.Event;
import com.google.web.bindery.event.shared.EventBus;
import com.google.web.bindery.event.shared.HandlerRegistration;

public class ZoomEditorEvent extends Event<ZoomEditorEvent.Handler> {
	
	public enum Zoom {
		IN, OUT;
	}
	
	public interface Handler {
		void onZoomEditor(ZoomEditorEvent event);
	}

	private final Zoom zoom;
	
	public ZoomEditorEvent() {
		super();
		zoom = Zoom.IN;
	}

	public ZoomEditorEvent(Zoom zoom) {
		super();
		this.zoom = zoom;
	}

	public static HandlerRegistration register(EventBus eventBus,
			ZoomEditorEvent.Handler handler) {
		return eventBus.addHandler(TYPE, handler);
	}

	private static final Type<ZoomEditorEvent.Handler> TYPE = new Type<ZoomEditorEvent.Handler>();

	@Override
	public Type<ZoomEditorEvent.Handler> getAssociatedType() {
		return TYPE;
	}

	@Override
	protected void dispatch(ZoomEditorEvent.Handler handler) {
		handler.onZoomEditor(this);
	}

	public Zoom getZoom() {
		return this.zoom;
	}
}
