package de.fub.agse.meq.mangold.client.interaction.events.action;

import com.google.web.bindery.event.shared.Event;

public abstract class ActionEvent<T,R> extends Event<ActionEvent.Handler<T,R>> {
	
	private T target;
	private R result;
	
	private boolean undoAction = false;
	
	public interface Handler<T,R> {
		void onAction(ActionEvent<T,R> event);
	}

	public ActionEvent() {
		super();
	}

	public ActionEvent(T target) {
		super();
		this.target = target;
	}
	
	public T getTarget() {
		return this.target;
	}
	
	public void setResult(R result) {
		this.result = result;
	}
	
	public R getResult() {
		return this.result;
	}

	public boolean isUndoAction() {
		return undoAction;
	}

	public void setUndoAction(boolean undoAction) {
		this.undoAction = undoAction;
	}
}
