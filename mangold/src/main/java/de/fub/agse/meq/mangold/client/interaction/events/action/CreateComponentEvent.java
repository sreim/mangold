package de.fub.agse.meq.mangold.client.interaction.events.action;

import net.edzard.kinetic.Vector2d;

import com.google.web.bindery.event.shared.EventBus;
import com.google.web.bindery.event.shared.HandlerRegistration;

import de.fub.agse.meq.mangold.client.components.Component;
import de.fub.agse.meq.mangold.client.components.Presentable;

public class CreateComponentEvent extends ActionEvent<Component, Presentable> {

	final Vector2d position;
	
	public CreateComponentEvent(Component component, Vector2d position) {
		super(component);
		this.position = position;
	}
	
	public CreateComponentEvent(Component component, Vector2d position, boolean undoAction) {
		super(component);
		this.position = position;
		setUndoAction(undoAction);
	}
	
	private static final Type<Handler<Component, Presentable>> TYPE = new Type<Handler<Component, Presentable>>();
	
	@Override
	public Type<Handler<Component, Presentable>> getAssociatedType() {
		return TYPE;
	}
	
	public static HandlerRegistration register(EventBus eventBus,
			Handler<Component, Presentable> handler) {
		return eventBus.addHandler(TYPE, handler);
	}
	
	protected void dispatch(Handler<Component, Presentable> handler) {
		handler.onAction(this);
	}

	public Vector2d getPosition() {
		return position;
	
	}
}
