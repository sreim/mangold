package de.fub.agse.meq.mangold.client.interaction.events.action;

import com.google.web.bindery.event.shared.EventBus;
import com.google.web.bindery.event.shared.HandlerRegistration;

import de.fub.agse.meq.mangold.client.Configuration;
import de.fub.agse.meq.mangold.client.components.InputComponent;
import de.fub.agse.meq.mangold.client.components.OutputComponent;
import de.fub.agse.meq.mangold.client.components.Presentable;

public class CreateConnectionEvent extends ActionEvent<InputComponent,Presentable> {

	private final OutputComponent source;
	private final Configuration configuration;
	
	public CreateConnectionEvent(OutputComponent source, InputComponent target, Configuration configuration) {
		super(target);
		this.source = source;
		this.configuration = configuration;
	}
	
	public CreateConnectionEvent(OutputComponent source, InputComponent target, Configuration configuration, boolean undoAction) {
		super(target);
		this.source = source;
		this.configuration = configuration;
		setUndoAction(undoAction);
	}
	
	private static final Type<Handler<InputComponent,Presentable>> TYPE = new Type<Handler<InputComponent,Presentable>>();
	
	@Override
	public Type<Handler<InputComponent,Presentable>> getAssociatedType() {
		return TYPE;
	}
	
	public static HandlerRegistration register(EventBus eventBus,
			Handler<InputComponent,Presentable> handler) {
		return eventBus.addHandler(TYPE, handler);
	}
	
	protected void dispatch(Handler<InputComponent,Presentable> handler) {
		handler.onAction(this);
	}

	public Configuration getConfiguration() {
		return configuration;
	}

	public OutputComponent getSource() {
		return source;
	}
}
