package de.fub.agse.meq.mangold.client.interaction.events.action;

import com.google.web.bindery.event.shared.EventBus;
import com.google.web.bindery.event.shared.HandlerRegistration;

import de.fub.agse.meq.mangold.client.components.Presentable;

public class RemoveEvent extends ActionEvent<Presentable, Integer> {
	
	public RemoveEvent(Presentable target) {
		super(target);
	}
	
	public RemoveEvent(Presentable target, boolean undoAction) {
		super(target);
		setUndoAction(undoAction);
	}
	
	private static final Type<Handler<Presentable, Integer>> TYPE = new Type<Handler<Presentable, Integer>>();
	
	@Override
	public Type<Handler<Presentable, Integer>> getAssociatedType() {
		return TYPE;
	}
	
	public static HandlerRegistration register(EventBus eventBus,
			Handler<Presentable, Integer> handler) {
		return eventBus.addHandler(TYPE, handler);
	}
	
	protected void dispatch(Handler<Presentable, Integer> handler) {
		handler.onAction(this);
	}
}
