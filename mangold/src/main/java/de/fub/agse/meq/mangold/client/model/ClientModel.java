package de.fub.agse.meq.mangold.client.model;

import java.security.MessageDigest;
import java.util.ArrayList;
import java.util.List;

import com.google.gwt.core.client.GWT;
import com.google.web.bindery.event.shared.EventBus;
import com.google.web.bindery.event.shared.SimpleEventBus;

import de.fub.agse.meq.mangold.client.ExecutionHistoryController;
import de.fub.agse.meq.mangold.client.UserInfo;
import de.fub.agse.meq.mangold.client.Workspace;
import de.fub.agse.meq.mangold.client.components.Component;

public class ClientModel {
	
	private static ClientModel instance;
	
	public static ClientModel getInstance() {
		if(instance == null) {
			instance = new ClientModel();
		}
		return instance;
	}
	
	private ClientModel() {
		this.components = new ArrayList<Component>();
		this.historyController = new ExecutionHistoryController();
	}
	
	public static final EventBus EVENT_BUS = GWT.create(SimpleEventBus.class);
	
	// END STATIC
	
	private UserInfo userInfo;
	private List<Component> components;
	private Workspace currentWorkspace;
	private final ExecutionHistoryController historyController;

	public UserInfo getUserInfo() {
		return userInfo;
	}

	public void setUserInfo(UserInfo userInfo) {
		this.userInfo = userInfo;
	}
	
	private static String toHexString(byte[] bytes) {
	    char[] hexArray = {'0','1','2','3','4','5','6','7','8','9','a','b','c','d','e','f'};
	    char[] hexChars = new char[bytes.length * 2];
	    int v;
	    for ( int j = 0; j < bytes.length; j++ ) {
	        v = bytes[j] & 0xFF;
	        hexChars[j*2] = hexArray[v/16];
	        hexChars[j*2 + 1] = hexArray[v%16];
	    }
	    return new String(hexChars);
	}
	
	public String getGravatarURL() {
		
		// TODO: default picture URL
		
		String hash; 
		try {
			MessageDigest md = MessageDigest.getInstance("MD5");
			md.reset();
			md.update(this.userInfo.getMailAddress().trim().toLowerCase().getBytes("ISO-8859-1"));
			hash = toHexString(md.digest());
	    } catch (Exception e) {
	    	hash = "00000000000000000000000000000000";
	    	// TODO: log exception
	    }
		final StringBuffer sb = new StringBuffer("http://www.gravatar.com/avatar/");
		sb.append(hash);
		sb.append("?s=28");
		return sb.toString();
	}

	public List<Component> getComponents() {
		return components;
	}

	public void setComponents(List<Component> components) {
		this.components = components;
	}
	
	public void addComponent(Component component) {
		this.components.add(component);
	}

	public Workspace getCurrentWorkspace() {
		if(currentWorkspace == null)
			currentWorkspace = new Workspace();
		return currentWorkspace;
	}

	public void setCurrentWorkspace(Workspace currentWorkspace) {
		this.currentWorkspace = currentWorkspace;
	}

	public ExecutionHistoryController getHistoryController() {
		return historyController;
	}
	
}
