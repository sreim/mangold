package de.fub.agse.meq.mangold.client.resources;

import com.google.gwt.core.client.GWT;
import com.google.gwt.resources.client.ClientBundle;
import com.google.gwt.resources.client.CssResource;
import com.google.gwt.resources.client.ImageResource;

public interface TheResources extends ClientBundle {
	
	public static final TheResources INSTANCE = GWT.create(TheResources.class);
	
	@Source("MangoldStyle.css")
	public CssResource css();
	
	@Source("IconDatasource.png")
	public ImageResource iconDataSource();
	
	@Source("IconInfosink.png")
	public ImageResource iconInfoSink();
		
	@Source("IconMenu_Remove.png")
	public ImageResource iconMenuRemove();
	
	@Source("IconMenu_Input.png")
	public ImageResource iconMenuAddInput();

	@Source("IconMenu_Output.png")
	public ImageResource iconMenuAddOutput();

	@Source("IconMenu_Point.png")
	public ImageResource iconMenuPoint();
	
	@Source("IconMenu_Connect.png")
	public ImageResource iconMenuConnect();
	
	@Source("IconMenu_Inspect.png")
	public ImageResource iconMenuInspect();
}