package de.fub.agse.meq.mangold.client.serialization;

public class JsonElement implements JsonWrapper {

	private static final long serialVersionUID = 5372386241873730592L;
	
	private String json;
	
	public JsonElement() {}
	
	public JsonElement(String json) {
		this.json = json;
	}

	@Override
	public String getJson() {
		return this.json;
	}

}
