package de.fub.agse.meq.mangold.client.serialization;

import java.io.Serializable;

public interface JsonWrapper extends Serializable {
	
	public String getJson();
	
}
