package de.fub.agse.meq.mangold.client.service;

import java.util.List;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;

import de.fub.agse.meq.mangold.client.serialization.JsonWrapper;

@RemoteServiceRelativePath("persistence")
public interface PersistenceService extends RemoteService {

	List<JsonWrapper> getComponents();

	void saveComponents(List<JsonWrapper> components);
}
