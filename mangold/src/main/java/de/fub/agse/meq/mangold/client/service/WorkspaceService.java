package de.fub.agse.meq.mangold.client.service;

import java.util.List;
import java.util.Map;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;

import de.fub.agse.meq.mangold.client.LoginInfo;
import de.fub.agse.meq.mangold.client.UserInfo;
import de.fub.agse.meq.mangold.client.dat.SessionUserException;
import de.fub.agse.meq.mangold.client.exception.ComponentDoesNotExistException;
import de.fub.agse.meq.mangold.client.exception.SaveDeltaException;
import de.fub.agse.meq.mangold.client.serialization.JsonWrapper;

@RemoteServiceRelativePath("workspace")
public interface WorkspaceService extends RemoteService {
	
	LoginInfo login(String requestUri, String workspaceName);
	UserInfo login(String username, String password, boolean remember);
	UserInfo relogin();
	
	void logout();
	
	List<String> createUUIDs(int number);
	
	JsonWrapper load(String name);
	
	JsonWrapper loadLatest();
	
	List<String> getWorkspacesToLoad();
	
	void save(String data) throws SessionUserException;
	
	void saveDelta(String workspaceName, Map<String, Map<String, JsonWrapper>> delta, 
			double zoom, double workspaceX, double workspaceY) 
					throws SaveDeltaException, ComponentDoesNotExistException;
}
