package de.fub.agse.meq.mangold.client.ui;

import gwtquery.plugins.draggable.client.events.DragStartEvent;
import gwtquery.plugins.draggable.client.events.DragStartEvent.DragStartEventHandler;
import gwtquery.plugins.draggable.client.events.DragStopEvent;
import gwtquery.plugins.draggable.client.events.DragStopEvent.DragStopEventHandler;
import gwtquery.plugins.draggable.client.gwt.DraggableWidget;
import net.edzard.kinetic.Vector2d;

import com.github.gwtbootstrap.client.ui.NavLink;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;

import de.fub.agse.meq.mangold.client.components.Component;
import de.fub.agse.meq.mangold.client.interaction.events.action.CreateComponentEvent;
import de.fub.agse.meq.mangold.client.model.ClientModel;

public class DraggableNavLink extends DraggableWidget<NavLink> {

	private final Component component;
	private NavLink navLink;

	public DraggableNavLink(final Component component) {
		this.navLink = new NavLink(component.getFieldConfiguration().get("name")
				.isString().stringValue());
		initWidget(this.navLink);
		this.component = component;
		this.useCloneAsHelper();

		setDraggingOpacity((float) 0.8);

		this.addDragStartHandler(new DragStartEventHandler() {
			public void onDragStart(DragStartEvent event) {
				//TODO use image
			}
		});

		this.addDragStopHandler(new DragStopEventHandler() {
			public void onDragStop(DragStopEvent event) {
				
			}
		});
		
		this.navLink.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				ClientModel.EVENT_BUS.fireEvent(new CreateComponentEvent(component, new Vector2d(400, 400)));
			}
		});
	}

	public void setText(String text) {
		getOriginalWidget().setText(text);
	}

	public Component getComponent() {
		return this.component;
	}

}
