package de.fub.agse.meq.mangold.client.ui;

import gwtquery.plugins.droppable.client.DroppableOptions.DroppableTolerance;
import gwtquery.plugins.droppable.client.events.DropEvent;
import gwtquery.plugins.droppable.client.events.DropEvent.DropEventHandler;
import gwtquery.plugins.droppable.client.gwt.DroppableWidget;
import de.fub.agse.meq.mangold.client.EditorView;
import de.fub.agse.meq.mangold.client.interaction.events.action.CreateComponentEvent;
import de.fub.agse.meq.mangold.client.model.ClientModel;

public class DroppableEditorView extends DroppableWidget<EditorView> {
	
	private EditorView editor;
	
	public DroppableEditorView() {
		this.editor = new EditorView();
		initWidget(this.editor);
		
		setTolerance(DroppableTolerance.POINTER);
		
		setDropConfiguration();
	}

	private void setDropConfiguration() {
		this.addDropHandler(new DropEventHandler() {
			public void onDrop(DropEvent event) {
		        DroppableWidget<EditorView> droppableLabel = (DroppableWidget<EditorView>)event.getDroppableWidget();
		        DraggableNavLink component  = (DraggableNavLink) event.getDraggableWidget();
		        
				ClientModel.EVENT_BUS.fireEvent(new CreateComponentEvent(component.getComponent(), editor.getUserPosition()));
			}
		});
	}
	
	public EditorView getEditor() {
		return this.editor;
	}
}
