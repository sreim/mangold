package de.fub.agse.meq.mangold.client.ui.helper;

public class AlertifyHelper {

	public static native void showSuccessLog(String value) /*-{
		$wnd.alertify.success(value);
	}-*/;

	public static native void showNotificationLog(String value) /*-{
		$wnd.alertify.log(value);
	}-*/;

	public static native void showErrorLog(String value) /*-{
		$wnd.alertify.error(value);
	}-*/;

	public static native void showAlert(String value) /*-{
		$wnd.alertify.alert(value);
	}-*/;

}
