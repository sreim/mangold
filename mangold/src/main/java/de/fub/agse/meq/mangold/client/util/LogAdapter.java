package de.fub.agse.meq.mangold.client.util;

import java.util.Date;
import java.util.logging.Formatter;
import java.util.logging.Level;
import java.util.logging.LogRecord;
import java.util.logging.Logger;

import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.logging.client.SystemLogHandler;


public class LogAdapter {
	
	private static final SystemLogHandler handler = new SystemLogHandler();
	
	public static LogAdapter get(Class<?> callerClass) {
		String className = callerClass.getName(); 
		Logger logger = Logger.getLogger(className.substring(className.lastIndexOf(".")+1));
		handler.setFormatter(new LogFormatter());
		logger.addHandler(handler);
		LogAdapter adapter = new LogAdapter(logger);
		return adapter;
	}
	
	private final Logger logger;
	
	private LogAdapter(Logger logger) {
		this.logger = logger;
	}
	
	public void severe(Object message) {
		logger.log(Level.SEVERE, message.toString());
	}
	
	public void debug(Object message) {
		logger.log(Level.CONFIG, message.toString());
	}
	
	public void info(Object message) {
		logger.log(Level.INFO, message.toString());
	}
	
	public void warn(Object message) {
		logger.log(Level.WARNING, message.toString());
	}
	
	public void fine(Object message) {
		logger.log(Level.FINE, message.toString());
	}
	
	private static final class LogFormatter extends Formatter {
		
		private final DateTimeFormat dtFormat = DateTimeFormat.getFormat("yyyy-MM-dd HH:mm:ss");
		
	    @Override
	    public String format(LogRecord record) {
	        StringBuilder sb = new StringBuilder();

	        sb.append(dtFormat.format(new Date(record.getMillis())))
	            .append(" ")
	            .append(record.getLoggerName())
	            .append(" - ")
	            .append(record.getLevel().getName())
	            .append(": ")
	            .append(formatMessage(record));

	        if (record.getThrown() != null) {
	            try {
	                sb.append(record.getThrown().getStackTrace());
	            } catch (Exception ex) {
	                // ignore
	            }
	        }

	        return sb.toString();
	    }
	}

}
