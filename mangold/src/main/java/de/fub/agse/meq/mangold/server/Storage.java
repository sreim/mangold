package de.fub.agse.meq.mangold.server;

import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.logging.Logger;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.EntityNotFoundException;
import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.memcache.MemcacheService;
import com.google.appengine.api.memcache.MemcacheServiceFactory;
import com.google.appengine.api.users.UserService;
import com.google.appengine.api.users.UserServiceFactory;


// Write-through memcache and datastore
public class Storage {
	
	private static final Logger log = Logger.getLogger(Storage.class.getName());
	
	public static final ObjectMapper mapper = new ObjectMapper();
	
	private static final MemcacheService memcache = MemcacheServiceFactory.getMemcacheService();
//	private static final Map<String, String> cache = new HashMap<String, String>();
	private static final UserService userService = UserServiceFactory.getUserService();
	private static final DatastoreService datastoreService = DatastoreServiceFactory.getDatastoreService();
	
	final private Key userKey;
	final private String workspaceName;
	
	public Storage(String workspaceName, String userName) {
//		this.userKey = KeyFactory.createKey(userKeyKind, userName);
		this.userKey = null;
		this.workspaceName = workspaceName;
	}
	
	public JsonNode read(String id) throws JsonProcessingException, IOException {
		
		// Lookup object
		Object obj = memcache.get(id);
		if (obj == null) {
			// Cache miss
			try {
				// Load from data storage
				final Entity entity = datastoreService.get(KeyFactory.createKey(userKey, workspaceName, id));
				final ObjectNode state = JsonNodeFactory.instance.objectNode();
				for (Map.Entry<String, Object> entry: entity.getProperties().entrySet()) {
					if (entry.getValue() instanceof String) {
						final JsonNode value = mapper.readTree((String)entry.getValue());
						state.put(entry.getKey(), value);
					} else {
						log.warning(String.format("No JsonNode: Entity %s with property %s of type %s", id, entry.getKey(), entry.getValue().getClass().toString()));
					}
				}		
				final String data = mapper.writeValueAsString(state);
				memcache.put(id, data);	// add to local cache
				return state;
			} catch (EntityNotFoundException e) {
				// Storage miss
				String msg = String.format("No object found in datastore for ID %s and User %s", id.toString(), userService.getCurrentUser().getUserId());
				log.warning(msg);
				return null;
			}
		} else if (obj instanceof String) {
			return mapper.readTree((String)obj);
		} else {
			// Type is wrong - local cache is corrupt. How did this happen?
			String msg = String.format("Object with ID %s for User %s has wrong type in memcache (should be %s, but is %s)", id.toString(), userService.getCurrentUser().getUserId(), JsonNode.class.toString(), obj.getClass().toString());
			log.severe(msg);
			throw new IOException(msg);
		}
	}
	
	// Write-through cache
	public void write(JsonNode state) throws JsonGenerationException, JsonMappingException, IOException {
		
		// Add to local cache
		final String id = state.get("id").asText();
		final String data = mapper.writeValueAsString(state);
//		userService.
		//TODO Soft / Weak Cache!?
//		memcache.put(id, data);
		
//		// Add to storage
		final Entity dataEntity = new Entity(workspaceName, id);
		final Iterator<String> it = state.fieldNames();
		while (it.hasNext()) {
			final String key = it.next();
			final String value = mapper.writeValueAsString(state.get(key));
			dataEntity.setProperty(key, value);
		}
		datastoreService.put(dataEntity);
	}
	
	public boolean has(String id) {
		if (memcache.contains(id)) return true;
		else {
			try {
				datastoreService.get(KeyFactory.createKey(userKey, workspaceName, id));
				return true;
			} catch (EntityNotFoundException e) {
				return false;
			}
		}
	}
	
	public String getWorkspaceName() {
		return workspaceName;
	}
}
