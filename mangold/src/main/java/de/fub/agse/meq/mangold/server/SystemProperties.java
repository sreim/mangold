package de.fub.agse.meq.mangold.server;

public class SystemProperties {
	
	public static final String ADMIN_USERNAME = "admin";
	public static final String ADMIN_PASSWORD = "admin";
	
	public static final String COMPONENT_PROCESS_FIELD = "process";
	public static final String COMPONENT_TYPE_FIELD = "type";
	public static final String COMPONENT_NAME_FIELD = "Component Name";
	public static final String COMPONENT_POSITION_FIELD = "position";
	public static final String COMPONENT_DEFAULT_FIELD = "default";
	public static final String COMPONENT_ID_FIELD = "componentId";
	public static final String COMPONENT_VALUE_FIELD = "value";
	
	public static final String OUTPUT_NAME = "output";
	public static final String INPUT_NAME = "input";

	public static final String WORKSPACE_DEFAULT_NAME = "default";
	public static final String WORKSPACE_THINGS = "things";
	public static final String WORKSPACE_POSITION = "workspace_pos";
	public static final String WORKSPACE_ZOOM = "workspace_zoom";
	
	public static final String WORKSPACE_DELTA_ADDED = "added";
	public static final String WORKSPACE_DELTA_REMOVED = "removed";
	public static final String WORKSPACE_DELTA_CHANGED = "changed";
}
