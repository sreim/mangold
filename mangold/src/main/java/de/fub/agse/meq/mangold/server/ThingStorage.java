package de.fub.agse.meq.mangold.server;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;


// Write-through memcache and datastore
public class ThingStorage {
	
	private static final Logger log = Logger.getLogger(ThingStorage.class.getName());
	
	public static final ObjectMapper mapper = new ObjectMapper();
	
	private final Map<String, String> cache;
	private JsonNode position;
	private double zoom;
	
	final private String workspaceName;
	
	public ThingStorage(String workspaceName) {
		this.workspaceName = workspaceName;
		this.cache = new HashMap<String, String>();
	}
	
	public JsonNode read(String id) throws JsonProcessingException, IOException {
		
		// Lookup object
		Object obj = cache.get(id);
		if (obj == null) {
			// Cache miss
		} else if (obj instanceof String) {
			return mapper.readTree((String)obj);
		} else {
			// Type is wrong - local cache is corrupt. How did this happen?
			String msg = String.format("Object with ID %s has wrong type in memcache (should be %s, but is %s)", id.toString(), JsonNode.class.toString(), obj.getClass().toString());
			log.severe(msg);
			throw new IOException(msg);
		}
		return null;
	}
	
	public List<String> readAll() {
		List<String> list = new ArrayList<String>();
		for(String key : cache.keySet()) {
			list.add(cache.get(key));
		}
		return list;
	}
	
	public boolean remove(String id) {
		Object obj = cache.get(id);
		if (obj == null) 
			return false;
		String data = cache.remove(id);
		if(data == null)
			return false;
		return true;
	}
	
	public boolean contains(String id) {
		Object obj = cache.get(id);
		if (obj == null)
			return false;
		return true;
	}
	
	// Write-through cache
	public void write(JsonNode state) throws JsonGenerationException, JsonMappingException, IOException {
		
		// Add to local cache
		final String id = state.get("id").asText();
		final String data = mapper.writeValueAsString(state);
		//TODO Soft / Weak Cache!?
		cache.put(id, data);
	}
	
	public void write(String id, JsonNode state) throws JsonGenerationException, JsonMappingException, IOException {
		final String data  = mapper.writeValueAsString(state);
		cache.put(id, data);
	}
	
	public boolean has(String id) {
		if (cache.containsKey(id)) return true;
		return false;
	}
	
	public String getWorkspaceName() {
		return workspaceName;
	}

	public JsonNode getPosition() {
		return position;
	}

	public void setPosition(JsonNode position) {
		this.position = position;
	}

	public double getZoom() {
		return zoom;
	}

	public void setZoom(double zoom) {
		this.zoom = zoom;
	}
}
