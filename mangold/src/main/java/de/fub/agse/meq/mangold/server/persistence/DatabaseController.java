package de.fub.agse.meq.mangold.server.persistence;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.google.gwt.json.client.JSONNumber;
import com.google.gwt.json.client.JSONObject;
import com.google.gwt.json.client.JSONParser;
import com.google.gwt.json.client.JSONValue;

import de.fub.agse.meq.mangold.client.dat.DatabaseInitializationException;
import de.fub.agse.meq.mangold.client.dat.DatabaseNotInitializedException;
import de.fub.agse.meq.mangold.client.exception.ParseComponentException;

/**
 * This is a sample SQLite Database Controller.
 * 
 * TODO replace this class with real Database Connector
 * 
 * @author Steven Reim
 *
 */
public class DatabaseController {
	
	private static DatabaseController instance;
    private static Connection connection;
    private static final String DB_PATH = "/Users/sreim/Documents/mangold-sqlite/" + "mangold.db";
    
    private static boolean databaseCorrectlyInstantiated = true;

    static {
        try {
            Class.forName("org.sqlite.JDBC");
        } catch (ClassNotFoundException e) {
            System.err.println("Fehler beim Laden des JDBC-Treibers");
            e.printStackTrace();
            databaseCorrectlyInstantiated = false;
        }
    }
    
    private DatabaseController() {
    	if(databaseCorrectlyInstantiated)
    		this.initConnection();
    }
    
    public static DatabaseController getInstance() {
        if(instance == null)
        	instance = new DatabaseController();
        return instance;
    }
    
    private void initConnection() {
        try {
            if (connection != null)
                return;
            System.out.println("Creating Connection to Database...");
            connection = DriverManager.getConnection("jdbc:sqlite:" + DB_PATH);
            if (!connection.isClosed())
                System.out.println("...Connection established");
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

        Runtime.getRuntime().addShutdownHook(new Thread() {
            public void run() {
                try {
                    if (!connection.isClosed() && connection != null) {
                        connection.close();
                        if (connection.isClosed())
                            System.out.println("Connection to Database closed");
                    }
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        });
        
        try {
			initTables();
		} catch (DatabaseInitializationException e) {
			// TODO Do sth smart
			e.printStackTrace();
		}
    }
    
    private void initTables() throws DatabaseInitializationException {
		try {
			Statement stmt = connection.createStatement();
			stmt.executeUpdate("CREATE TABLE IF NOT EXISTS mng_components (NUMBER id, STRING rep)");
		} catch (SQLException e) {
			e.printStackTrace();
			throw new DatabaseInitializationException("Could not correctly set up Tables");
		}
	}

	public List<JSONObject> getComponents() throws DatabaseNotInitializedException {
		if(!databaseCorrectlyInstantiated) {
			throw new DatabaseNotInitializedException();
		}
    	List<JSONObject> list = new ArrayList<JSONObject>();
    	try {
			Statement stmt = connection.createStatement();
			
			ResultSet rs = stmt.executeQuery("select * from mng_components");
			JSONValue val = null;
			JSONObject obj = null;
			while(rs.next()) {
				val = JSONParser.parseStrict(rs.getString("rep"));
				if((obj = val.isObject()) != null)
					list.add(obj);
			}			
		} catch (SQLException e) {
			//TODO logging
			e.printStackTrace();
			return list;
		}
    	
    	return list;
    }
    
    public void addComponent(JSONObject component) throws ParseComponentException, DatabaseNotInitializedException {
    	if(!databaseCorrectlyInstantiated) {
			throw new DatabaseNotInitializedException();
		}
    	try {
			PreparedStatement ps = connection.prepareStatement("INSERT INTO mng_components (id, rep) VALUES (?, ?)");
			JSONNumber jsonId = null;
			if((jsonId = component.get("id").isNumber()) != null) {
				double id = jsonId.doubleValue();
				ps.setDouble(0, id);
				ps.setString(1, component.toString());
			} else {
				throw new ParseComponentException("Could not correctly fetch Component ID");
			}
		} catch (SQLException e) {
			//TODO logging
			e.printStackTrace();
			throw new ParseComponentException(e.getMessage());
		}
    }
}
