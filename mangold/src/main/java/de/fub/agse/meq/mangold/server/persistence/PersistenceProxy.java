package de.fub.agse.meq.mangold.server.persistence;


/**
 * This class connects to a remote persistence service.
 * 
 * TODO replace the external service with this sample one
 * 
 * @author Steven Reim
 *
 */
public class PersistenceProxy {
	
	private static PersistenceProxy instance;
    
    
    private PersistenceProxy() {
		this.initConnection();
    }
    
    public static PersistenceProxy getInstance() {
        if(instance == null)
        	instance = new PersistenceProxy();
        return instance;
    }
    
    private void initConnection() {
        
    }
}
