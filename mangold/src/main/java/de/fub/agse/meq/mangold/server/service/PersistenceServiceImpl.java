package de.fub.agse.meq.mangold.server.service;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import de.fub.agse.meq.mangold.client.serialization.JsonElement;
import de.fub.agse.meq.mangold.client.serialization.JsonWrapper;
import de.fub.agse.meq.mangold.client.service.PersistenceService;
import de.fub.agse.meq.mangold.server.persistence.PersistenceProxy;

public class PersistenceServiceImpl extends ServiceBase implements PersistenceService {

	private static final long serialVersionUID = 3322638121256859235L;
	
	private static final Logger log = Logger.getLogger(PersistenceService.class.getName());
	
//	private final static DatabaseController dbController = DatabaseController.getInstance();
	private final static PersistenceProxy persistence = PersistenceProxy.getInstance();

	@Override
	public List<JsonWrapper> getComponents() {
		List<JsonWrapper> list = new ArrayList<JsonWrapper>();
		
		list.add(new JsonElement("{\"name\" : \"DBpedia.de SPARQL Endpoint\", \"componentId\" : \"sparql\", \"endpoint\": {\"default\": \"http://de.dbpedia.org/sparql\"}, \"output\": {\"name\": \"SPARQL Source Connection Point\", \"query\": \"SELECT foo FROM bar\"} }"));
		list.add(new JsonElement("{\"name\" : \"Some Info Sink\", \"componentId\" : \"infoSink\", \"input\": {\"name\": \"Some Info Sink Connection Point\"}}"));
		list.add(new JsonElement("{\"name\" : \"General MySQL Endpoint\", \"componentId\" : \"mysql\", \"host\": { \"default\": \"http://127.0.0.1:5050\"} , \"database\": { \"default\": \"mainDb\"} , \"user\": { \"default\": \"root\"} , \"password\": { \"default\": \"password\"}, \"output\": { \"name\" : \"MySQL Source Connection Point\", \"query\" : \"SELECT foo FROM bar\" } }"));
		list.add(new JsonElement("{\"name\" : \"Generic Transformation\", \"componentId\" : \"transformation\", \"input\": {\"name\": \"Generic Transformation Connection Point\"}, \"output\": { \"name\": \"Generic Transformation Connection Point\", \"query\": \"SELECT foo FROM bar\" } }"));
		log.log(Level.FINE, "Loaded "+list.size()+" Components");
		return list;
	}

	@Override
	public void saveComponents(List<JsonWrapper> components) {
		for (JsonWrapper jsonWrapper : components) {
			//TODO Do sth with that json Stuff
			System.out.println(jsonWrapper.getJson());
		}
	}

	
}
