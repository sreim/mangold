package de.fub.agse.meq.mangold.server.service;

import java.util.logging.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.google.gwt.user.server.rpc.RemoteServiceServlet;

import de.fub.agse.meq.mangold.client.UserInfo;

public class ServiceBase extends RemoteServiceServlet {

	private static final long serialVersionUID = 5863047321777418782L;
	
	private static final Logger log = Logger.getLogger(ServiceBase.class.getName());
	
	private final String KEY_USER = "user";
	
	protected String storeLogin(UserInfo user) {
		return storeSessionData(KEY_USER, user);
	}
	
	protected void invalidateLogin() {
		HttpSession session = getSession();
		session.removeAttribute(KEY_USER);
	}
	
	protected UserInfo getUserFromSession() {
		Object userObj = getSessionData(KEY_USER);
		if(userObj == null)
			return null;
		try {
			UserInfo sessionUser = (UserInfo) userObj;
			if(sessionUser.isLoggedIn())
				return sessionUser;
		} catch (ClassCastException e) {
			log.warning("error trying to cast session user object to UserInfo though being "+userObj.getClass().getName());
		}			
		return null;
	}

	private String storeSessionData(String key, Object value) {
		HttpSession session = getSession();
		session.setAttribute(key, value);
		return session.getId();
	}
	
	private Object getSessionData(String key) {
		HttpSession session = getSession();
		Object obj = session.getAttribute(key);
		return obj;
	}
	
	private HttpSession getSession() {
		HttpServletRequest req = getThreadLocalRequest();
		HttpSession session = req.getSession();
		return session;
	}
}
