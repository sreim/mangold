package de.fub.agse.meq.mangold.server.service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.EntityNotFoundException;
import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.users.User;
import com.google.appengine.api.users.UserService;
import com.google.appengine.api.users.UserServiceFactory;
import com.google.gwt.core.shared.GWT;

import de.fub.agse.meq.mangold.client.LoginInfo;
import de.fub.agse.meq.mangold.client.UserInfo;
import de.fub.agse.meq.mangold.client.dat.SessionUserException;
import de.fub.agse.meq.mangold.client.exception.ComponentDoesNotExistException;
import de.fub.agse.meq.mangold.client.exception.SaveDeltaException;
import de.fub.agse.meq.mangold.client.serialization.JsonElement;
import de.fub.agse.meq.mangold.client.serialization.JsonWrapper;
import de.fub.agse.meq.mangold.client.service.WorkspaceService;
import de.fub.agse.meq.mangold.server.Storage;
import de.fub.agse.meq.mangold.server.SystemProperties;
import de.fub.agse.meq.mangold.server.ThingStorage;

public class WorkspaceServiceImpl extends ServiceBase implements
		WorkspaceService {

	private static final long serialVersionUID = 4044637182687711418L;

	public static final String userKeyKind = "UserInfo";
	public static final String workspaceKeyKind = "Workspace";

	private static final Logger log = Logger
			.getLogger(WorkspaceServiceImpl.class.getName());

	private static final Map<String, Map<String, ThingStorage>> storages = new HashMap<String, Map<String, ThingStorage>>();
	private static final Map<String, String> latestUsedWorkspaces = new HashMap<String, String>();
	
	public WorkspaceServiceImpl() {
	}

	private boolean isProductionMode() {
		return GWT.isScript();
	}

	public UserInfo relogin() {
		UserInfo user = getUserFromSession();
		if (user == null) {
			user = new UserInfo();
			user.setLoggedIn(false);
		} else {
			user.setLoggedIn(true);
			if (isAdmin(user.getMailAddress()))
				user.setAdmin(true);
		}
		return user;
	}

	public UserInfo login(String username, String password, boolean remember) {
		// TODO implement own Database here

		UserInfo user = new UserInfo();
		user.setMailAddress(username);

		user.setLoggedIn(true);
		storeLogin(user);

		if (isAdmin(username, password))
			user.setAdmin(true);

		log.fine("User <" + user.getMailAddress() + "> logged in. Is Admin: "
				+ (user.isAdmin() ? "true" : "false"));

		return user;
	}

	public void logout() {
		invalidateLogin();
	}

	@Deprecated
	public LoginInfo login(String requestUri, String workspaceName) {

		// Retrieve current user
		final UserService userService = UserServiceFactory.getUserService();
		final User user = userService.getCurrentUser();

		LoginInfo loginInfo = new LoginInfo();

		if (user != null) {

			loginInfo.setLoggedIn(true);
			loginInfo.setEmailAddress(user.getEmail());
			loginInfo.setNickname(user.getNickname());

			String logoutURL = userService.createLogoutURL(requestUri);
			if (isProductionMode())
				logoutURL += "?gwt.codesvr=127.0.0.1:9997"; // TODO: this only
															// works with
															// default settings
			loginInfo.setLogoutUrl(logoutURL);

			// Get a handle on the datastore itself
			final DatastoreService datastore = DatastoreServiceFactory
					.getDatastoreService();

			// Lookup data by known key name
			Key userKey = KeyFactory.createKey(userKeyKind, user.getUserId());
			Entity userEntity;
			try {
				userEntity = datastore.get(userKey);
			} catch (EntityNotFoundException e) {
				// Must be a new user!
				String msg = String.format("Creating new user %s",
						userKey.getName());
				log.info(msg);
				userEntity = new Entity(userKeyKind, user.getUserId());
				datastore.put(userEntity);
			}

		} else {
			loginInfo.setLoggedIn(false);

			String loginURL = userService.createLoginURL(requestUri);
			if (isProductionMode())
				loginURL += "?gwt.codesvr=127.0.0.1:9997"; // TODO: this only
															// works with
															// default settings
			loginInfo.setLoginUrl(loginURL);
		}
		return loginInfo;
	}

	private boolean isAdmin(String username, String password) {
		return isAdmin(username)
				&& SystemProperties.ADMIN_PASSWORD.equals(password);
	}

	private boolean isAdmin(String username) {
		return SystemProperties.ADMIN_USERNAME.equals(username);
	}

	@Override
	public List<String> createUUIDs(int number) {
		List<String> result = new ArrayList<String>();
		for (int i = 0; i < number; ++i) {
			result.add(UUID.randomUUID().toString());
		}
		return result;
	}
	
	@Override
	public JsonWrapper loadLatest() {
		UserInfo user = getUserFromSession();
		String latestWorkspace = latestUsedWorkspaces.get(user.getMailAddress());
		if(latestWorkspace != null && !latestWorkspace.isEmpty()) 
			return load(latestWorkspace);
		return null;
	}

	@Override
	public JsonWrapper load(String name) {
		UserInfo user = getUserFromSession();
		if(!storages.containsKey(user.getMailAddress())) {
			storages.put(user.getMailAddress(), new HashMap<String, ThingStorage>());
		}
		Map<String, ThingStorage> userStorage = storages.get(user.getMailAddress());
		ThingStorage storage = userStorage.get(name);

		JsonNode root = ThingStorage.mapper.createObjectNode();
		((ObjectNode) root).put("name", name);
		JsonNode things = ThingStorage.mapper.createArrayNode();
		JsonNode node = null;
		try {
			for (String nodeStr : storage.readAll()) {
				node = ThingStorage.mapper.readTree(nodeStr);
				((ArrayNode) things).add(node);
			}
			((ObjectNode) root).put(SystemProperties.WORKSPACE_THINGS, things);
			((ObjectNode) root).put(SystemProperties.WORKSPACE_POSITION, storage.getPosition());
			((ObjectNode) root).put(SystemProperties.WORKSPACE_ZOOM, storage.getZoom());
			return new JsonElement(root.toString());
		} catch (JsonProcessingException e) {
			log.severe(String.format(
					"Problem while unmarshaling content to save workspace: %s",
					e.getMessage()));
		} catch (IOException e) {
			log.severe(String
					.format("Problem while receiving and unmarshaling content to save workspace: %s",
							e.getMessage()));
		}

		return null;
	}

	public List<String> getWorkspacesToLoad() {
		List<String> list = new ArrayList<String>();
		
		UserInfo user = getUserFromSession();
		if(!storages.containsKey(user.getMailAddress())) {
			storages.put(user.getMailAddress(), new HashMap<String, ThingStorage>());
		}
		Map<String, ThingStorage> userStorage = storages.get(user.getMailAddress());
		
		for (String key : userStorage.keySet()) {
			list.add(key);
		}
		return list;
	}

	@Override
	public void save(String data) throws SessionUserException {
		JsonNode root;
		log.config("Saving workspace");
		UserInfo user = getUserFromSession();
		if(!storages.containsKey(user.getMailAddress())) {
			storages.put(user.getMailAddress(), new HashMap<String, ThingStorage>());
		}
		Map<String, ThingStorage> userStorage = storages.get(user.getMailAddress());
		try {
			root = ThingStorage.mapper.readTree(data);
			final String name = root.get("name").asText();
			final ThingStorage storage = new ThingStorage(name);

			// Store each thing in the workspace
			for (JsonNode node : root.get(SystemProperties.WORKSPACE_THINGS)) {
				storage.write(node);
			}
			storage.setPosition(root.get(SystemProperties.WORKSPACE_POSITION));
			storage.setZoom(Double.parseDouble(root.get(SystemProperties.WORKSPACE_ZOOM).textValue()));
			userStorage.put(name, storage);
			
			latestUsedWorkspaces.put(user.getMailAddress(), name);

			// Log
			log.config(String.format("Saved workspace %s", name));
			if (log.isLoggable(Level.FINEST)) {
				ObjectWriter writer = Storage.mapper
						.writerWithDefaultPrettyPrinter();
				log.finest(writer.writeValueAsString(root));
			}

		} catch (JsonProcessingException e) {
			log.severe(String.format(
					"Problem while unmarshaling content to save workspace: %s",
					e.getMessage()));
		} catch (IOException e) {
			log.severe(String
					.format("Problem while receiving and unmarshaling content to save workspace: %s",
							e.getMessage()));
		}
	}

	@Override
	public void saveDelta(String workspaceName, Map<String, Map<String, JsonWrapper>> delta, double zoom, double workspaceX, double workspaceY) throws SaveDeltaException, ComponentDoesNotExistException {
		UserInfo user = getUserFromSession();
		if(!storages.containsKey(user.getMailAddress())) {
			storages.put(user.getMailAddress(), new HashMap<String, ThingStorage>());
		}
		Map<String, ThingStorage> userStorage = storages.get(user.getMailAddress());
		if(userStorage.containsKey(workspaceName)) {
			ThingStorage storage = userStorage.get(workspaceName);
			JsonNode node;
			JsonWrapper wrapper;
			try {
				//save added components
				for(String key : delta.get(SystemProperties.WORKSPACE_DELTA_ADDED).keySet()) {
					wrapper = delta.get(SystemProperties.WORKSPACE_DELTA_ADDED).get(key);
					node = ThingStorage.mapper.readTree(wrapper.getJson());
					storage.write(node);
				}
				
				//save edited components
				for(String key : delta.get(SystemProperties.WORKSPACE_DELTA_CHANGED).keySet()) {
					if(storage.contains(key)) {
						wrapper = delta.get(SystemProperties.WORKSPACE_DELTA_CHANGED).get(key);
						node = ThingStorage.mapper.readTree(wrapper.getJson());
						storage.write(key, node);
					} else {
						throw new ComponentDoesNotExistException("Could not edit Component "+ key +" - does not exist"); 
					}
				}
				
				//remove components
				for (String  key : delta.get(SystemProperties.WORKSPACE_DELTA_REMOVED).keySet()) {
					if(!storage.remove(key)) 
						throw new ComponentDoesNotExistException("Could not remove Component "+ key +" - does not exist");
				}
				
				
				storage.setZoom(zoom);
				JsonNode pos = ThingStorage.mapper.createObjectNode();
				((ObjectNode) pos).put("x", workspaceX);
				((ObjectNode) pos).put("y", workspaceY);
				storage.setPosition(pos);
				
				latestUsedWorkspaces.put(user.getMailAddress(), workspaceName);
				
			}  catch (JsonProcessingException e) {
				log.severe(String.format(
						"Problem while unmarshaling content to save workspace: %s",
						e.getMessage()));
			} catch (IOException e) {
				log.severe(String
						.format("Problem while receiving and unmarshaling content to save workspace: %s",
								e.getMessage()));
			}
		} else {
			throw new SaveDeltaException("Could not find workspace name in storage!");
		}
	}
}
