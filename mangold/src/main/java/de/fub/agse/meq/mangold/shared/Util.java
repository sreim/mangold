package de.fub.agse.meq.mangold.shared;

public class Util {
	
	public static String normalizeName(final String name) {
		final String tokens[] = name.split("_");
		final StringBuffer result = new StringBuffer();
		for (String s: tokens) {
			s = s.toLowerCase();
			char[] c = s.toCharArray();
			c[0] = Character.toUpperCase(c[0]);
			result.append(c);
			result.append(" ");
		}
		return result.toString().trim();
	}

}
